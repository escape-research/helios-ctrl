<?xml version='1.0' encoding='ISO-8859-1' standalone='yes' ?>
<tagfile>
  <compound kind="page">
    <name>index</name>
    <title>VST Module Architecture</title>
    <filename>index</filename>
    <docanchor file="index">piInterfaces</docanchor>
    <docanchor file="index">piVstMa</docanchor>
    <docanchor file="index">piPlugins</docanchor>
    <docanchor file="index">Unicode</docanchor>
  </compound>
  <compound kind="file">
    <name>conststringtable.cpp</name>
    <path>/Users/user/.pulse-agent2/data/recipes/41277653/base/HwRemoteSDK/pluginterfaces/base/</path>
    <filename>conststringtable_8cpp</filename>
    <includes id="conststringtable_8h" name="conststringtable.h" local="yes" imported="no">conststringtable.h</includes>
  </compound>
  <compound kind="file">
    <name>conststringtable.h</name>
    <path>/Users/user/.pulse-agent2/data/recipes/41277653/base/HwRemoteSDK/pluginterfaces/base/</path>
    <filename>conststringtable_8h</filename>
    <includes id="ftypes_8h" name="ftypes.h" local="yes" imported="no">ftypes.h</includes>
    <class kind="class">Steinberg::ConstStringTable</class>
    <namespace>Steinberg</namespace>
  </compound>
  <compound kind="file">
    <name>doc.h</name>
    <path>/Users/user/.pulse-agent2/data/recipes/41277653/base/HwRemoteSDK/pluginterfaces/base/</path>
    <filename>doc_8h</filename>
  </compound>
  <compound kind="file">
    <name>fplatform.h</name>
    <path>/Users/user/.pulse-agent2/data/recipes/41277653/base/HwRemoteSDK/pluginterfaces/base/</path>
    <filename>fplatform_8h</filename>
    <member kind="define">
      <type>#define</type>
      <name>kLittleEndian</name>
      <anchorfile>fplatform_8h.html</anchorfile>
      <anchor>abfa3c6c92ac4ce1ce65133b421fee2fe</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>kBigEndian</name>
      <anchorfile>fplatform_8h.html</anchorfile>
      <anchor>a59154ea8599c8d0e7187be1c42889675</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MAC</name>
      <anchorfile>fplatform_8h.html</anchorfile>
      <anchor>a4d942614e0ca30372d649e643d229f90</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PTHREADS</name>
      <anchorfile>fplatform_8h.html</anchorfile>
      <anchor>a3c2a454c4a71caf93110e4a1d5db7a55</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TARGET_API_MAC_CARBON</name>
      <anchorfile>fplatform_8h.html</anchorfile>
      <anchor>a8601b9b36b5d7d40e6d42c0e04dd936c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>BYTEORDER</name>
      <anchorfile>fplatform_8h.html</anchorfile>
      <anchor>a0c2fdec33260a22c7f28e1cc4d1de480</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fstrdefs.h</name>
    <path>/Users/user/.pulse-agent2/data/recipes/41277653/base/HwRemoteSDK/pluginterfaces/base/</path>
    <filename>fstrdefs_8h</filename>
    <includes id="ftypes_8h" name="ftypes.h" local="yes" imported="no">ftypes.h</includes>
    <includes id="conststringtable_8h" name="conststringtable.h" local="yes" imported="no">conststringtable.h</includes>
    <namespace>Steinberg</namespace>
    <member kind="define">
      <type>#define</type>
      <name>STR16</name>
      <anchorfile>fstrdefs_8h.html</anchorfile>
      <anchor>a826c07705e0d6242387611986d74ee48</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>STR</name>
      <anchorfile>fstrdefs_8h.html</anchorfile>
      <anchor>a18d295a837ac71add5578860b55e5502</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>tStrBufferSize</name>
      <anchorfile>fstrdefs_8h.html</anchorfile>
      <anchor>a237b89af6a40bef74dff5b5e67efae92</anchor>
      <arglist>(buffer)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>str8BufferSize</name>
      <anchorfile>fstrdefs_8h.html</anchorfile>
      <anchor>aa2da887b221541aae95dc720e91f1563</anchor>
      <arglist>(buffer)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>str16BufferSize</name>
      <anchorfile>fstrdefs_8h.html</anchorfile>
      <anchor>ab60e7beaa39f0257d103524ccbb7aa9e</anchor>
      <arglist>(buffer)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FORMAT_INT64A</name>
      <anchorfile>fstrdefs_8h.html</anchorfile>
      <anchor>adebf25a9067f8beae00b856b97e59493</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FORMAT_UINT64A</name>
      <anchorfile>fstrdefs_8h.html</anchorfile>
      <anchor>acaa60848d1abc5e2ecc7bb7495cb167e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>stricmp</name>
      <anchorfile>fstrdefs_8h.html</anchorfile>
      <anchor>a4e0be90a3757e352f42612d09a7d1aa5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>strnicmp</name>
      <anchorfile>fstrdefs_8h.html</anchorfile>
      <anchor>a16a19b1831112e876b010468ec15916f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FORMAT_INT64</name>
      <anchorfile>fstrdefs_8h.html</anchorfile>
      <anchor>a6fc06c408b7c51eb867462d2733051a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FORMAT_UINT64</name>
      <anchorfile>fstrdefs_8h.html</anchorfile>
      <anchor>ad76e8f81482f416ef999fa817c37d1d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENDLINE_A</name>
      <anchorfile>fstrdefs_8h.html</anchorfile>
      <anchor>a46520f93e91d264d018120231fdd7afa</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENDLINE_W</name>
      <anchorfile>fstrdefs_8h.html</anchorfile>
      <anchor>a28de1ded2200fa58d2e63f3852ee7da0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ENDLINE</name>
      <anchorfile>fstrdefs_8h.html</anchorfile>
      <anchor>a1f1b098f673669db04b6f4aa6a8fdc8e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>_tstrlen</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a0f20517a819515665c6fbd35ece5a434</anchor>
      <arglist>(const T *wcs)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>tstrlen</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a51fad7b4c1e3dfa032d4f638ecd68c7a</anchor>
      <arglist>(const char *str)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>strlen8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a350bed0883f4792e7535252b3122dd5b</anchor>
      <arglist>(const char8 *str)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>strlen16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>aae116b01b47bca8e67679337ef48d8d3</anchor>
      <arglist>(const char16 *str)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>_tstrcmp</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a7023958e09487e98aaa402f6b4665212</anchor>
      <arglist>(const T *src, const T *dst)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>tstrcmp</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a2f01ef81ad1c686ebb34a97ef4e2d098</anchor>
      <arglist>(const char *src, const char *dst)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>strcmp8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a90dd0a8f52813eb453f60ac1c49559bc</anchor>
      <arglist>(const char8 *src, const char8 *dst)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>strcmp16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a9b47c8ef5ed96ca9034e87624f69b890</anchor>
      <arglist>(const char16 *src, const char16 *dst)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>_tstrncmp</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a9472c661ae9df487057d848006bd12b1</anchor>
      <arglist>(const T *first, const T *last, uint32 count)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>tstrncmp</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a25694d169804ad218b221f3e101eaee4</anchor>
      <arglist>(const char *first, const char *last, uint32 count)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>strncmp8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a8f3cca9e30bb739b30be3eca6b891c8f</anchor>
      <arglist>(const char8 *first, const char8 *last, uint32 count)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>strncmp16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a54115da13293d44eaae9ba44a4fa82e2</anchor>
      <arglist>(const char16 *first, const char16 *last, uint32 count)</arglist>
    </member>
    <member kind="function">
      <type>T *</type>
      <name>_tstrcpy</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a0ed99e5e2d595d1fbb46be4af6bad16e</anchor>
      <arglist>(T *dst, const T *src)</arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>tstrcpy</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a406cfbdd4e67843189e729a50a22c952</anchor>
      <arglist>(char *dst, const char *src)</arglist>
    </member>
    <member kind="function">
      <type>char8 *</type>
      <name>strcpy8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>aa12723f519b8ecf7d99b4067b6f44344</anchor>
      <arglist>(char8 *dst, const char8 *src)</arglist>
    </member>
    <member kind="function">
      <type>char16 *</type>
      <name>strcpy16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a32880d14cc3ac2196ddd0859e4fb7bd6</anchor>
      <arglist>(char16 *dst, const char16 *src)</arglist>
    </member>
    <member kind="function">
      <type>T *</type>
      <name>_tstrncpy</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a78d52df38c5128a70f98c9b832853ed9</anchor>
      <arglist>(T *dest, const T *source, uint32 count)</arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>tstrncpy</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>ae8ecddebc9de1d21e16eb0929574959a</anchor>
      <arglist>(char *dest, const char *source, uint32 count)</arglist>
    </member>
    <member kind="function">
      <type>char8 *</type>
      <name>strncpy8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a6d93eaadf6a7fa89aa51c5ee8cdae132</anchor>
      <arglist>(char8 *dest, const char8 *source, uint32 count)</arglist>
    </member>
    <member kind="function">
      <type>char16 *</type>
      <name>strncpy16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a71e109979dde381bbba56500bbde60e8</anchor>
      <arglist>(char16 *dest, const char16 *source, uint32 count)</arglist>
    </member>
    <member kind="function">
      <type>T *</type>
      <name>_tstrcat</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a71213f60ea53751e1460a66919e526f4</anchor>
      <arglist>(T *dst, const T *src)</arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>tstrcat</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a64e0b47b3f8b466e2cb815d6662298e0</anchor>
      <arglist>(char *dst, const char *src)</arglist>
    </member>
    <member kind="function">
      <type>char8 *</type>
      <name>strcat8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>ac632cd3fa4f20541cabfb27d20ad2581</anchor>
      <arglist>(char8 *dst, const char8 *src)</arglist>
    </member>
    <member kind="function">
      <type>char16 *</type>
      <name>strcat16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a90f489c9a479d8c86d651dbe97f6f74f</anchor>
      <arglist>(char16 *dst, const char16 *src)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>str8ToStr16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a43172f2a5d34fc811021a16d1a38434d</anchor>
      <arglist>(char16 *dst, const char8 *src, int32 n=-1)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>FIDStringsEqual</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>ae609f786a23774a473851d3eddaa22ff</anchor>
      <arglist>(FIDString id1, FIDString id2)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>ftypes.h</name>
    <path>/Users/user/.pulse-agent2/data/recipes/41277653/base/HwRemoteSDK/pluginterfaces/base/</path>
    <filename>ftypes_8h</filename>
    <includes id="fplatform_8h" name="fplatform.h" local="yes" imported="no">fplatform.h</includes>
    <namespace>Steinberg</namespace>
    <member kind="define">
      <type>#define</type>
      <name>UNICODE</name>
      <anchorfile>ftypes_8h.html</anchorfile>
      <anchor>a09ecca53f2cd1b8d1c566bedb245e141</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>_UNICODE</name>
      <anchorfile>ftypes_8h.html</anchorfile>
      <anchor>a78880e1abcefc90f14185aee93ad0e20</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SWAP_32</name>
      <anchorfile>ftypes_8h.html</anchorfile>
      <anchor>ad0de6cd6cef290e9609feccdf002f51c</anchor>
      <arglist>(l)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SWAP_16</name>
      <anchorfile>ftypes_8h.html</anchorfile>
      <anchor>a178e2230dac3adf497d06c6f42abccad</anchor>
      <arglist>(w)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SWAP_64</name>
      <anchorfile>ftypes_8h.html</anchorfile>
      <anchor>ae7ff9262ec2cb91d49035bffbea62625</anchor>
      <arglist>(i)</arglist>
    </member>
    <member kind="typedef">
      <type>char</type>
      <name>int8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a76303c06c8689fb47178d9baabce246b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned char</type>
      <name>uint8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>ac26ab57d77dced6ef40e21775a18e4fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned char</type>
      <name>uchar</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>ac51755538a5d39c7345c4987a3096440</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>short</type>
      <name>int16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a091b21d361005029928d1a49e59f1fd9</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned short</type>
      <name>uint16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a5f06a8622e4753e2f9617feb26f625ae</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>long</type>
      <name>int32</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a1d803386753fac54394e2d4cd95bb208</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned long</type>
      <name>uint32</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a15d783bb5cc364d5fbf405f08f79cd07</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>long long</type>
      <name>int64</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a8e6d63a581d885f3e9a3e1642b0f75b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned long long</type>
      <name>uint64</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a04d83e9fd2cf38c5ca71f1359271476f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int64</type>
      <name>TSize</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a32bafd3ffec4907b661ddcd0724846a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int32</type>
      <name>tresult</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>aea95c0544bdb23962c2e6f2d16843225</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>uint32</type>
      <name>TPtrInt</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a8b8518bce61dcab277a071466e1c71e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>uint8</type>
      <name>TBool</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>aa89abbc5b931de8ef6ef03ec0f342375</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>char</type>
      <name>char8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a4a83db7f2467f59f3050248287aef5fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int16</type>
      <name>char16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a1d62754aa66db400e0a9ddf18dc912b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>char16</type>
      <name>char</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a5f3e8d57146de462f5d51b3a0f619edf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>const char8 *</type>
      <name>CStringA</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a95bf7f0949d8a9419ac7281b386a279b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>const char16 *</type>
      <name>CStringW</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>adc199510a6f16e934a6bb602721204ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>const char *</type>
      <name>CString</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a7d0ff086774285ba7acb0770eab39caf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>const char8 *</type>
      <name>FIDString</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a6ca68110b739e9a9bec4fe884cad394a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int32</type>
      <name>UCoord</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a91e26bd765c8845ae5e3cf7871c842e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>strEmpty</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a6df75d455350d4aaadc70786730f11fa</anchor>
      <arglist>(const char *str)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>str8Empty</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a107fdab7d7c86e4f5191a27005a6bbc3</anchor>
      <arglist>(const char8 *str)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>str16Empty</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a51822113d808dc2e0594f7f1c61fa823</anchor>
      <arglist>(const char16 *str)</arglist>
    </member>
    <member kind="variable">
      <type>const FIDString</type>
      <name>kPlatformStringWin</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a6605d81276d8182104fa7df6470b22af</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const FIDString</type>
      <name>kPlatformStringMac</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a11d7b5f02c2605bcde61a18d2d69f88b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const FIDString</type>
      <name>kPlatformString</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a6bbdca6c72d935ab0a455e38657b6f24</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>funknown.cpp</name>
    <path>/Users/user/.pulse-agent2/data/recipes/41277653/base/HwRemoteSDK/pluginterfaces/base/</path>
    <filename>funknown_8cpp</filename>
    <includes id="funknown_8h" name="funknown.h" local="yes" imported="no">funknown.h</includes>
    <includes id="fstrdefs_8h" name="fstrdefs.h" local="yes" imported="no">fstrdefs.h</includes>
    <namespace>Steinberg</namespace>
  </compound>
  <compound kind="file">
    <name>funknown.h</name>
    <path>/Users/user/.pulse-agent2/data/recipes/41277653/base/HwRemoteSDK/pluginterfaces/base/</path>
    <filename>funknown_8h</filename>
    <includes id="ftypes_8h" name="ftypes.h" local="yes" imported="no">ftypes.h</includes>
    <class kind="class">Steinberg::FUID</class>
    <class kind="class">Steinberg::FUnknown</class>
    <class kind="struct">Steinberg::FReleaser</class>
    <class kind="class">Steinberg::IPtr</class>
    <class kind="class">Steinberg::FUnknownPtr</class>
    <class kind="class">Steinberg::FVariant</class>
    <namespace>Steinberg</namespace>
    <member kind="define">
      <type>#define</type>
      <name>COM_COMPATIBLE</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>a23a5cf1b40dd19e713a03ac320c81ca7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PLUGIN_API</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>afcb1b0bf7320fa3ebfc13fd896ebca4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>INLINE_UID</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>a8024fad3ea7ecdd1ab3221d15e04aa52</anchor>
      <arglist>(l1, l2, l3, l4)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DECLARE_UID</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>a5e349c26a022f90b3562eeb32976e6ed</anchor>
      <arglist>(name, l1, l2, l3, l4)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>EXTERN_UID</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>a845c8fdb88f7f23c4060134a798a1e7d</anchor>
      <arglist>(name)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DECLARE_CLASS_IID</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>aae92a855975a91bb846f6e0f906745f1</anchor>
      <arglist>(ClassName, l1, l2, l3, l4)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DEF_CLASS_IID</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>a8999be9dadfd41e1f563866e01e94d1c</anchor>
      <arglist>(ClassName)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>INLINE_UID_OF</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>aaf419cd3f56488ae59cd1b9c8397377b</anchor>
      <arglist>(ClassName)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>INLINE_UID_FROM_FUID</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>ae43645513f5c72529a3fb6c6d1e76b46</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DECLARE_FUNKNOWN_METHODS</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>acb6e5cc5e226c37c1fca4f5192fb80a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DELEGATE_REFCOUNT</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>a6d9d411a27dcc158df8ff1f5e73510d0</anchor>
      <arglist>(ClassName)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IMPLEMENT_REFCOUNT</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>ae3db5eac4c75b5199dcdc4462b6289aa</anchor>
      <arglist>(ClassName)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FUNKNOWN_CTOR</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>a2e898abff4a673feaa52f7ed20a42e50</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>FUNKNOWN_DTOR</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>ac31390eb834d3acfda3d871db3f9f959</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>QUERY_INTERFACE</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>a1884893f6b75952d6e29bcd333f3ea40</anchor>
      <arglist>(iid, obj, InterfaceIID, InterfaceName)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IMPLEMENT_QUERYINTERFACE</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>a023a5c16eb6b8a0057d712343f9d752c</anchor>
      <arglist>(ClassName, InterfaceName, ClassIID)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IMPLEMENT_FUNKNOWN_METHODS</name>
      <anchorfile>funknown_8h.html</anchorfile>
      <anchor>a3131ec3273dc17d39bef17154309b893</anchor>
      <arglist>(ClassName, InterfaceName, ClassIID)</arglist>
    </member>
    <member kind="typedef">
      <type>int64</type>
      <name>LARGE_INT</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a14e2e16222a7f4eea4e11b63539d4768</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int8</type>
      <name>TUID</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a5d8be9d51c9a0fd7cca7f16c262d8460</anchor>
      <arglist>[16]</arglist>
    </member>
    <member kind="enumvalue">
      <name>kNoInterface</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8a0b0fde070589cc4246d5d9e0cf4c99e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kResultOk</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8a186a780dc7f261ce261f6669ec20c64c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kResultTrue</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8abe8a193950563af44d7042627097968b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kResultFalse</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8aaf8cabc311c10bb47d816dae21a06f4c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kInvalidArgument</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8a2dd28db67752c148a0a05b79c0e4a543</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kNotImplemented</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8a5005741bef61bc2dcc1595c9cfeed5a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kInternalError</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8a37e1717b845b9b4d7097c1976d24216d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kNotInitialized</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8af7b68bfec65ee88636794b7f78edb0ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kOutOfMemory</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8a42a74955a32b411501297bf214eac9cd</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>futils.h</name>
    <path>/Users/user/.pulse-agent2/data/recipes/41277653/base/HwRemoteSDK/pluginterfaces/base/</path>
    <filename>futils_8h</filename>
    <namespace>Steinberg</namespace>
    <member kind="function">
      <type>const T &amp;</type>
      <name>Min</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>adcde5fff9b5074d3e36fc411ee865dd9</anchor>
      <arglist>(const T &amp;a, const T &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>const T &amp;</type>
      <name>Max</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>aa4b9f4d937d8d865a8bfa86e6e392505</anchor>
      <arglist>(const T &amp;a, const T &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>Abs</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>acbbd1cb9aa59aaa62572519d5e381549</anchor>
      <arglist>(const T &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>Sign</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a8f0cb0def093b54ebe00691604f5a09b</anchor>
      <arglist>(const T &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>Bound</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a91ac21e99d77a7471db59984466167bb</anchor>
      <arglist>(T minval, T maxval, T x)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Swap</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a5d8f1525e15b2c83643fc473d75b6c19</anchor>
      <arglist>(T &amp;t1, T &amp;t2)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>ierrorcontext.h</name>
    <path>/Users/user/.pulse-agent2/data/recipes/41277653/base/HwRemoteSDK/pluginterfaces/base/</path>
    <filename>ierrorcontext_8h</filename>
    <includes id="funknown_8h" name="funknown.h" local="yes" imported="no">pluginterfaces/base/funknown.h</includes>
    <class kind="class">Steinberg::IErrorContext</class>
    <namespace>Steinberg</namespace>
  </compound>
  <compound kind="file">
    <name>ihostapplication.h</name>
    <path>/Users/user/.pulse-agent2/data/recipes/41277653/base/HwRemoteSDK/pluginterfaces/host/</path>
    <filename>ihostapplication_8h</filename>
    <class kind="class">Steinberg::IHostApplicationA</class>
    <class kind="class">Steinberg::IHostApplicationW</class>
    <class kind="class">Steinberg::IHostApplication2</class>
    <class kind="class">Steinberg::ISplashDisplayA</class>
    <class kind="class">Steinberg::ISplashDisplayW</class>
    <class kind="class">Steinberg::IHostVersionInfo</class>
    <namespace>Steinberg</namespace>
  </compound>
  <compound kind="file">
    <name>ihostclasses.h</name>
    <path>/Users/user/.pulse-agent2/data/recipes/41277653/base/HwRemoteSDK/pluginterfaces/host/</path>
    <filename>ihostclasses_8h</filename>
    <class kind="class">Steinberg::IHostClasses</class>
    <class kind="class">Steinberg::FInstancePtr</class>
    <namespace>Steinberg</namespace>
    <member kind="define">
      <type>#define</type>
      <name>FHostCreate</name>
      <anchorfile>group__hostInterfacesBasic.html</anchorfile>
      <anchor>gaf92e0e865a5aba9a43465219dd5b6f8c</anchor>
      <arglist>(Interface, hostClasses)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>ipluginbase.h</name>
    <path>/Users/user/.pulse-agent2/data/recipes/41277653/base/HwRemoteSDK/pluginterfaces/base/</path>
    <filename>ipluginbase_8h</filename>
    <includes id="funknown_8h" name="funknown.h" local="yes" imported="no">funknown.h</includes>
    <includes id="fstrdefs_8h" name="fstrdefs.h" local="yes" imported="no">fstrdefs.h</includes>
    <class kind="class">Steinberg::IPluginBase</class>
    <class kind="struct">Steinberg::PFactoryInfo</class>
    <class kind="struct">Steinberg::PClassInfo</class>
    <class kind="class">Steinberg::IPluginFactory</class>
    <class kind="struct">Steinberg::PClassInfo2</class>
    <class kind="class">Steinberg::IPluginFactory2</class>
    <class kind="struct">Steinberg::PClassInfoW</class>
    <class kind="class">Steinberg::IPluginFactory3</class>
    <namespace>Steinberg</namespace>
    <member kind="define">
      <type>#define</type>
      <name>LICENCE_UID</name>
      <anchorfile>ipluginbase_8h.html</anchorfile>
      <anchor>a15eabd5eb49e5ec7c8e2f6210f53ffdc</anchor>
      <arglist>(l1, l2, l3, l4)</arglist>
    </member>
    <member kind="typedef">
      <type>Steinberg::IPluginFactory *(*</type>
      <name>GetFactoryProc</name>
      <anchorfile>ipluginbase_8h.html</anchorfile>
      <anchor>acdf055c3f848689878f8d7cdccac3406</anchor>
      <arglist>)()</arglist>
    </member>
    <member kind="function">
      <type>Steinberg::IPluginFactory *</type>
      <name>GetPluginFactory</name>
      <anchorfile>group__pluginBase.html</anchorfile>
      <anchor>ga843ac97a36dfc717dadaa7192c7e8330</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="page">
    <name>howtoClass</name>
    <title>HOWTO derive a class from an interface</title>
    <filename>howtoClass</filename>
  </compound>
  <compound kind="page">
    <name>versionInheritance</name>
    <title>Interface Versions and Inheritance</title>
    <filename>versionInheritance</filename>
  </compound>
  <compound kind="page">
    <name>loadPlugin</name>
    <title>How the host will load a plug-in</title>
    <filename>loadPlugin</filename>
  </compound>
  <compound kind="group">
    <name>pluginBase</name>
    <title>Basic Interfaces</title>
    <filename>group__pluginBase.html</filename>
    <class kind="class">Steinberg::FUID</class>
    <class kind="class">Steinberg::FUnknown</class>
    <class kind="struct">Steinberg::FReleaser</class>
    <class kind="class">Steinberg::IPtr</class>
    <class kind="class">Steinberg::FVariant</class>
    <class kind="class">Steinberg::IPluginBase</class>
    <class kind="struct">Steinberg::PFactoryInfo</class>
    <class kind="struct">Steinberg::PClassInfo</class>
    <class kind="class">Steinberg::IPluginFactory</class>
    <class kind="struct">Steinberg::PClassInfo2</class>
    <class kind="class">Steinberg::IPluginFactory2</class>
    <member kind="function">
      <type>Steinberg::IPluginFactory *</type>
      <name>GetPluginFactory</name>
      <anchorfile>group__pluginBase.html</anchorfile>
      <anchor>ga843ac97a36dfc717dadaa7192c7e8330</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>hostInterfacesBasic</name>
    <title>Basic Host Interfaces</title>
    <filename>group__hostInterfacesBasic.html</filename>
    <class kind="class">Steinberg::IHostClasses</class>
    <class kind="class">Steinberg::FInstancePtr</class>
    <class kind="class">Steinberg::IHostApplicationA</class>
    <class kind="class">Steinberg::ISplashDisplayA</class>
    <class kind="class">Steinberg::IHostVersionInfo</class>
    <member kind="define">
      <type>#define</type>
      <name>FHostCreate</name>
      <anchorfile>group__hostInterfacesBasic.html</anchorfile>
      <anchor>gaf92e0e865a5aba9a43465219dd5b6f8c</anchor>
      <arglist>(Interface, hostClasses)</arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>Steinberg</name>
    <filename>namespaceSteinberg.html</filename>
    <class kind="class">Steinberg::ConstStringTable</class>
    <class kind="class">Steinberg::FUID</class>
    <class kind="class">Steinberg::FUnknown</class>
    <class kind="struct">Steinberg::FReleaser</class>
    <class kind="class">Steinberg::IPtr</class>
    <class kind="class">Steinberg::FUnknownPtr</class>
    <class kind="class">Steinberg::FVariant</class>
    <class kind="class">Steinberg::IErrorContext</class>
    <class kind="class">Steinberg::IPluginBase</class>
    <class kind="struct">Steinberg::PFactoryInfo</class>
    <class kind="struct">Steinberg::PClassInfo</class>
    <class kind="class">Steinberg::IPluginFactory</class>
    <class kind="struct">Steinberg::PClassInfo2</class>
    <class kind="class">Steinberg::IPluginFactory2</class>
    <class kind="struct">Steinberg::PClassInfoW</class>
    <class kind="class">Steinberg::IPluginFactory3</class>
    <class kind="class">Steinberg::IHostClasses</class>
    <class kind="class">Steinberg::FInstancePtr</class>
    <class kind="class">Steinberg::IHostApplicationA</class>
    <class kind="class">Steinberg::IHostApplicationW</class>
    <class kind="class">Steinberg::IHostApplication2</class>
    <class kind="class">Steinberg::ISplashDisplayA</class>
    <class kind="class">Steinberg::ISplashDisplayW</class>
    <class kind="class">Steinberg::IHostVersionInfo</class>
    <member kind="typedef">
      <type>char</type>
      <name>int8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a76303c06c8689fb47178d9baabce246b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned char</type>
      <name>uint8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>ac26ab57d77dced6ef40e21775a18e4fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned char</type>
      <name>uchar</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>ac51755538a5d39c7345c4987a3096440</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>short</type>
      <name>int16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a091b21d361005029928d1a49e59f1fd9</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned short</type>
      <name>uint16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a5f06a8622e4753e2f9617feb26f625ae</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>long</type>
      <name>int32</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a1d803386753fac54394e2d4cd95bb208</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned long</type>
      <name>uint32</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a15d783bb5cc364d5fbf405f08f79cd07</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>long long</type>
      <name>int64</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a8e6d63a581d885f3e9a3e1642b0f75b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned long long</type>
      <name>uint64</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a04d83e9fd2cf38c5ca71f1359271476f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int64</type>
      <name>TSize</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a32bafd3ffec4907b661ddcd0724846a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int32</type>
      <name>tresult</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>aea95c0544bdb23962c2e6f2d16843225</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>uint32</type>
      <name>TPtrInt</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a8b8518bce61dcab277a071466e1c71e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>uint8</type>
      <name>TBool</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>aa89abbc5b931de8ef6ef03ec0f342375</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>char</type>
      <name>char8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a4a83db7f2467f59f3050248287aef5fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int16</type>
      <name>char16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a1d62754aa66db400e0a9ddf18dc912b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>char16</type>
      <name>char</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a5f3e8d57146de462f5d51b3a0f619edf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>const char8 *</type>
      <name>CStringA</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a95bf7f0949d8a9419ac7281b386a279b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>const char16 *</type>
      <name>CStringW</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>adc199510a6f16e934a6bb602721204ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>const char *</type>
      <name>CString</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a7d0ff086774285ba7acb0770eab39caf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>const char8 *</type>
      <name>FIDString</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a6ca68110b739e9a9bec4fe884cad394a</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int32</type>
      <name>UCoord</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a91e26bd765c8845ae5e3cf7871c842e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int64</type>
      <name>LARGE_INT</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a14e2e16222a7f4eea4e11b63539d4768</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int8</type>
      <name>TUID</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a5d8be9d51c9a0fd7cca7f16c262d8460</anchor>
      <arglist>[16]</arglist>
    </member>
    <member kind="enumvalue">
      <name>kNoInterface</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8a0b0fde070589cc4246d5d9e0cf4c99e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kResultOk</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8a186a780dc7f261ce261f6669ec20c64c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kResultTrue</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8abe8a193950563af44d7042627097968b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kResultFalse</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8aaf8cabc311c10bb47d816dae21a06f4c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kInvalidArgument</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8a2dd28db67752c148a0a05b79c0e4a543</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kNotImplemented</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8a5005741bef61bc2dcc1595c9cfeed5a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kInternalError</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8a37e1717b845b9b4d7097c1976d24216d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kNotInitialized</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8af7b68bfec65ee88636794b7f78edb0ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kOutOfMemory</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a73319dfb2f91921edfe136abd41b96d8a42a74955a32b411501297bf214eac9cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>_tstrlen</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a0f20517a819515665c6fbd35ece5a434</anchor>
      <arglist>(const T *wcs)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>tstrlen</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a51fad7b4c1e3dfa032d4f638ecd68c7a</anchor>
      <arglist>(const char *str)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>strlen8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a350bed0883f4792e7535252b3122dd5b</anchor>
      <arglist>(const char8 *str)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>strlen16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>aae116b01b47bca8e67679337ef48d8d3</anchor>
      <arglist>(const char16 *str)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>_tstrcmp</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a7023958e09487e98aaa402f6b4665212</anchor>
      <arglist>(const T *src, const T *dst)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>tstrcmp</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a2f01ef81ad1c686ebb34a97ef4e2d098</anchor>
      <arglist>(const char *src, const char *dst)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>strcmp8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a90dd0a8f52813eb453f60ac1c49559bc</anchor>
      <arglist>(const char8 *src, const char8 *dst)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>strcmp16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a9b47c8ef5ed96ca9034e87624f69b890</anchor>
      <arglist>(const char16 *src, const char16 *dst)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>_tstrncmp</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a9472c661ae9df487057d848006bd12b1</anchor>
      <arglist>(const T *first, const T *last, uint32 count)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>tstrncmp</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a25694d169804ad218b221f3e101eaee4</anchor>
      <arglist>(const char *first, const char *last, uint32 count)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>strncmp8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a8f3cca9e30bb739b30be3eca6b891c8f</anchor>
      <arglist>(const char8 *first, const char8 *last, uint32 count)</arglist>
    </member>
    <member kind="function">
      <type>int32</type>
      <name>strncmp16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a54115da13293d44eaae9ba44a4fa82e2</anchor>
      <arglist>(const char16 *first, const char16 *last, uint32 count)</arglist>
    </member>
    <member kind="function">
      <type>T *</type>
      <name>_tstrcpy</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a0ed99e5e2d595d1fbb46be4af6bad16e</anchor>
      <arglist>(T *dst, const T *src)</arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>tstrcpy</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a406cfbdd4e67843189e729a50a22c952</anchor>
      <arglist>(char *dst, const char *src)</arglist>
    </member>
    <member kind="function">
      <type>char8 *</type>
      <name>strcpy8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>aa12723f519b8ecf7d99b4067b6f44344</anchor>
      <arglist>(char8 *dst, const char8 *src)</arglist>
    </member>
    <member kind="function">
      <type>char16 *</type>
      <name>strcpy16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a32880d14cc3ac2196ddd0859e4fb7bd6</anchor>
      <arglist>(char16 *dst, const char16 *src)</arglist>
    </member>
    <member kind="function">
      <type>T *</type>
      <name>_tstrncpy</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a78d52df38c5128a70f98c9b832853ed9</anchor>
      <arglist>(T *dest, const T *source, uint32 count)</arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>tstrncpy</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>ae8ecddebc9de1d21e16eb0929574959a</anchor>
      <arglist>(char *dest, const char *source, uint32 count)</arglist>
    </member>
    <member kind="function">
      <type>char8 *</type>
      <name>strncpy8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a6d93eaadf6a7fa89aa51c5ee8cdae132</anchor>
      <arglist>(char8 *dest, const char8 *source, uint32 count)</arglist>
    </member>
    <member kind="function">
      <type>char16 *</type>
      <name>strncpy16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a71e109979dde381bbba56500bbde60e8</anchor>
      <arglist>(char16 *dest, const char16 *source, uint32 count)</arglist>
    </member>
    <member kind="function">
      <type>T *</type>
      <name>_tstrcat</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a71213f60ea53751e1460a66919e526f4</anchor>
      <arglist>(T *dst, const T *src)</arglist>
    </member>
    <member kind="function">
      <type>char *</type>
      <name>tstrcat</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a64e0b47b3f8b466e2cb815d6662298e0</anchor>
      <arglist>(char *dst, const char *src)</arglist>
    </member>
    <member kind="function">
      <type>char8 *</type>
      <name>strcat8</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>ac632cd3fa4f20541cabfb27d20ad2581</anchor>
      <arglist>(char8 *dst, const char8 *src)</arglist>
    </member>
    <member kind="function">
      <type>char16 *</type>
      <name>strcat16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a90f489c9a479d8c86d651dbe97f6f74f</anchor>
      <arglist>(char16 *dst, const char16 *src)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>str8ToStr16</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a43172f2a5d34fc811021a16d1a38434d</anchor>
      <arglist>(char16 *dst, const char8 *src, int32 n=-1)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>FIDStringsEqual</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>ae609f786a23774a473851d3eddaa22ff</anchor>
      <arglist>(FIDString id1, FIDString id2)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>strEmpty</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a6df75d455350d4aaadc70786730f11fa</anchor>
      <arglist>(const char *str)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>str8Empty</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a107fdab7d7c86e4f5191a27005a6bbc3</anchor>
      <arglist>(const char8 *str)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>str16Empty</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a51822113d808dc2e0594f7f1c61fa823</anchor>
      <arglist>(const char16 *str)</arglist>
    </member>
    <member kind="function">
      <type>const T &amp;</type>
      <name>Min</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>adcde5fff9b5074d3e36fc411ee865dd9</anchor>
      <arglist>(const T &amp;a, const T &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>const T &amp;</type>
      <name>Max</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>aa4b9f4d937d8d865a8bfa86e6e392505</anchor>
      <arglist>(const T &amp;a, const T &amp;b)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>Abs</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>acbbd1cb9aa59aaa62572519d5e381549</anchor>
      <arglist>(const T &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>Sign</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a8f0cb0def093b54ebe00691604f5a09b</anchor>
      <arglist>(const T &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>Bound</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a91ac21e99d77a7471db59984466167bb</anchor>
      <arglist>(T minval, T maxval, T x)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Swap</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a5d8f1525e15b2c83643fc473d75b6c19</anchor>
      <arglist>(T &amp;t1, T &amp;t2)</arglist>
    </member>
    <member kind="variable">
      <type>const FIDString</type>
      <name>kPlatformStringWin</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a6605d81276d8182104fa7df6470b22af</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const FIDString</type>
      <name>kPlatformStringMac</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a11d7b5f02c2605bcde61a18d2d69f88b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const FIDString</type>
      <name>kPlatformString</name>
      <anchorfile>namespaceSteinberg.html</anchorfile>
      <anchor>a6bbdca6c72d935ab0a455e38657b6f24</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::ConstStringTable</name>
    <filename>classSteinberg_1_1ConstStringTable.html</filename>
    <member kind="function">
      <type>char16 *</type>
      <name>getString</name>
      <anchorfile>classSteinberg_1_1ConstStringTable.html</anchorfile>
      <anchor>a9c5f9b13fb243e7f64dd0bbbf2abb602</anchor>
      <arglist>(const char8 *str) const </arglist>
    </member>
    <member kind="function">
      <type>char16</type>
      <name>getString</name>
      <anchorfile>classSteinberg_1_1ConstStringTable.html</anchorfile>
      <anchor>a729f982a922172937010363ad25ae343</anchor>
      <arglist>(const char8 str) const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ConstStringTable *</type>
      <name>instance</name>
      <anchorfile>classSteinberg_1_1ConstStringTable.html</anchorfile>
      <anchor>ac3a151a650a39a005d121605552a2a4a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>ConstStringTable</name>
      <anchorfile>classSteinberg_1_1ConstStringTable.html</anchorfile>
      <anchor>afa8d62c9b3e472508fa9bf534c7b4e8c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>~ConstStringTable</name>
      <anchorfile>classSteinberg_1_1ConstStringTable.html</anchorfile>
      <anchor>af3a1d04c31247821c3dc5006a4a9f9fb</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::FUID</name>
    <filename>classSteinberg_1_1FUID.html</filename>
    <member kind="enumeration">
      <name>UIDPrintStyle</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>ac1a5fdb65855756721145f19419a3d5e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kINLINE_UID</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>ac1a5fdb65855756721145f19419a3d5eaca9231fad9e03687405527342a551dc9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kDECLARE_UID</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>ac1a5fdb65855756721145f19419a3d5ea4e0b56687cf901dacf1633505cf5993f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kFUID</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>ac1a5fdb65855756721145f19419a3d5eae7b51c041dc8e0233aa762f55d00c3ae</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kCLASS_UID</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>ac1a5fdb65855756721145f19419a3d5ea1081fde9eea7b29c3a0cf5672f188891</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>char8</type>
      <name>String</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a6e903cd1319da6bfb8f609c2a7beb461</anchor>
      <arglist>[64]</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FUID</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a1d2f11e036cd1e964832c522596f28e2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FUID</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>ae9c9b954978decdd7b2d5c834ad82473</anchor>
      <arglist>(const TUID uid)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FUID</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>af27f09bbd3a39f103620995f02a739fe</anchor>
      <arglist>(uint32 l1, uint32 l2, uint32 l3, uint32 l4)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FUID</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a8993eac3f2a7291496110d1c371ffb5f</anchor>
      <arglist>(const FUID &amp;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~FUID</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>ac33a0c0d62496172458a40a50e381aa3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>generate</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>af204b01ca8130dcaf7f3d67b65a98d88</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isValid</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a77e8b693be38f2516b448f3e48f4ab2e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>FUID &amp;</type>
      <name>operator=</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a558369ea20400d40ea96b76be65b81ce</anchor>
      <arglist>(const FUID &amp;f)</arglist>
    </member>
    <member kind="function">
      <type>FUID &amp;</type>
      <name>operator=</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a8e1a7c2c1e0274d4afed1db997a579e3</anchor>
      <arglist>(FIDString uid)</arglist>
    </member>
    <member kind="function">
      <type>FUID &amp;</type>
      <name>operator=</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>ae76142cdae3e86a71978ef9121eed25c</anchor>
      <arglist>(TUID uid)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a4dfbedd74e7567e19b5b7a44a725dc00</anchor>
      <arglist>(const FUID &amp;f) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a70d6e878ab6e2597ba7e16efea5423a4</anchor>
      <arglist>(FIDString uid) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>ac00b358a300c18a198deb6652847623b</anchor>
      <arglist>(TUID uid) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>acd6bace86d67737d41673a46cce25e19</anchor>
      <arglist>(const FUID &amp;f) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>af0850490a3404b60fdfd6a9a9d1f54f8</anchor>
      <arglist>(FIDString uid) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator&lt;</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a75f7dfd52e31e95f8ccc98977fe05ed2</anchor>
      <arglist>(TUID uid) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a45ed2739fe398ce9c6d8cbafb2a166b6</anchor>
      <arglist>(const FUID &amp;f) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a75d346e085edb61b5a4849cd5f3efdcc</anchor>
      <arglist>(FIDString uid) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>aed637a0ce6cf5031e408453a5c1f1e10</anchor>
      <arglist>(TUID uid) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator FIDString</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>abc3f338eb8d8dff0632a2e38cefa6c21</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator char *</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>ab8dfe10792edd2ca650e210c2a52a8d5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>uint32</type>
      <name>getLong1</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a59e29db91322499dd8191b22c583168d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>uint32</type>
      <name>getLong2</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a5db9bb007d969a231732be9bbd162037</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>uint32</type>
      <name>getLong3</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a4afb577827c66c828d39ca86959ee70f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>uint32</type>
      <name>getLong4</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>ae68bd7386a953dc6a7b69adf013f650a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>from4Int</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a697401c8cecb5257280201249671d8dd</anchor>
      <arglist>(uint32 d1, uint32 d2, uint32 d3, uint32 d4)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>to4Int</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>ab0698679bdc2ea9f7ba196fce950decf</anchor>
      <arglist>(uint32 &amp;d1, uint32 &amp;d2, uint32 &amp;d3, uint32 &amp;d4) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>toString</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a93a943252302b577f071148a2a67dee8</anchor>
      <arglist>(char8 *string) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>fromString</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>aa056f457106e76080cb339f638eaa480</anchor>
      <arglist>(const char8 *string)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>toRegistryString</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>ae25ff37ab3679014ade776513766d5c6</anchor>
      <arglist>(char8 *string) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>fromRegistryString</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a7162be52a6ccdb32012486da398917e1</anchor>
      <arglist>(const char8 *string)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>print</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a769c50e87f58d165944ce81dfee493af</anchor>
      <arglist>(char8 *string=0, int32 style=kINLINE_UID) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>toTUID</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>ab7452e67eaec306e1d96708804e60b10</anchor>
      <arglist>(TUID result) const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>TUID</type>
      <name>data</name>
      <anchorfile>classSteinberg_1_1FUID.html</anchorfile>
      <anchor>a9ccb21bb150a5e101c4db9cc1fb481ee</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::FUnknown</name>
    <filename>classSteinberg_1_1FUnknown.html</filename>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>queryInterface</name>
      <anchorfile>classSteinberg_1_1FUnknown.html</anchorfile>
      <anchor>a9c89bc0ff361a583f739c5588fffdb5f</anchor>
      <arglist>(const TUID iid, void **obj)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual uint32</type>
      <name>addRef</name>
      <anchorfile>classSteinberg_1_1FUnknown.html</anchorfile>
      <anchor>a6ebe8fbd6c70f647af365ec5c07deb5b</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual uint32</type>
      <name>release</name>
      <anchorfile>classSteinberg_1_1FUnknown.html</anchorfile>
      <anchor>af2f99371476793dda3b9b0121c9f2b12</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static int32</type>
      <name>atomicAdd</name>
      <anchorfile>classSteinberg_1_1FUnknown.html</anchorfile>
      <anchor>a59abbe296f05929f39147f92107dad6e</anchor>
      <arglist>(int32 &amp;value, int32 amount)</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const FUID</type>
      <name>iid</name>
      <anchorfile>classSteinberg_1_1FUnknown.html</anchorfile>
      <anchor>a8f26dd81a1115f476537b3c30dc150a8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>Steinberg::FReleaser</name>
    <filename>structSteinberg_1_1FReleaser.html</filename>
    <member kind="function">
      <type></type>
      <name>FReleaser</name>
      <anchorfile>structSteinberg_1_1FReleaser.html</anchorfile>
      <anchor>a9797b385f344c943338ed269373b24f6</anchor>
      <arglist>(FUnknown *u)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~FReleaser</name>
      <anchorfile>structSteinberg_1_1FReleaser.html</anchorfile>
      <anchor>af02e7412c66e1a70411f24016742cbb3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>FUnknown *</type>
      <name>u</name>
      <anchorfile>structSteinberg_1_1FReleaser.html</anchorfile>
      <anchor>ad3f4ea941473a53ff09c2981772c3077</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::IPtr</name>
    <filename>classSteinberg_1_1IPtr.html</filename>
    <templarg>I</templarg>
    <member kind="function">
      <type></type>
      <name>IPtr</name>
      <anchorfile>classSteinberg_1_1IPtr.html</anchorfile>
      <anchor>af89edb28c155ef9a374f00fb117ae672</anchor>
      <arglist>(I *ptr, bool addRef=true)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>IPtr</name>
      <anchorfile>classSteinberg_1_1IPtr.html</anchorfile>
      <anchor>ae1b547d303dee02eef8c875295b01772</anchor>
      <arglist>(const IPtr &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>IPtr</name>
      <anchorfile>classSteinberg_1_1IPtr.html</anchorfile>
      <anchor>ad98db7a79b8ef82a4a580509ef1a4692</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~IPtr</name>
      <anchorfile>classSteinberg_1_1IPtr.html</anchorfile>
      <anchor>a37971c157cf11e5e282fced74a198b1b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>I *</type>
      <name>operator=</name>
      <anchorfile>classSteinberg_1_1IPtr.html</anchorfile>
      <anchor>aedcfe1dada200d74c189fab06a8234ea</anchor>
      <arglist>(I *ptr)</arglist>
    </member>
    <member kind="function">
      <type>IPtr &amp;</type>
      <name>operator=</name>
      <anchorfile>classSteinberg_1_1IPtr.html</anchorfile>
      <anchor>a6740f00483d9003162fb21e2575a4854</anchor>
      <arglist>(const IPtr &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator I *</name>
      <anchorfile>classSteinberg_1_1IPtr.html</anchorfile>
      <anchor>aa131a631893c277404b22360b8448783</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>I *</type>
      <name>operator-&gt;</name>
      <anchorfile>classSteinberg_1_1IPtr.html</anchorfile>
      <anchor>a4de5e19b56bf2c1fa37c41f970675ff8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>I *</type>
      <name>ptr</name>
      <anchorfile>classSteinberg_1_1IPtr.html</anchorfile>
      <anchor>a5a7f2c27d449527ce1b19bc2e968306d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::FUnknownPtr</name>
    <filename>classSteinberg_1_1FUnknownPtr.html</filename>
    <templarg></templarg>
    <base>Steinberg::IPtr</base>
    <member kind="function">
      <type></type>
      <name>FUnknownPtr</name>
      <anchorfile>classSteinberg_1_1FUnknownPtr.html</anchorfile>
      <anchor>adf4b465342f789c537af041ecfdc861f</anchor>
      <arglist>(FUnknown *unknown)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FUnknownPtr</name>
      <anchorfile>classSteinberg_1_1FUnknownPtr.html</anchorfile>
      <anchor>a082ea3be68a9552581eebeb28eb15881</anchor>
      <arglist>(const FUnknownPtr &amp;p)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FUnknownPtr</name>
      <anchorfile>classSteinberg_1_1FUnknownPtr.html</anchorfile>
      <anchor>aeb6984642dc0a2291f9aa74d07b56ff9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>FUnknownPtr &amp;</type>
      <name>operator=</name>
      <anchorfile>classSteinberg_1_1FUnknownPtr.html</anchorfile>
      <anchor>ae9041e3251ee181108828f5581c333f3</anchor>
      <arglist>(const FUnknownPtr &amp;p)</arglist>
    </member>
    <member kind="function">
      <type>I *</type>
      <name>operator=</name>
      <anchorfile>classSteinberg_1_1FUnknownPtr.html</anchorfile>
      <anchor>a691b97371b548384d7a39465b1b396ed</anchor>
      <arglist>(FUnknown *unknown)</arglist>
    </member>
    <member kind="function">
      <type>I *</type>
      <name>getInterface</name>
      <anchorfile>classSteinberg_1_1FUnknownPtr.html</anchorfile>
      <anchor>a1450e4f037b7882a877170f2ea0f67bc</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::FVariant</name>
    <filename>classSteinberg_1_1FVariant.html</filename>
    <member kind="enumvalue">
      <name>kEmpty</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a4f9088cdc0d1d9c59871618055f4fde4adee55cba05ca557cd5abacdd85e2c702</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kInteger</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a4f9088cdc0d1d9c59871618055f4fde4aa05a5c9579787a6ca51fb1a47b049882</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kFloat</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a4f9088cdc0d1d9c59871618055f4fde4a61f5ea0d253935e7169f8a6a1089b481</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kString8</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a4f9088cdc0d1d9c59871618055f4fde4a27bd6ccc3f4427febdf12d70b526b2a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kObject</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a4f9088cdc0d1d9c59871618055f4fde4abd435f42d0656eba094c1886f67c7c00</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kOwner</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a4f9088cdc0d1d9c59871618055f4fde4a4c2db25275e93420872f3821c143b4aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kString16</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a4f9088cdc0d1d9c59871618055f4fde4ab72139d664a511b6d6987cd8a6f3db05</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FVariant</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a1c318e7ef458bdec256a9fbe0f05f774</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FVariant</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a57463fa4e7308432391c09270c3c069c</anchor>
      <arglist>(const FVariant &amp;variant)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FVariant</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a6f0572c35f0bfafbe79d7019c6861125</anchor>
      <arglist>(int64 v)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FVariant</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a7027f49d188614d570b7a36201950f06</anchor>
      <arglist>(double v)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FVariant</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a648a07dd23abc254e0a8cc2077f1bd4f</anchor>
      <arglist>(const char8 *str)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FVariant</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a6fb8592571e03c9c44bb3c1db7dcc30b</anchor>
      <arglist>(const char16 *str)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FVariant</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a8a522a353ba213f988237a698cd81996</anchor>
      <arglist>(FUnknown *obj)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~FVariant</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a240bb85e6561473e3153814de2b5297f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>FVariant &amp;</type>
      <name>operator=</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>ab9178b734c7469431d6942b7ad292271</anchor>
      <arglist>(const FVariant &amp;variant)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setInt</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>ae03721d8d4c7f7986d53de4fe63a7a0e</anchor>
      <arglist>(int64 v)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFloat</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a6e1da72abb3d860c1e9cccdc86dd3094</anchor>
      <arglist>(double v)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setString8</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>aac08d3cd40babd0ed4a803629f0cf6eb</anchor>
      <arglist>(const char8 *v)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setString16</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>ab5979495bcb3ce86717d7c68ac87c3b9</anchor>
      <arglist>(const char16 *v)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setObject</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>ab0d7b344cb00187eb8ac7a05c3bd5796</anchor>
      <arglist>(FUnknown *obj)</arglist>
    </member>
    <member kind="function">
      <type>int64</type>
      <name>getInt</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>aca21b6079545f91b1ef1e734f22f8b9b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getFloat</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>aa44dfccf5b73bc9db07ca9e46c60e5d1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getNumber</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a62ae7af8f6d672bbde193e44d594a2db</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char8 *</type>
      <name>getString8</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a18b692f5dd013719dfdc68c857af6c70</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const char16 *</type>
      <name>getString16</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a7a0dfb561f007cba4ac4bd82221cfea3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>FUnknown *</type>
      <name>getObject</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>ac745ace0037cc341add3ff1d70e075e1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>uint16</type>
      <name>getType</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a526ad97515fb42e42f0b43e1979b714b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isEmpty</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a1b4771336a30bc9fc422d87bd1ee3c44</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isOwner</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a39076f6343957793915c84616c12a462</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isString</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>ae65c4d2af55f8c3c3f5f6ba12eeca54e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setOwner</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>aeff2c93eae7c9136671e1056eb668cb4</anchor>
      <arglist>(bool state)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>empty</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>aa6bbad33418e5c2b4d5cc0d33f82e312</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>uint16</type>
      <name>type</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a6484bce4cf03fe9af69526f921dd0b44</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>union Steinberg::FVariant::@2</type>
      <name>@3</name>
      <anchorfile>classSteinberg_1_1FVariant.html</anchorfile>
      <anchor>a07352764020cf362ee98c684143ea21d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int64</type>
      <name>intValue</name>
      <anchorfile>unionSteinberg_1_1FVariant_1_1@2.html</anchorfile>
      <anchor>ad1b8b091f9f156b9c82f6beb116771af</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>floatValue</name>
      <anchorfile>unionSteinberg_1_1FVariant_1_1@2.html</anchorfile>
      <anchor>a03bf7f9d5cb75f80b74c0f0d683973bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const char8 *</type>
      <name>string8</name>
      <anchorfile>unionSteinberg_1_1FVariant_1_1@2.html</anchorfile>
      <anchor>a6954153401a455809f14062da0ec0c75</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const char16 *</type>
      <name>string16</name>
      <anchorfile>unionSteinberg_1_1FVariant_1_1@2.html</anchorfile>
      <anchor>a4851c353bea630724d05fd9ac4b50dce</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>FUnknown *</type>
      <name>object</name>
      <anchorfile>unionSteinberg_1_1FVariant_1_1@2.html</anchorfile>
      <anchor>afa5aa487b88f4187e34ae318467a293d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::IErrorContext</name>
    <filename>classSteinberg_1_1IErrorContext.html</filename>
    <base>Steinberg::FUnknown</base>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>disableErrorUI</name>
      <anchorfile>classSteinberg_1_1IErrorContext.html</anchorfile>
      <anchor>a8bb0a2cb90a74b5de5647a44834e1e0e</anchor>
      <arglist>(bool state)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>errorMessageShown</name>
      <anchorfile>classSteinberg_1_1IErrorContext.html</anchorfile>
      <anchor>a07c0d47c4f6c70937ea0e03873831fab</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>getErrorMessage</name>
      <anchorfile>classSteinberg_1_1IErrorContext.html</anchorfile>
      <anchor>a2f19a6ac457654422a946d271e26462e</anchor>
      <arglist>(IString *message)=0</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const FUID</type>
      <name>iid</name>
      <anchorfile>classSteinberg_1_1IErrorContext.html</anchorfile>
      <anchor>a3ef2abefb2d9898223dcb46a79acfe1e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::IPluginBase</name>
    <filename>classSteinberg_1_1IPluginBase.html</filename>
    <base>Steinberg::FUnknown</base>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>initialize</name>
      <anchorfile>classSteinberg_1_1IPluginBase.html</anchorfile>
      <anchor>a22b4236771ca1bf4eacbf36791a62fd8</anchor>
      <arglist>(FUnknown *context)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>terminate</name>
      <anchorfile>classSteinberg_1_1IPluginBase.html</anchorfile>
      <anchor>a12d02321cd873881ed9b185d226fb52b</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const FUID</type>
      <name>iid</name>
      <anchorfile>classSteinberg_1_1IPluginBase.html</anchorfile>
      <anchor>a960428d8b80deb8f956839be4ce4d81b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>Steinberg::PFactoryInfo</name>
    <filename>structSteinberg_1_1PFactoryInfo.html</filename>
    <member kind="enumeration">
      <name>FactoryFlags</name>
      <anchorfile>structSteinberg_1_1PFactoryInfo.html</anchorfile>
      <anchor>a507d22f3b229b2022f993bc7a7b59978</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kNoFlags</name>
      <anchorfile>structSteinberg_1_1PFactoryInfo.html</anchorfile>
      <anchor>a507d22f3b229b2022f993bc7a7b59978a7d548ec253976cbfb1474e882272e886</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kClassesDiscardable</name>
      <anchorfile>structSteinberg_1_1PFactoryInfo.html</anchorfile>
      <anchor>a507d22f3b229b2022f993bc7a7b59978ad5b06df824e8a3c308802c026061cf72</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kLicenseCheck</name>
      <anchorfile>structSteinberg_1_1PFactoryInfo.html</anchorfile>
      <anchor>a507d22f3b229b2022f993bc7a7b59978a81726af6eb25bc931287061af0349d8e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kComponentNonDiscardable</name>
      <anchorfile>structSteinberg_1_1PFactoryInfo.html</anchorfile>
      <anchor>a507d22f3b229b2022f993bc7a7b59978aaeb9b51aa531fc0b1984ff717e8629e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kUnicode</name>
      <anchorfile>structSteinberg_1_1PFactoryInfo.html</anchorfile>
      <anchor>a507d22f3b229b2022f993bc7a7b59978aa4303207da7cb8c310c7a9170b75c809</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kURLSize</name>
      <anchorfile>structSteinberg_1_1PFactoryInfo.html</anchorfile>
      <anchor>a67a91e38ba679a9a7387fd3c4c7d9437a7364f53815792394856b8b2a8e827aa4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kEmailSize</name>
      <anchorfile>structSteinberg_1_1PFactoryInfo.html</anchorfile>
      <anchor>a67a91e38ba679a9a7387fd3c4c7d9437add7d036d5da8d903249ac5553318f35f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kNameSize</name>
      <anchorfile>structSteinberg_1_1PFactoryInfo.html</anchorfile>
      <anchor>a67a91e38ba679a9a7387fd3c4c7d9437a94e875f3780961825ba1215e875a9293</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PFactoryInfo</name>
      <anchorfile>structSteinberg_1_1PFactoryInfo.html</anchorfile>
      <anchor>a67a0bf42b1a5d01c46404d5fa2b97792</anchor>
      <arglist>(const char8 *_vendor, const char8 *_url, const char8 *_email, int32 _flags)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PFactoryInfo</name>
      <anchorfile>structSteinberg_1_1PFactoryInfo.html</anchorfile>
      <anchor>a1f2e4b53c6bdb864256444bf2c62184b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>char8</type>
      <name>vendor</name>
      <anchorfile>structSteinberg_1_1PFactoryInfo.html</anchorfile>
      <anchor>aa447769cfdd8c31083302ca3e002e550</anchor>
      <arglist>[kNameSize]</arglist>
    </member>
    <member kind="variable">
      <type>char8</type>
      <name>url</name>
      <anchorfile>structSteinberg_1_1PFactoryInfo.html</anchorfile>
      <anchor>aa33dae62c9eb24d5b382eb1aaddad234</anchor>
      <arglist>[kURLSize]</arglist>
    </member>
    <member kind="variable">
      <type>char8</type>
      <name>email</name>
      <anchorfile>structSteinberg_1_1PFactoryInfo.html</anchorfile>
      <anchor>a80cbb0d19a7cbaffe717300cf73b0684</anchor>
      <arglist>[kEmailSize]</arglist>
    </member>
    <member kind="variable">
      <type>int32</type>
      <name>flags</name>
      <anchorfile>structSteinberg_1_1PFactoryInfo.html</anchorfile>
      <anchor>a1f13b9c33d8e969fff2050e9fa39eae9</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>Steinberg::PClassInfo</name>
    <filename>structSteinberg_1_1PClassInfo.html</filename>
    <member kind="enumeration">
      <name>ClassCardinality</name>
      <anchorfile>structSteinberg_1_1PClassInfo.html</anchorfile>
      <anchor>ab00e691a05e78d718e89c814aaa4184f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kManyInstances</name>
      <anchorfile>structSteinberg_1_1PClassInfo.html</anchorfile>
      <anchor>ab00e691a05e78d718e89c814aaa4184fafd6ecd3bcdd07d25fa25da386b2150aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kCategorySize</name>
      <anchorfile>structSteinberg_1_1PClassInfo.html</anchorfile>
      <anchor>a5a573bcd9f60bdd176b7131bb96f5e0fae5d1c69a1e542e1a92b9aa7a1bb3c460</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kNameSize</name>
      <anchorfile>structSteinberg_1_1PClassInfo.html</anchorfile>
      <anchor>a5a573bcd9f60bdd176b7131bb96f5e0fa20f50cc038e77a37d7b630f57d484179</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PClassInfo</name>
      <anchorfile>structSteinberg_1_1PClassInfo.html</anchorfile>
      <anchor>a8c2cfc1456344ae3d76886308bde0253</anchor>
      <arglist>(TUID _cid, int32 _cardinality, const char8 *_category, const char8 *_name)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PClassInfo</name>
      <anchorfile>structSteinberg_1_1PClassInfo.html</anchorfile>
      <anchor>a2657d8dc5f5459a9c8bdcf3421692f7b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>TUID</type>
      <name>cid</name>
      <anchorfile>structSteinberg_1_1PClassInfo.html</anchorfile>
      <anchor>a41e877b033f5aa81a4c4acd56aa50725</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int32</type>
      <name>cardinality</name>
      <anchorfile>structSteinberg_1_1PClassInfo.html</anchorfile>
      <anchor>a19b5da3182dad2e727573456244b6d66</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char8</type>
      <name>category</name>
      <anchorfile>structSteinberg_1_1PClassInfo.html</anchorfile>
      <anchor>a77175211c92513e69557af3755688885</anchor>
      <arglist>[kCategorySize]</arglist>
    </member>
    <member kind="variable">
      <type>char8</type>
      <name>name</name>
      <anchorfile>structSteinberg_1_1PClassInfo.html</anchorfile>
      <anchor>adb09bde5607c7707c051c4b046e37008</anchor>
      <arglist>[kNameSize]</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::IPluginFactory</name>
    <filename>classSteinberg_1_1IPluginFactory.html</filename>
    <base>Steinberg::FUnknown</base>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>getFactoryInfo</name>
      <anchorfile>classSteinberg_1_1IPluginFactory.html</anchorfile>
      <anchor>a1b98fd69c4b1eac0cbd7f56f947748eb</anchor>
      <arglist>(PFactoryInfo *info)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int32</type>
      <name>countClasses</name>
      <anchorfile>classSteinberg_1_1IPluginFactory.html</anchorfile>
      <anchor>a2e8702b5a11a1ec979cb066d32526e2d</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>getClassInfo</name>
      <anchorfile>classSteinberg_1_1IPluginFactory.html</anchorfile>
      <anchor>a864b441be95a17c1ae23443b9da9c9a8</anchor>
      <arglist>(int32 index, PClassInfo *info)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>createInstance</name>
      <anchorfile>classSteinberg_1_1IPluginFactory.html</anchorfile>
      <anchor>a3e7de17db956604b968e752eba2fa72d</anchor>
      <arglist>(FIDString cid, FIDString iid, void **obj)=0</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const FUID</type>
      <name>iid</name>
      <anchorfile>classSteinberg_1_1IPluginFactory.html</anchorfile>
      <anchor>aa276d0e87064da3f86456df0eac20a2b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>Steinberg::PClassInfo2</name>
    <filename>structSteinberg_1_1PClassInfo2.html</filename>
    <member kind="enumvalue">
      <name>kVendorSize</name>
      <anchorfile>structSteinberg_1_1PClassInfo2.html</anchorfile>
      <anchor>a9614058685c643a2d93d21f9e3a15c54a68dfcd208f5ac4a906ad5394cde74b28</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kVersionSize</name>
      <anchorfile>structSteinberg_1_1PClassInfo2.html</anchorfile>
      <anchor>a9614058685c643a2d93d21f9e3a15c54add6043ba910e43a5776728cf7913589e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kSubCategoriesSize</name>
      <anchorfile>structSteinberg_1_1PClassInfo2.html</anchorfile>
      <anchor>a9614058685c643a2d93d21f9e3a15c54ac98d38e1d1bd1e2616c6418901f26fbf</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PClassInfo2</name>
      <anchorfile>structSteinberg_1_1PClassInfo2.html</anchorfile>
      <anchor>afcedb0076a58080efc7edde97b1f02f5</anchor>
      <arglist>(const TUID _cid, int32 _cardinality, const char8 *_category, const char8 *_name, int32 _classFlags, const char8 *_subCategories, const char8 *_vendor, const char8 *_version, const char8 *_sdkVersion)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PClassInfo2</name>
      <anchorfile>structSteinberg_1_1PClassInfo2.html</anchorfile>
      <anchor>a6706da8265a94efbbcda020570c6b3bf</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>TUID</type>
      <name>cid</name>
      <anchorfile>structSteinberg_1_1PClassInfo2.html</anchorfile>
      <anchor>a9c16730611f301b37b8648e808c5ad97</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int32</type>
      <name>cardinality</name>
      <anchorfile>structSteinberg_1_1PClassInfo2.html</anchorfile>
      <anchor>aad1a876cb4dc24d26ab3b7ef0db9b0b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char8</type>
      <name>category</name>
      <anchorfile>structSteinberg_1_1PClassInfo2.html</anchorfile>
      <anchor>a3471a7ad61ababf667fd756503895721</anchor>
      <arglist>[PClassInfo::kCategorySize]</arglist>
    </member>
    <member kind="variable">
      <type>char8</type>
      <name>name</name>
      <anchorfile>structSteinberg_1_1PClassInfo2.html</anchorfile>
      <anchor>a3186646d22f9b639cba71fc7cf1587a1</anchor>
      <arglist>[PClassInfo::kNameSize]</arglist>
    </member>
    <member kind="variable">
      <type>uint32</type>
      <name>classFlags</name>
      <anchorfile>structSteinberg_1_1PClassInfo2.html</anchorfile>
      <anchor>a9df411bfb166dd584ad72bac05e95f68</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char8</type>
      <name>subCategories</name>
      <anchorfile>structSteinberg_1_1PClassInfo2.html</anchorfile>
      <anchor>afd60c9f6e95848f6cabf717adf92feac</anchor>
      <arglist>[kSubCategoriesSize]</arglist>
    </member>
    <member kind="variable">
      <type>char8</type>
      <name>vendor</name>
      <anchorfile>structSteinberg_1_1PClassInfo2.html</anchorfile>
      <anchor>adc122dfa43681c3cf715af6ed2e7dbc3</anchor>
      <arglist>[kVendorSize]</arglist>
    </member>
    <member kind="variable">
      <type>char8</type>
      <name>version</name>
      <anchorfile>structSteinberg_1_1PClassInfo2.html</anchorfile>
      <anchor>a29fd3584f0f445dffedd20a9aedd3063</anchor>
      <arglist>[kVersionSize]</arglist>
    </member>
    <member kind="variable">
      <type>char8</type>
      <name>sdkVersion</name>
      <anchorfile>structSteinberg_1_1PClassInfo2.html</anchorfile>
      <anchor>ab5d5d6a14af506cf3a9ccd0dbda5ba3e</anchor>
      <arglist>[kVersionSize]</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::IPluginFactory2</name>
    <filename>classSteinberg_1_1IPluginFactory2.html</filename>
    <base>Steinberg::IPluginFactory</base>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>getClassInfo2</name>
      <anchorfile>classSteinberg_1_1IPluginFactory2.html</anchorfile>
      <anchor>ad87710f72880f19f1f7c6d035dec196a</anchor>
      <arglist>(int32 index, PClassInfo2 *info)=0</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const FUID</type>
      <name>iid</name>
      <anchorfile>classSteinberg_1_1IPluginFactory2.html</anchorfile>
      <anchor>a203a0a137e97c969f813275c4034cfdf</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>Steinberg::PClassInfoW</name>
    <filename>structSteinberg_1_1PClassInfoW.html</filename>
    <member kind="enumvalue">
      <name>kVendorSize</name>
      <anchorfile>structSteinberg_1_1PClassInfoW.html</anchorfile>
      <anchor>a513b6f0e4219701d3512e6c6d086e72ea01290627f37459a90c2a3b00907dd6b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kVersionSize</name>
      <anchorfile>structSteinberg_1_1PClassInfoW.html</anchorfile>
      <anchor>a513b6f0e4219701d3512e6c6d086e72ea707933564a1078eaa2a34c86e2a7dd61</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>kSubCategoriesSize</name>
      <anchorfile>structSteinberg_1_1PClassInfoW.html</anchorfile>
      <anchor>a513b6f0e4219701d3512e6c6d086e72eaa4d9ecd22af130e67bb67d0de0ade7cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PClassInfoW</name>
      <anchorfile>structSteinberg_1_1PClassInfoW.html</anchorfile>
      <anchor>ae12a90c565ce9b23bedb01e379acaf40</anchor>
      <arglist>(const TUID _cid, int32 _cardinality, const char8 *_category, const char16 *_name, int32 _classFlags, const char8 *_subCategories, const char16 *_vendor, const char16 *_version, const char16 *_sdkVersion)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PClassInfoW</name>
      <anchorfile>structSteinberg_1_1PClassInfoW.html</anchorfile>
      <anchor>aab2db6524de40942a44378098f96fcf5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fromAscii</name>
      <anchorfile>structSteinberg_1_1PClassInfoW.html</anchorfile>
      <anchor>a3c904f286a6ca920bb66d958176118d9</anchor>
      <arglist>(const PClassInfo2 &amp;ci2)</arglist>
    </member>
    <member kind="variable">
      <type>TUID</type>
      <name>cid</name>
      <anchorfile>structSteinberg_1_1PClassInfoW.html</anchorfile>
      <anchor>aef10c6957b5b7164d36071724b340d8a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int32</type>
      <name>cardinality</name>
      <anchorfile>structSteinberg_1_1PClassInfoW.html</anchorfile>
      <anchor>af441b643c3df6ac06f14031847c2aa08</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char8</type>
      <name>category</name>
      <anchorfile>structSteinberg_1_1PClassInfoW.html</anchorfile>
      <anchor>ad44013f70c6e11fab5059e5c5c21763d</anchor>
      <arglist>[PClassInfo::kCategorySize]</arglist>
    </member>
    <member kind="variable">
      <type>char16</type>
      <name>name</name>
      <anchorfile>structSteinberg_1_1PClassInfoW.html</anchorfile>
      <anchor>a3dca8fab57451d059bef8bc54f013dc5</anchor>
      <arglist>[PClassInfo::kNameSize]</arglist>
    </member>
    <member kind="variable">
      <type>uint32</type>
      <name>classFlags</name>
      <anchorfile>structSteinberg_1_1PClassInfoW.html</anchorfile>
      <anchor>a65664bdfcdfdc9822468f921aa75143d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char8</type>
      <name>subCategories</name>
      <anchorfile>structSteinberg_1_1PClassInfoW.html</anchorfile>
      <anchor>abb24e5623258c01de7a06941aadb97d9</anchor>
      <arglist>[kSubCategoriesSize]</arglist>
    </member>
    <member kind="variable">
      <type>char16</type>
      <name>vendor</name>
      <anchorfile>structSteinberg_1_1PClassInfoW.html</anchorfile>
      <anchor>adaa12a709c9666281a7d4b7790f58378</anchor>
      <arglist>[kVendorSize]</arglist>
    </member>
    <member kind="variable">
      <type>char16</type>
      <name>version</name>
      <anchorfile>structSteinberg_1_1PClassInfoW.html</anchorfile>
      <anchor>a75c1b20edbf7ef8a0d926d1095278e9a</anchor>
      <arglist>[kVersionSize]</arglist>
    </member>
    <member kind="variable">
      <type>char16</type>
      <name>sdkVersion</name>
      <anchorfile>structSteinberg_1_1PClassInfoW.html</anchorfile>
      <anchor>a3e1f69ba71a2a24c292674d0bb53ec92</anchor>
      <arglist>[kVersionSize]</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::IPluginFactory3</name>
    <filename>classSteinberg_1_1IPluginFactory3.html</filename>
    <base>Steinberg::IPluginFactory2</base>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>getClassInfoUnicode</name>
      <anchorfile>classSteinberg_1_1IPluginFactory3.html</anchorfile>
      <anchor>a3bacc211d1ab823fd00731b5f26524d0</anchor>
      <arglist>(int32 index, PClassInfoW *info)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>setHostContext</name>
      <anchorfile>classSteinberg_1_1IPluginFactory3.html</anchorfile>
      <anchor>af4b1bb71fc20ef3d177094e394b6f942</anchor>
      <arglist>(FUnknown *context)=0</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const FUID</type>
      <name>iid</name>
      <anchorfile>classSteinberg_1_1IPluginFactory3.html</anchorfile>
      <anchor>a5d955688bd5e979ed93cc7dcac399362</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::IHostClasses</name>
    <filename>classSteinberg_1_1IHostClasses.html</filename>
    <base>Steinberg::FUnknown</base>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>createClassInstance</name>
      <anchorfile>classSteinberg_1_1IHostClasses.html</anchorfile>
      <anchor>afa6d71fe1d08995271ecaa1e3f8fe7b6</anchor>
      <arglist>(FIDString iid, void **obj)=0</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void *</type>
      <name>createInstance</name>
      <anchorfile>classSteinberg_1_1IHostClasses.html</anchorfile>
      <anchor>a4686f93ef678a568a9b63ce38713d2d1</anchor>
      <arglist>(IHostClasses *hc, FIDString iid)</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const FUID</type>
      <name>iid</name>
      <anchorfile>classSteinberg_1_1IHostClasses.html</anchorfile>
      <anchor>a8d4cbb012e76116691580d2d4205519b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::FInstancePtr</name>
    <filename>classSteinberg_1_1FInstancePtr.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type></type>
      <name>FInstancePtr</name>
      <anchorfile>classSteinberg_1_1FInstancePtr.html</anchorfile>
      <anchor>a374a2ea379e21a059a728753231f7d92</anchor>
      <arglist>(IHostClasses *hc)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~FInstancePtr</name>
      <anchorfile>classSteinberg_1_1FInstancePtr.html</anchorfile>
      <anchor>a05a27ed0d1c87becf8767bb76d2b080a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>operator I *</name>
      <anchorfile>classSteinberg_1_1FInstancePtr.html</anchorfile>
      <anchor>ad24ce190930b3c1758150e6f95ba5c97</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>I *</type>
      <name>operator-&gt;</name>
      <anchorfile>classSteinberg_1_1FInstancePtr.html</anchorfile>
      <anchor>aaa19dd80b5f187ae724ca1ac5ef5ce14</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>I *</type>
      <name>iface</name>
      <anchorfile>classSteinberg_1_1FInstancePtr.html</anchorfile>
      <anchor>a5b19f9c0e1d800cad1b3be0a040b6037</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::IHostApplicationA</name>
    <filename>classSteinberg_1_1IHostApplicationA.html</filename>
    <base>Steinberg::FUnknown</base>
    <member kind="function" virtualness="pure">
      <type>virtual const char8 *</type>
      <name>getName</name>
      <anchorfile>classSteinberg_1_1IHostApplicationA.html</anchorfile>
      <anchor>a169a758e5f004c112fc0378fb27a5429</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const char8 *</type>
      <name>getVersion</name>
      <anchorfile>classSteinberg_1_1IHostApplicationA.html</anchorfile>
      <anchor>a2929a82aa0485844c7f55ecb319e49b9</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const char8 *</type>
      <name>getBuild</name>
      <anchorfile>classSteinberg_1_1IHostApplicationA.html</anchorfile>
      <anchor>a48dc21cf343be6346dc049ac5ed31616</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>getApplicationPath</name>
      <anchorfile>classSteinberg_1_1IHostApplicationA.html</anchorfile>
      <anchor>abc40ed947d28dd8858d220a0d0801128</anchor>
      <arglist>(void **path)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>getMainWindow</name>
      <anchorfile>classSteinberg_1_1IHostApplicationA.html</anchorfile>
      <anchor>ae0d7df5790a1522f608e918571b14eff</anchor>
      <arglist>(void **window)=0</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const FUID</type>
      <name>iid</name>
      <anchorfile>classSteinberg_1_1IHostApplicationA.html</anchorfile>
      <anchor>a00900203a63e6283175e56d7cb8cf15f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::IHostApplicationW</name>
    <filename>classSteinberg_1_1IHostApplicationW.html</filename>
    <base>Steinberg::FUnknown</base>
    <member kind="function" virtualness="pure">
      <type>virtual const char16 *</type>
      <name>getNameW</name>
      <anchorfile>classSteinberg_1_1IHostApplicationW.html</anchorfile>
      <anchor>a3d5959721b9d646c1b576b252ff621cd</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const char16 *</type>
      <name>getVersionW</name>
      <anchorfile>classSteinberg_1_1IHostApplicationW.html</anchorfile>
      <anchor>a795c2084d051242e59689dfbda6b2750</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const char16 *</type>
      <name>getBuildW</name>
      <anchorfile>classSteinberg_1_1IHostApplicationW.html</anchorfile>
      <anchor>a80a5efd23627f4c37352b38f51f90f92</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>getApplicationPathW</name>
      <anchorfile>classSteinberg_1_1IHostApplicationW.html</anchorfile>
      <anchor>a60f999d32aa4621f4ddb000225e81a12</anchor>
      <arglist>(char16 *path)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>isUnicodeApplication</name>
      <anchorfile>classSteinberg_1_1IHostApplicationW.html</anchorfile>
      <anchor>a80ad87545908cb9d8a6adb6931c2d50e</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const FUID</type>
      <name>iid</name>
      <anchorfile>classSteinberg_1_1IHostApplicationW.html</anchorfile>
      <anchor>a038cab2733bf9d1694ec84de7aac4031</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::IHostApplication2</name>
    <filename>classSteinberg_1_1IHostApplication2.html</filename>
    <base>Steinberg::FUnknown</base>
    <member kind="function" virtualness="pure">
      <type>virtual tresult</type>
      <name>loadFile</name>
      <anchorfile>classSteinberg_1_1IHostApplication2.html</anchorfile>
      <anchor>a02b63ee33dac7032e0955a08cd026953</anchor>
      <arglist>(IPath *path, IPath *projectDir=0)=0</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const FUID</type>
      <name>iid</name>
      <anchorfile>classSteinberg_1_1IHostApplication2.html</anchorfile>
      <anchor>aac94387d93c243afb7f652f0d7dd27c7</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::ISplashDisplayA</name>
    <filename>classSteinberg_1_1ISplashDisplayA.html</filename>
    <base>Steinberg::FUnknown</base>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>updateSplashText</name>
      <anchorfile>classSteinberg_1_1ISplashDisplayA.html</anchorfile>
      <anchor>a946facdc1c25a92b081026ba672eabcc</anchor>
      <arglist>(const char8 *text)=0</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const FUID</type>
      <name>iid</name>
      <anchorfile>classSteinberg_1_1ISplashDisplayA.html</anchorfile>
      <anchor>adc0cf30a2b296695437c9fd69918a528</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::ISplashDisplayW</name>
    <filename>classSteinberg_1_1ISplashDisplayW.html</filename>
    <base>Steinberg::FUnknown</base>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>updateSplashTextW</name>
      <anchorfile>classSteinberg_1_1ISplashDisplayW.html</anchorfile>
      <anchor>a270a404cb17ff9b8e9d37e0ae1ad37a1</anchor>
      <arglist>(const char16 *text)=0</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const FUID</type>
      <name>iid</name>
      <anchorfile>classSteinberg_1_1ISplashDisplayW.html</anchorfile>
      <anchor>a613285760588465d7c331d5341524752</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Steinberg::IHostVersionInfo</name>
    <filename>classSteinberg_1_1IHostVersionInfo.html</filename>
    <base>Steinberg::FUnknown</base>
    <member kind="function" virtualness="pure">
      <type>virtual int32</type>
      <name>getModelVersion</name>
      <anchorfile>classSteinberg_1_1IHostVersionInfo.html</anchorfile>
      <anchor>ace38f17c0f51a76bdec344d407fc060a</anchor>
      <arglist>(FIDString which)=0</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const FUID</type>
      <name>iid</name>
      <anchorfile>classSteinberg_1_1IHostVersionInfo.html</anchorfile>
      <anchor>a1c2c0731e096b57b1f44f4c24bee3b79</anchor>
      <arglist></arglist>
    </member>
  </compound>
</tagfile>

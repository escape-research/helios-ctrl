//-----------------------------------------------------------------------------
// Steinberg Remote Interface Software Development Kit (SDK)
// Version 2.1    Date : 2008
//
// Category     : Devices
// Filename     : idetector.h
// Created by   : Steinberg
// Description  : Device detection interface
//-----------------------------------------------------------------------------
// LICENSE
// (c) 2010, Steinberg Media Technologies GmbH, All Rights Reserved
//-----------------------------------------------------------------------------
// This Software Development Kit may not be distributed in parts or its entirety  
// without prior written agreement by Steinberg Media Technologies GmbH. 
// This SDK must not be used to re-engineer or manipulate any technology used  
// in any Steinberg or Third-party application or software module, 
// unless permitted by law.
// Neither the name of the Steinberg Media Technologies nor the names of its
// contributors may be used to endorse or promote products derived from this 
// software without specific prior written permission.
// 
// THIS SDK IS PROVIDED BY STEINBERG MEDIA TECHNOLOGIES GMBH "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL STEINBERG MEDIA TECHNOLOGIES GMBH BE LIABLE FOR ANY DIRECT, 
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//------------------------------------------------------------------------------

#ifndef __idetector__
#define __idetector__

#ifndef __funknown__
#include "../base/funknown.h"
#endif

#ifndef __ipluginbase__
#include "../base/ipluginbase.h"
#endif


//------------------------------------------------------------------------
/** \anchor componentCategoryIDs  
	\name Component categories 
	to be filled in the PClassInfo::category field */
//@{
#ifndef kDeviceDetectionClass
#define kDeviceDetectionClass "Device Detection" ///< this is the device detection category
#endif

#define kDeviceDetectionPlugAndPlay "Plug and Play" ///< this is the device detection method for plug and play devices (USB or FireWire/1394)
#define kDeviceDetectionManual "Manual" ///< this is the device detection method for devices connected via MIDI or via Ethernet

namespace Steinberg {

//----------------------------------------------------------------------
// IDetectorDeviceInfo - information about a device
//----------------------------------------------------------------------
/** Stucture with the information about the detected device type. */
struct IDetectorDeviceInfo
{
	const char* category; ///< the component category of the detected device, i.e. kDeviceClass or kRemoteDeviceClass
	const char* name; ///< name of the detected device
	const char* method; ///< method of detection, kDeviceDetectionPlugAndPlay or kDeviceDetectionManual
};

//----------------------------------------------------------------------
// interface to the detection
//----------------------------------------------------------------------
/** Device Detection interface.
[Released: SX4]
*/
class IDetector : public FUnknown
{
public:
	/** called when the host requires device detection */
	virtual tresult PLUGIN_API detect () = 0;

	/** retrieve information about the device type the detector will detect.*/
	virtual tresult PLUGIN_API getDeviceTypeInfo (struct IDetectorDeviceInfo* devInfo) = 0;
//------------------------------------------------------------------------
	static const FUID iid;
};

DECLARE_CLASS_IID (IDetector, 0xCD115F16, 0xAD8243A5, 0xA355A7CB, 0xAFBD5D0C)

}

#endif

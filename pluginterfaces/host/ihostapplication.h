//-----------------------------------------------------------------------------
// Project     : SDK Core
// Version     : 1.0
//
// Category    : SDK Core Interfaces
// Filename    : pluginterfaces/host/ihostapplication.h
// Created by  : Steinberg, 01/2004
// Description : Basic Host Interfaces
//
//-----------------------------------------------------------------------------
// LICENSE
// (c) 2010, Steinberg Media Technologies GmbH, All Rights Reserved
//-----------------------------------------------------------------------------
// This Software Development Kit may not be distributed in parts or its entirety  
// without prior written agreement by Steinberg Media Technologies GmbH. 
// This SDK must not be used to re-engineer or manipulate any technology used  
// in any Steinberg or Third-party application or software module, 
// unless permitted by law.
// Neither the name of the Steinberg Media Technologies nor the names of its
// contributors may be used to endorse or promote products derived from this 
// software without specific prior written permission.
// 
// THIS SDK IS PROVIDED BY STEINBERG MEDIA TECHNOLOGIES GMBH "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL STEINBERG MEDIA TECHNOLOGIES GMBH BE LIABLE FOR ANY DIRECT, 
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//------------------------------------------------------------------------------

#ifndef __ihostapplication__
#define __ihostapplication__

#ifndef __funknown__
#include "../base/funknown.h"
#endif

namespace Steinberg {

//------------------------------------------------------------------------  
/*! \defgroup hostInterfacesBasic Basic Host Interfaces

All interfaces provided by the host application are called "Host interfaces". \n
When a plugin component is initialized, it will be passed a so called 'context'. 
From the context, the Plug-In can query a number of interfaces. It depends on 
the Plug-In category what interfaces the context really contains, but any
context contains the following ones:

	\htmlonly <hr width="100%" size="2" align="left" /> \endhtmlonly 
\b Steinberg::IHostApplication \n \copydoc Steinberg::IHostApplication 
	\htmlonly <hr width="100%" size="2" align="left" /> \endhtmlonly 
\b Steinberg::IHostClasses \n \copydoc Steinberg::IHostClasses
*/
//------------------------------------------------------------------------  


//------------------------------------------------------------------------  
/** Information about the host.
[host imp] \n
[this interface is usually passed to IPluginBase::initialize as context]
- get information about host (name, version, build)
- get application path
- get main window

\ingroup hostInterfacesBasic
*/
//------------------------------------------------------------------------
class IHostApplicationA: public FUnknown
{
public:
//------------------------------------------------------------------------
	/** Get the application name (eg "Nuendo") */
	virtual const char8* PLUGIN_API getName () = 0;    
	/** Get the application version as string (eg "Version 2.0.2") */
	virtual const char8* PLUGIN_API getVersion () = 0;  
	/** Get build number (as string, eg "10") */
	virtual const char8* PLUGIN_API getBuild () = 0;    

	/** Directory where application is installed (as ascii string) */ 
	virtual tresult PLUGIN_API getApplicationPath (void** path) = 0;  

	/** Get platform handle to main window of the application */ 
	virtual tresult PLUGIN_API getMainWindow (void** window) = 0;     
//------------------------------------------------------------------------
	static const FUID iid;
};
DECLARE_CLASS_IID (IHostApplicationA, 0xCC0B7716, 0x362B4EE5, 0x89EE5F41, 0x5458463F)

//------------------------------------------------------------------------
/** Unicode version of host information.
*/
//------------------------------------------------------------------------
class IHostApplicationW: public FUnknown
{
public:
//------------------------------------------------------------------------
	/** Get the application name (eg "Nuendo") */
	virtual const char16* PLUGIN_API getNameW () = 0;    
	/** Get the application version as string (eg "Version 2.0.2") */
	virtual const char16* PLUGIN_API getVersionW () = 0;  
	/** Get build number (as string, eg "10") */
	virtual const char16* PLUGIN_API getBuildW () = 0;    

	/** Directory where application is installed (as unicode string) */ 
	virtual tresult PLUGIN_API getApplicationPathW (char16* path) = 0;  

	/** Returns kResultTrue if application was built with UNICODE defined*/
	virtual tresult PLUGIN_API isUnicodeApplication () = 0;  
//------------------------------------------------------------------------
	static const FUID iid;
};

DECLARE_CLASS_IID (IHostApplicationW, 0x97E507DA, 0xE5594EEA, 0xA5AC4042, 0xE458F2B4)

//------------------------------------------------------------------------  
/** Information about the host.
[host imp] \n
[this interface is usually passed to IPluginBase::initialize as context] \n
[released: S3.2]
- open a file

\ingroup hostInterfacesBasic
*/
//------------------------------------------------------------------------
class IPath;

class IHostApplication2: public FUnknown
{
public:
//------------------------------------------------------------------------
	/** Open a file (can be any project file, that the host can open or import) */
	virtual tresult PLUGIN_API loadFile (IPath* path, IPath* projectDir = 0) = 0;
//------------------------------------------------------------------------
	static const FUID iid;
};

DECLARE_CLASS_IID (IHostApplication2, 0x43B13CC1, 0x1B754432, 0xA09CD952, 0x7C1DD0AC)


//------------------------------------------------------------------------  
/** Information about the host.
[host imp] \n
[this interface is usually passed to IPluginBase::initialize as context] \n
[released: SX4]
- show info on startup screen

\ingroup hostInterfacesBasic
*/
//------------------------------------------------------------------------
class ISplashDisplayA : public FUnknown
{
public:
//------------------------------------------------------------------------
	/** Update info text on application startup screen. */
	virtual void PLUGIN_API updateSplashText (const char8* text) = 0;
//------------------------------------------------------------------------
	static const FUID iid;
};
DECLARE_CLASS_IID (ISplashDisplayA, 0x5A104B3A, 0x09D34D46, 0xA3979A37, 0x5775A157)

//------------------------------------------------------------------------  
/** Unicode version of ISplashDisplay.
*/
//------------------------------------------------------------------------
class ISplashDisplayW : public FUnknown
{
public:
//------------------------------------------------------------------------
	/** Update info text on application startup screen. */
	virtual void PLUGIN_API updateSplashTextW (const char16* text) = 0;
//------------------------------------------------------------------------
	static const FUID iid;
};

DECLARE_CLASS_IID (ISplashDisplayW, 0x915B5C7B, 0x37A247B6, 0xB5421F40, 0xFF823CF5)

//------------------------------------------------------------------------  
/** Information about the host data model version.
[host imp] \n
[this interface is usually passed to IPluginBase::initialize as context] \n
[released: N5] \n
\n
Get information about the data model structure version. While VST-MA interfaces
stay the same, the underlying datamodel can change from host version to host version.
Some interfaces have a generic access to this model (e.g IHostController) where IDs are
used to identify objects. When these IDs or the structure how objects are orgnized change,
a component can test this by comparing the data model version. 

\ingroup hostInterfacesBasic
*/

//------------------------------------------------------------------------
class IHostVersionInfo : public FUnknown
{
public:
//------------------------------------------------------------------------
	/** Get version of data model. 
	    \param which is an identifier for the data model part of interest. Defines for this
		parameter can be found along the according interface definitions.
		\return the version number, or -1 on error */
	virtual int32 PLUGIN_API getModelVersion (FIDString which) = 0;
//------------------------------------------------------------------------
	static const FUID iid;
};
DECLARE_CLASS_IID (IHostVersionInfo, 0x44AD7BC8, 0x24FB43DB, 0xBB6759C3, 0x8233492A)


}

#endif

//-----------------------------------------------------------------------------
// Project     : SDK Core
// Version     : 1.0
//
// Category    : SDK Core Interfaces
// Filename    : pluginterfaces/host/ihostclasses.h
// Created by  : Steinberg, 01/2004
// Description : Factory of application objects and interfaces
//
//-----------------------------------------------------------------------------
// LICENSE
// (c) 2010, Steinberg Media Technologies GmbH, All Rights Reserved
//-----------------------------------------------------------------------------
// This Software Development Kit may not be distributed in parts or its entirety  
// without prior written agreement by Steinberg Media Technologies GmbH. 
// This SDK must not be used to re-engineer or manipulate any technology used  
// in any Steinberg or Third-party application or software module, 
// unless permitted by law. It may only be used for development of products 
// defined in the license agreement between Steinberg and Licensee or for internal, 
// not publicly released products of Licensee.
//----------------------------------------------------------------------------------

#ifndef __ihostclasses__
#define __ihostclasses__

#ifndef __funknown__
#include "../base/funknown.h"
#endif

namespace Steinberg {

//------------------------------------------------------------------------
/** Factory of application objects and interfaces.
\ingroup hostInterfacesBasic
[host imp] \n
[this interface is passed as context to a plugin via IPluginBase::initialize]
- create instances of application objects
- get access interfaces to existing application objects

<b>This interface is the main entry point to any public interface provided
by the host! </b>
\see FHostCreate, FInstancePtr
*/
//------------------------------------------------------------------------
class IHostClasses: public FUnknown
{
public:
//------------------------------------------------------------------------
	/** Create instances of application objects and create interfaces to global application objects 
	\param iid : (in) the class id of the class to create (eg IPath::iid)
	            or the interface id of the global instance (eg IFileSystem::iid)
	\param obj : (out) if the method returns without error, 'obj' points to the requested interface */
	virtual tresult PLUGIN_API createClassInstance (FIDString iid, void** obj) = 0;

//------------------------------------------------------------------------
	/** Helper function for macro FHostCreate */
	static inline void* createInstance (IHostClasses* hc, FIDString iid)
	{
		void* obj = 0;
		if (hc)
			hc->createClassInstance (iid, &obj);
		return obj;
	}

//------------------------------------------------------------------------
	static const FUID iid;
};

DECLARE_CLASS_IID (IHostClasses, 0xEEC89683, 0x80DB4336, 0x9177273E, 0x138A763B)
}

//------------------------------------------------------------------------
// FHostCreate
//------------------------------------------------------------------------
/** Convenience macro for usage of IHostClasses.
\ingroup hostInterfacesBasic
example:
																		\code
tresult PLUGIN_API ExampleComponent::initialize (FUnknown* context)
{
	IHostClasses* hostClasses = 0;	
	context->queryInterface (IHostClasses::iid, (void**)&hostClasses);
	if (!hostClasses)
		return kResultFalse;

	IPath* path = FHostCreate (IPath, hostClasses)
	path->setFullPath ("C:\\");
	path->release ();
	return kResultOK;
}																		
																		\endcode 
*/
//------------------------------------------------------------------------
#define FHostCreate(Interface, hostClasses) \
	(Interface*) Steinberg::IHostClasses::createInstance (hostClasses, Interface::iid);

namespace Steinberg {

//------------------------------------------------------------------------
/** Convenience template class for declaring a pointer to a global interface.
\ingroup hostInterfacesBasic
example:
																	\code
	IHostClasses* hostClasses = ...;	
	FInstancePtr <IUpdateHandler> updateHandler (hostClasses);
	if (updateHandler)
		updateHandler->addDependent (object, this);
																	\endcode
*/
//------------------------------------------------------------------------
template <class I> class FInstancePtr
{
public:
//------------------------------------------------------------------------
	inline FInstancePtr (IHostClasses* hc){	iface = FHostCreate (I, hc);}
	inline ~FInstancePtr (){ if (iface) iface->release ();}

	inline operator I* ()  const { return iface; }      // act as I*
	inline I* operator->() const { return iface; }      // act as I*
//------------------------------------------------------------------------
protected:
	I* iface;
};

}

#endif


//---------------------------------------------------------------------------------
// Steinberg Module Architecture SDK
// Version 1.0    Date : 2005
//
// Category     : SKI
// Filename     : iportmanager.h
// Created by   : Mario Ewald
// Description  : 
//---------------------------------------------------------------------------------

//---------------------------------------------------------------------------------
// Steinberg Kernel Interface Software Development Kit (SDK)
// (c) 2010, Steinberg Media Technologies GmbH
//
// This Software Development Kit may not be distributed in parts or its entirety  
// without prior written agreement by Steinberg Media Technologies GmbH. 
// This SDK must not be used to re-engineer or manipulate any technology used  
// in any Steinberg or Third-party application or software module, 
// unless permitted by law. It may only be used for development of products 
// defined in the license agreement between Steinberg and Licensee or for internal, 
// not publicly released products of Licensee.
//----------------------------------------------------------------------------------

#ifndef __iportmanager__
#define __iportmanager__

#include "../../base/funknown.h"
#include "../../base/istringresult.h"

namespace Steinberg {

typedef FIDString PPortType;

// main port types
const PPortType kMidiPortType = "midi";   ///< midi ports
const PPortType kAudioPortType = "audio"; ///< audio ports

// sub types
const PPortType kSystemPortType = "system";   ///< OS system ports / ASIO ports
const PPortType kExternPortType = "extern";   ///< rewire / syslink
const PPortType kProxyPortType = "proxy";     ///< midi device ports (representing other ports)
const PPortType kVirtualPortType = "virtual"; ///< vst instrument / other software ports
const PPortType kMergedPortType = "merged";   ///< eg. all midi inputs
const PPortType kSidePortType = "side";		  ///< sidechain inputs
const PPortType kAliasPortType = "alias";	  ///< temporaray port representing a missing port

class IPortRegistryNotification;

//------------------------------------------------------------------------
/** Access to system ports. 
\ingroup devices
[host imp] \n
[released: SX4] \n

This interface represents a port. 
Ports are collected in the IPortRegistry.

\see IPortRegistry
*/
//------------------------------------------------------------------------
class IPort : public FUnknown
{
public:
//------------------------------------------------------------------------
	/** Each port has a unique identifier string. Such a string can be used
	    to find a port in the IPortRegistry*/
	virtual bool PLUGIN_API getPortIdentifier (IStringResult* result) = 0;

	/** Query the basic port type. This can be kMidiPortType or kAudioPortType */
	virtual bool PLUGIN_API isPortType (PPortType type) = 0; 
	/** The port category, eg. kSystemPortType */
	virtual bool PLUGIN_API isSubType (PPortType type) = 0;

	/** The direction of the port (input or output). */
	virtual bool PLUGIN_API isSystemInput () = 0; 
	
	/** For system ports: How the system names the port. */
	virtual const tchar* PLUGIN_API getSysName () = 0;

	/** The id string of the device that owns the port. */
	virtual FIDString PLUGIN_API getDeviceID () = 0;
	
	/** How the port is displayed in menus. */
	virtual const tchar* PLUGIN_API getDisplayName () = 0;

	/** Change the display name. */
	virtual bool PLUGIN_API setDisplayName (const tchar*) = 0;
		
	/** A port can be hidden. That means that it is not listed in menus. */
	virtual bool PLUGIN_API isVisible () = 0;

	/** Show/hide the port. */
	virtual void PLUGIN_API setVisible (bool state) = 0;

	/** Tell if the port is connected. */
	virtual bool PLUGIN_API isUsed () = 0;

//------------------------------------------------------------------------
	static const FUID iid;
};

#ifdef UNICODE
DECLARE_CLASS_IID (IPort, 0x6A0C2E5F, 0xDE7340C3, 0x86AA5B88, 0x1C425098)
#else
DECLARE_CLASS_IID (IPort, 0xC7241571, 0x2D484403, 0x865285D9, 0xE162758D)
#endif

//------------------------------------------------------------------------
/** Access to all registered ports.
\ingroup devices
[host imp] \n
[get this interface from IHostClasses] \n
[released: SX4] \n

\see IPort
*/
//------------------------------------------------------------------------
class IPortRegistry : public FUnknown
{
public:	
//------------------------------------------------------------------------
	/** Get number of ports */
	virtual int32 PLUGIN_API countPorts () = 0;
	/** Get the nth port */
	virtual IPort* PLUGIN_API getPortByIndex (int32 index) = 0;

	/** Query a port by its identifier. \see IPort::getPortIdentifier () */
	virtual IPort* PLUGIN_API queryPort (FIDString portUID) = 0;

	/** Unregister a port. When a port is removed from the registry it gets hidden
	    from the application and will not be listed in port setup views (Device Setup).
		This is a stronger hiding than filtering the port in menus as with IPort::setVisible ()*/
	virtual tresult PLUGIN_API removePort (IPort* port) = 0;
	
	/** Create a port identifier string from the given information. */
	virtual tresult PLUGIN_API makePortID (IStringResult* result,
	                         FIDString deviceName,
	                         FIDString portSystemName,
	                         bool isInputPort) = 0;

//------------------------------------------------------------------------
	static const FUID iid;
};

DECLARE_CLASS_IID (IPortRegistry, 0xB6C29A8E, 0x5476493E, 0xA8E99F1C, 0xB51AB756)

//------------------------------------------------------------------------
/** Extended access to registered ports.
\ingroup devices
[host imp] \n
[get this interface from IHostClasses] \n
[released: N4.2.1] \n
*/
//------------------------------------------------------------------------
class IPortRegistry2 : public IPortRegistry
{
public:	
//------------------------------------------------------------------------
	/** Install a notification interface. No refcounting is done.
	    It must be removed when the implementing component terminates.*/
	virtual void registerNotification (IPortRegistryNotification*) = 0;
	/** Remove a notification interface.*/
	virtual void unregisterNotification (IPortRegistryNotification*) = 0;

	/** Message IDs specifying the kind of port change. Passed
	    as parameter msg to IPortRegistryNotification::onPortChange*/
	enum PortChangeMsg {
		kPortAddRemove = 100, ///< new port added or port removed
		kPortName,            ///< the name of a port has changed
		kPortVisibility,      ///< port was hidden or shown
		kPortConnection       ///< the port connection has changed
	};

//------------------------------------------------------------------------
	static const FUID iid;
};

DECLARE_CLASS_IID (IPortRegistry2, 0x1F3053C4, 0xD6B54559, 0xA92DF18A, 0x4B3F2F92)


//------------------------------------------------------------------------
/** A connector establishes a connection to a connectable object.
\ingroup devices
[host imp] \n
[Extends IDevice] \n
[released: SX4] \n

purpose: 
- connect midi ports to midi/remote devices
- connect audio channels to each other
*/
//------------------------------------------------------------------------
class IConnector : public FUnknown
{
public:	
//------------------------------------------------------------------------
	/** Connect to the passed peer. 
	- When the IConnector is a midi device midi ports can be connected
	- When the IConnector is an audio channel input-, output- and group-channels can be connected. \n
	  It can be a Vst::IBusDescriptor as well (since host 4.2) */
	virtual tresult PLUGIN_API connectTo (FUnknown* peer) = 0;

	/** Check, if the peer is already connected*/
	virtual bool PLUGIN_API isConnected (FUnknown* peer) = 0;

//------------------------------------------------------------------------
	static const FUID iid;
};

DECLARE_CLASS_IID (IConnector, 0x52F474D9, 0xA7284CA5, 0xA9CC0C30, 0x7B17127A)



//------------------------------------------------------------------------
/** Port Registry Notification.
\ingroup devices
[released: N4.2.1] \n
Install a IPortRegistryNotification via IPortRegistry2 to get informed
when a port changes. 
*/
//------------------------------------------------------------------------
class IPortRegistryNotification
{
public:
//------------------------------------------------------------------------
	/** A port has changed. 
	  \param port: the changed port
	  \param msg: a value of IPortRegistry2::PortChangeMsg */
	virtual void onPortChange (IPort* port, uint32 msg) = 0; 
//------------------------------------------------------------------------
};

}

#endif
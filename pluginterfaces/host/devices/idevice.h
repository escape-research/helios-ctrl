//---------------------------------------------------------------------------------
// Steinberg Module Architecture SDK
// Version 1.0    Date : 2004
//
// Category     : SKI
// Filename     : idevice.h
// Created by   : Mario Ewald
// Description  : Device Access
//---------------------------------------------------------------------------------

//---------------------------------------------------------------------------------
// Steinberg Kernel Interface Software Development Kit (SDK)
// (c) 2010, Steinberg Media Technologies GmbH
//
// This Software Development Kit may not be distributed in parts or its entirety  
// without prior written agreement by Steinberg Media Technologies GmbH. 
// This SDK must not be used to re-engineer or manipulate any technology used  
// in any Steinberg or Third-party application or software module, 
// unless permitted by law. It may only be used for development of products 
// defined in the license agreement between Steinberg and Licensee or for internal, 
// not publicly released products of Licensee.
//----------------------------------------------------------------------------------


#ifndef __idevice__
#define __idevice__

#ifndef __funknown__
#include "../../base/funknown.h"
#endif

namespace Steinberg {

class IDevice;
class IValue;
//------------------------------------------------------------------------
/** Access to all devices used.
\ingroup devices
[host imp] \n
[get this interface from IHostClasses] \n
[released: N3.0]

- list all accessible devices

\see IDevice

*/
//------------------------------------------------------------------------
class IDeviceList : public FUnknown
{
public:
//------------------------------------------------------------------------
	/** Get number of root devices */
	virtual int32 countDevices () = 0;
	/** Get the nth root device */
	virtual IDevice* getDeviceByIndex (int32 index) = 0;

	/** Get number of devices of a specific class. There is no official list of device classes.
	    If you need more information about the id of a specific device, please contact Steinberg.*/
	virtual int32 countClassDevices (FIDString deviceID) = 0; 
	/** Get nth device of a specific class. */
	virtual IDevice* getDeviceByClassID (FIDString deviceID, int32 index) = 0; 

//------------------------------------------------------------------------
	static const FUID iid;
};

DECLARE_CLASS_IID (IDeviceList, 0x7F60CB8A, 0xB7784BB5, 0x9465FBB7, 0xF9CCC827)


//------------------------------------------------------------------------
/** Extension of IDeviceList. 
\ingroup devices
[host imp] \n
[get this interface from IHostClasses] \n
[released: SX4]

- automatic installing/uninstalling of devices defined in component. Rule is: when
a component has installed a device, it must install it always. The host will
never try to re-install such a device on the next program start.

\see IDevice, IDeviceList
*/
//------------------------------------------------------------------------
class IDeviceList2 : public IDeviceList
{
public:
//------------------------------------------------------------------------
	/** Allow plugin component to install a device.
	\param cid : class ID (FUID) of component - must be exported by the component's class registry
	\param index : this paramter is usually 0. Only if you want to install a device more than one time,
		the index identifies the different instances.
	\param isPublic : when set to 'true' the device will be listed in the 'Studio Setup Dialog' */
	virtual IDevice* installDeviceByClassId (FIDString cid, int32 index, bool isPublic) = 0;
	/** Allow plugin component to install a device by passing category and name.*/
	virtual IDevice* installDeviceByName (FIDString category, FIDString name, int32 index, bool isPublic) = 0;
	
	/** Allow plugin to uninstall a device.*/
	virtual tresult uninstallDevice (IDevice*) = 0;

//------------------------------------------------------------------------
	static const FUID iid;
};

DECLARE_CLASS_IID (IDeviceList2, 0xA2370BD2, 0xDF0D4030, 0x90A65AA5, 0x7A3F3172)



//------------------------------------------------------------------------
/** Automation write on/off messages.
- sent to IDependent::update when IValue changes its automation recording state
*/
//------------------------------------------------------------------------
enum AutomationWriteMessages
{
	kMsgParamBeginAutoRecord = 7000,
	kMsgParamEndAutoRecord,
	kMsgParamBeginAutoPreview,
	kMsgParamEndAutoPreview
};

//------------------------------------------------------------------------
/** Definitions for specific device node classes.
    Used with IDevice::getDeviceClass
*/
//------------------------------------------------------------------------
namespace IDeviceNode
{
	typedef FIDString DeviceClass;
	static const DeviceClass kChannels = "Channels";
}

//------------------------------------------------------------------------
/** Interface to a device.
\ingroup devices
[host imp] \n
[released: N300] \n

- get basic information about the device
- access parameters of the device
- access subdevices of the device

\see IDeviceList
*/
//------------------------------------------------------------------------
class IDevice : public FUnknown
{
public:
	/** The class to which the device belongs. \see IDeviceList::getDeviceByClassID */
	virtual FIDString getDeviceClass () = 0; 
	/** The (localized) name of the device. */
	virtual const tchar* getTitle () = 0; 

	/// \name Parameters
	//@{
	/** Get the number of parameters of the device. */
	virtual int32 countParameters () = 0;
	/** Get the tag of the nth parameter. A tag is the internal identifier for a parameter */
	virtual int32 getParameterTag (int32 index) = 0;
	/** Get the (localized) title of a parameter */
	virtual const tchar* getParameterTitle (int32 tag) = 0;

	/** Create an IValue interface to any of a devices value objects. 
		The IValue must be released! */
	virtual IValue* createParamInterfaceByID (FIDString paramId) = 0; 
	
	/** Create an IValue interface to any of a devices value objects. 
		The IValue must be released! */
	virtual IValue* createParamInterfaceByTag (int32 tag) = 0; 
	//@}

	/// \name Sub Devices
	//@{
	/** Get number of subdevices (eg. channels) of the device.*/
	virtual int32 countSubDevices () = 0;
	/** Get the nth subdevice of the device.*/
	virtual IDevice* getSubDevice (int32 index) = 0;
	//@}

//------------------------------------------------------------------------
	static const FUID iid;
};

#ifdef UNICODE
DECLARE_CLASS_IID (IDevice, 0xAA9BD1AE, 0x0EF94229, 0xB79F96F0, 0xA9BB031D)
#else
DECLARE_CLASS_IID (IDevice, 0x967572B2, 0x3AA243EB, 0xA4ECACCC, 0xA81F8205)
#endif
//------------------------------------------------------------------------
/** Extended interface to a device.
\ingroup devices
[Extends IDevice]
[host imp] \n
[released: S310] \n

	- get identifier string for device
	- get short parameter title
*/
//------------------------------------------------------------------------
class IDevice2 : public IDevice 
{
public:
	/** Get an identifier for this device in form of a path string.
		Caller does not own the string! */
	virtual FIDString getIDPath () = 0;	

	/** Get the (localized) abbreviation title of a parameter.
	    \param tag : (in) the parameter's identifier
		\param maxLen : (in) maximal length of the result string, can be -1 for default length*/
	virtual const tchar* getParameterShortTitle (int32 tag, int32 maxLen) = 0;

//------------------------------------------------------------------------
	static const FUID iid;
};

#ifdef UNICODE
DECLARE_CLASS_IID (IDevice2, 0x6A55D22C, 0xAF994846, 0xA5055148, 0x28B17F11)
#else
DECLARE_CLASS_IID (IDevice2, 0x727BCA64, 0xD0EE4253, 0x89BB3B0C, 0x300EAC53)
#endif

//------------------------------------------------------------------------
/** Set global Solo mode.
\ingroup devices
[get this interface from IHostClasses] \n
[host imp] \n
[released: S310] \n

	- get/set solo mode (used when changed IValues with tag kParamSolo)

*/
//------------------------------------------------------------------------
class ISoloModeHandler : public FUnknown
{
public:
//------------------------------------------------------------------------
	enum SoloMode 
	{ 
		kSoloUseGlobal, 
		kSoloIndependent, 
		kSoloExclusive, 
		kSoloDefeat 
	};

	virtual void setSoloMode (SoloMode mode) = 0; 
	virtual SoloMode getSoloMode () = 0;

//------------------------------------------------------------------------
	static const FUID iid;
};

DECLARE_CLASS_IID (ISoloModeHandler, 0x9CA540D4, 0xF5D54822, 0xA7CD4CEC, 0x32EB4212)

//------------------------------------------------------------------------
/** Set global Listen mode.
\ingroup devices
[get this interface from IHostClasses] \n
[host imp] \n
[released: N4] \n

	- get/set solo mode for Listen feature (used when changed IValues with tag kParamListen)
*/
//------------------------------------------------------------------------
class IListenModeHandler : public ISoloModeHandler
{
public:
	static const FUID iid;
};
DECLARE_CLASS_IID (IListenModeHandler, 0xF78574E8, 0xC19449FB, 0x9B38D4ED, 0xD09A728D)

}

#endif

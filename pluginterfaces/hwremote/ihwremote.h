//-----------------------------------------------------------------------------
// Steinberg Remote Interface Software Development Kit (SDK)
// Version 2.2    Date : 2009
// Version 2.1    Date : 2008
// Version 2.0b   Date : 2007
// Version 1.0    Date : 2005
//
// Category     : HW Remote Controller
// Filename     : ihwremote.h
// Created by   : Steinberg
// Description  : HW Remote Controller module
//-----------------------------------------------------------------------------
// LICENSE
// (c) 2010, Steinberg Media Technologies GmbH
//-----------------------------------------------------------------------------
// This Software Development Kit may not be distributed in parts or its entirety 
// without prior written agreement by Steinberg Media Technologies GmbH. This
// SDK must not be used to re-engineer or manipulate any technology used in any
// Steinberg or Third-party application or software module, unless permitted by
// law.
//-----------------------------------------------------------------------------

#ifndef __ihwremote__
#define __ihwremote__

#ifndef __funknown__
#include "../base/funknown.h"
#endif

#ifndef __ipluginbase__
#include "../base/ipluginbase.h"
#endif


//------------------------------------------------------------------------
#ifndef kRemoteDeviceClass
#define kRemoteDeviceClass "Remote Devices" // component registry ID
#endif

namespace Steinberg {

//----------------------------------------------------------------------
// PHWRemoteControl - describes a control
// It is optimized for MIDI controls, but might be used for other input data too
//----------------------------------------------------------------------
/** Stucture with the data identifying a control object on the remote control. */
struct PHWRemoteControl
{
	const char8 *name;	 ///< descriptive name of the control
	uint8  status;   ///< MIDI status byte (and special codings) see \ref PHWRemoteControlStatus
	uint8  channel;  ///< MIDI channel
	uint16 addr;     ///< controller address, note number
	uint32  maxValue; ///< max. value of the MIDI controller
	uint32  flags;    ///< some additional qualifier see \ref PHWRemoteControlFlags
};

// describe behaviour of the control
/** Flags for a control object. \anchor PHWRemoteControlFlags*/
enum PHWRemoteControlFlags
{
	kControlReceive            = 1 << 0, ///< a message will be receive on the host
	kControlTransmit           = 1 << 1, ///< a message will be transmitted from the host
	kControlRelative           = 1 << 2, ///< increment,decrement controller - 2nd complement coding of 7 bit (0x7f is -1, 0x01 is 1)
	kControlNoFeedback         = 1 << 3, ///< fader or dial without motor (computer can't set control)
	kControlMSBLSB             = 1 << 4, ///< CC MSB/LSB required (for MIDI used only, indication for 14 bit handling, CC 0 + CC 32 create 14 bit data)
	kControlRequiresRetransmit = 1 << 5  ///< requires retransmit of message after reception of a message (on default this is prevented)
};

// Event Control Status values which instructs the MIDI decoder to evaluate the incoming data in a special way
/** Declaration of supported MIDI status types according to the. \anchor PHWRemoteControlStatus
 MIDI Standard Specification. And special decoding methods for common controller messages used with remote controls */
enum PHWRemoteControlStatus
{
	// MIDI status bytes
	kMIDINoteOff = 0x80,           ///< note, off velocity
	kMIDINoteOn = 0x90,            ///< note, on velocity
	kMIDIPolyPressure = 0xA0,      ///< note, pressure
	kMIDIController = 0xB0,        ///< controller, value
	kMIDIProgramChange = 0xC0,     ///< program
	kMIDIAfterTouch = 0xD0,        ///< pressure
	kMIDIPitchBend = 0xE0,         ///< lsb, msb

	kMIDISysexStart = 0xF0,        ///< data byte until sysexend
	kMIDISysexEnd = 0xF7,          ///< sysex end identifier


	// internal special cases for MIDI message decoding
	kControlRPN  = 1,	 ///< treat controller 0x65/0x64 as RPN (registered parameter number)
	kControlNRPN = 2,	 ///< treat controller 0x63/0x62 as NRPN (non registered parameter number)
	kControlJLCCtrl = 3, ///< treat controller as JL Cooper style button (CC addr 0,1,2 for state information and value as button address)
	kControlText = 4,    ///< treat controller as text display, console specific driver need to implement formatting (transmit only)
	kControlMMC = 5,     ///< treat controller as MMC command (obsolete)
	kControlHouston = 6  ///< treat controller as Houston switch (combine CC Addr and value for address and use lowest bit for state)
};

//----------------------------------------------------------------------
// PHWRemoteValue - value description for a control
//----------------------------------------------------------------------
/** Stucture with the data of a host parameter connected to a remote control object */
struct PHWRemoteValue
{
	int32   flags;   ///< properties of the value, see \ref PHWRemoteValueFlags
	int32   style;	///< style information about the value (switch, gain, freq, level, etc.) see \ref PHWRemoteValueStyles

	union
	{
		// integer representation
		struct intData
		{
			int32   iValue;   ///< current value as integer
			int32   iMin;     ///< minimum possible value (inclusive)
			int32   iMax;     ///< maximum possible value (inclusive)
			int32   iDefault; ///< default value
		} i;

		// float representation
		struct floatData
		{
			float  fValue;   ///< current value as float
			float  fMin;     ///< minimum possible value (inclusive)
			float  fMax;     ///< maximum possible value (inclusive)
			float  fDefault; ///< default value
		} f;
	} data;
};

/** Remote value flags. \anchor PHWRemoteValueFlags */
enum PHWRemoteValueFlags
{
	kValueEntryExists = 1 << 0,  ///< value exists at the connection point (data is valid)
	kValueIsFloat     = 1 << 1,  ///< value should be treated as float (use the f-part of the union)
	kValueIsTrim      = 1 << 2,  ///< value is a trim value, e.g. a fader is operated in trim mode, and might required a different scaling. (\ref version2)
	kValueIsNotActive = 1 << 3   ///< value is available but cannot be modified. (\ref version2)
};

/** Remote value styles. \anchor PHWRemoteValueStyles */
/*! used to indicate the bevahiour for a value, for UI representation */
enum PHWRemoteValueStyles
{
	kValueUnknownStyle   = 0,	///< unknown style
	kValueGainStyle      = 1,	///< fill from bottom to top (Level)
	kValueWidthStyle     = 2,	///< spread from middle to both sides (Q)
	kValueFreqStyle      = 3,	///< frequency selection mode (quite similar to single Ledmode)
	kValueBoostStyle     = 4,	///< fill middle to bottom or middle to top (Pan, EQ-Gain)
	kValueRevGainStyle   = 5,	///< fill from top to bottom (reverse level, gain reduction)
	kValueSignleLedStyle = 6,	///< only one LED (used for selection, unknown or discrete values)
	kValueSwitchStyle    = 7,	///< all off or all on
	kValueShelfLfStyle   = 8,	///< not used currently, like Q and indicate filter mode Low Shelf
	kValueShelfHfStyle   = 9 	///< not used currently, like Q and indicate filter mode High Shelf
};

//----------------------------------------------------------------------
// PRemoteBankEntry - description for a remote bank entry (connected currently to a control)
//----------------------------------------------------------------------
/** Stucture with the data used to connect host internal parameter to a remote control object. \sa IHWRemoteBank::getEntryProperties ()
 */
struct PRemoteBankEntry
{
	int32 flags; ///<see \ref PRemoteBankEntryFlags

	union
	{
		struct
		{
			const char  *deviceName;	///< internal device name
			int32         channel;		///< channel index also see \ref PRemoteBankEntrySpecialChannel
			const char  *valueName;		///< value name of the channel
			int32         tag;			///< alternatively a tag of the value (used if valueName == 0)
			int32         flags;			///< some flags for a value entry see \ref PRemoteBankEntryFlags
		} value;

		struct
		{
			const char  *category;		///< category of the command
			const char  *action;		///< action for the category
			int32         flags;			///< some flags see \ref PRemoteBankEntryFlags
		} command;
	} data;
};

/** Remote Bank entry special channels. \anchor PRemoteBankEntrySpecialChannel
\sa IHWRemoteBank::addValueEntry (), IHWRemoteBank::getEntryProperties (), PRemoteBankEntry
*/
enum PRemoteBankEntrySpecialChannel
{
	kBankEntryDeviceChannel = -1,    ///< target for the parameter assignment is the device
	kBankEntrySelectedChannel = -2   ///< target for the parameter assignment is the currently selected channel
};

/** Remote Bank entry flags. \anchor PRemoteBankEntryFlags
Describe properties for IHWRemote Bank entries.
\note The flags are only valid for the specified scope, as described in the square brackets [].
*/
enum PRemoteBankEntryFlags
{
	// return information for PRemoteBankEntry
	kBankEntryExists  = 1 << 0,  ///< entry exists at the connection point (data is valid) [PRemoteBankEntry]
	kBankEntryIsValue = 1 << 1,  ///< entry is a value, otherwise it is a command [PRemoteBankEntry]

	// value flags for IHWRemoteBank::addValueEntry ()
	kBankEntryValuePushType     = 1 << 0,   ///< entry is a push type switch [IHWRemoteBank::addValueEntry ()]
	kBankEntryValueNoScaleType  = 1 << 1,   ///< value will not be scaled (required for some controls) [IHWRemoteBank::addValueEntry ()]
	kBankEntryValueReverse      = 1 << 2,   ///< use reverse data mode (i.e. value changes occur upside down) [IHWRemoteBank::addValueEntry ()]
	kBankEntryValueToggleType   = 1 << 8,   ///< switch will toggle value state [IHWRemoteBank::addValueEntry ()]
	kBankEntryValueNotAutomated = 1 << 9,   ///< control is not automatable, override default behaviour for some controls [IHWRemoteBank::addValueEntry ()]
	kBankEntryValueTouch        = 1 << 12,  ///< control will issue automation start/stop for the value object [IHWRemoteBank::addValueEntry ()]
	kBankEntryValueAutoSelect   = 1 << 13,  ///< control will "select" channel target if value is unequal 0, and auto select mode is enabled [IHWRemoteBank::addValueEntry ()]

	// command flags for IHWRemoteBank::addCommandEntry ()
	kBankEntryCommandPushType   = 1 << 0,   ///< entry is a push type switch [IHWRemoteBank::addCommandEntry ()]
};

/** Remote properties. \anchor PRemoteProperties
Property identifier for IHWRemoteStandard::setRemoteProperty()
*/
enum PRemoteProperties
{
	/** MIDI transport, this indicates that the remote actually uses MIDI as physical transport layer.
	 * Host application will provide a way to select MIDI ports
	 */
	kHWRemotePropertyMIDITransport      = 'midi',

	/** Inform that the HW Remote device will use its MIDI ports only for the device/plugin communication (\ref version21).
	 * The host might prevent further use of the MIDI ports for other purposes.
	 * Introduced for Cubase 4.5 and Nuendo 4.2.
	 */
	kHWRemotePropertyExclusiveMIDIPorts = 'miex',

	/** Hardware fader supports touch. This is required in order to optimize for this behaviour.
	 * Set value to 0 if the device does not support fader touch.
	 */
	kHWRemotePropertySupportsFaderTouch = 'ftch',

	/** Wheel range. Required to syncronize host application internal scaling for jog wheel operations.
	 * The value is the number of encoder ticks for a full turn of the control.
	 */
	kHWRemotePropertyWheelRange         = 'wrng',

	/** Inform that the plugin will want to support Control Room features (\ref version21). If the host supports
	 * Control Room functionality setRemoteProperty() will return true. False if Control Room
	 * functionality is not supported.
	 * Introduced for Cubase 4.5 and Nuendo 4.2.
	 */
	kHWRemotePropertySupportControlRoom = '?scr',

	/** Inform that the plugin will not make use of the Smart Switch feature (\ref version21).
	 * Introduced for Cubase 4.5 and Nuendo 4.2.
	 */
	kHWRemotePropertyDisableSmartSwitchOperation = 'dsso',

	/** Inform that the plugin has a bank selector. By default a plug-in that derives
	 *	from HWRemtoeBase has a bank selector. Plug-ins that don't need this can switch
	 *	it off by setting this property to 0.
	 *  \note This is not public yet. Introduced for Cubase AI5 (\ref version 22) */
	kHWRemotePropertyHasBankSelector = '?hbs'
};

//----------------------------------------------------------------------
// interface to the HWRemote plugin
//----------------------------------------------------------------------
/** Remote interface (as implemented by the plugin) */
class IHWRemote : public IPluginBase
{
public:
	//------------------------------------------------------------------------
	/// \name Initialization of the plugin
	//------------------------------------------------------------------------
	//@{
	/** Initialize the plugin's internal data. This call is called after the plugin was loaded.
	 *
	 * \note The following steps need to be performed for a complete initialization:
	 * -# Store standardImplementation for later use
	 * -# Set special device properties see, IHWRemoteStandard::setRemoteProperty()
	 * -# Add control descriptions, see IHWRemoteStandard::addRemoteControl
	 * -# Create a bank and populate it with control connections, see IHWRemoteStandard::createBank()
	 *
	 * \param standardImplementation is a pointer to an interface with standard
	 * implementations of the functionality, see IHWRemoteStandard.
	 *
	 * \return true for success.
	 * \return false for any failure.
	 */
	virtual bool PLUGIN_API init (class IHWRemoteStandard *standardImplementation) = 0;

	/** Bring the plugin down, release resources, etc.
	 * The plugin should release any resources it allocated.
	 * Referenced IHWRemoteBank's are removed by the host.
	 *
	 * \return true for success (mandatory).
	 * \return false will leave the plugin and device in an undefined state.
	 */
	virtual bool PLUGIN_API term () = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Activation and deactivation
	//------------------------------------------------------------------------
	//@{
	/** Activate the HW remote device.
	 * This call informs the plugin that the host is about to use the HW remote device.
	 * 
	 * IHWRemoteStandard::openStandard() needs to be called during this call to successfully
	 * complete this function.
	 *
	 * \note After the plugin called IHWRemoteStandard::openStandard():
	 * - the MIDI ports are active for transmit and receive of MIDI data.
	 * - the first bank is connected and setup for operation.
	 * - the host will synchronize the state of the remote control objects on the HW remote device with its internal state
	 *
	 * \return true for success.
	 * \return false informs the host that the remote remains inactive.
	 */
	virtual bool PLUGIN_API open () = 0;

	/** De-activate the HW remote device.
	 * This call informs the plugin that the host is about to use the HW remote device.
	 *
	 * The plugin can make sure that the device is brought to an in-active state.
	 * E.g. turn off any LED on the device.
	 *
	 * IHWRemoteStandard::closeStandard() needs to be called during this call to successfully
	 * complete this function.
	 *
	 * \note After the plugin called IHWRemoteStandard::closeStandard():
	 * - the MIDI ports are not active anymore. Plugin initiated data transfers are discarded.
	 *   No MIDI data is received from the input MIDI port.
	 * - the host will not synchronize the state of the remote control objects on the
	 *   HW remote device
	 *
	 * \return true for success (mandatory).
	 * \return false will leave the plugin and device in an undefined state.
	 */
	virtual bool PLUGIN_API close () = 0;

	/** Bring the external device into the same state as the application.
	 * This call will bring the plugin/device into a defined state.
	 *
	 * IHWRemoteStandard::resetAllStandard() needs to be called during this call to successfully
	 * complete this function.
	 */
	virtual void PLUGIN_API resetAll () = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Receiving data from HW control
	//------------------------------------------------------------------------
	//@{
	/** Pass the value change request to the plugin.
	 * This function is called when a value change occurs after a
	 * IHWRemoteStandard::doMidiEventStandard() decoding.
	 *
	 * The plugin calls IHWRemoteStandard::changeValueStandard() to perform the actual
	 * value change.
	 *
	 * \note The plugin can change both addr and value and/or issue multiple
	 * IHWRemoteStandard::changeValueStandard() calls during one call of this function.
	 *
	 * \param addr is the ID of the HW remote control object.
	 * \param value is the received value from the HW remote control object.
	 */
	virtual void PLUGIN_API changeValue (int32 addr, int32 value) = 0;

	/** Pass MIDI event to plugin for decoding.
	 * This function is called when the host received a MIDI event on the
	 * MIDI input port for the HW remote.
	 *
	 * A plugin, which does not require its own decoding of the MIDI event,
	 * calls IHWRemoteStandard::doMidiEventStandard() directly.
	 *
	 * A plugin, which requires a special decoding for the MIDI data (i.e.
	 * Sysex or special data formats), calls IHWRemoteStandard::changeValueStandard()
	 * with the appropiate addr and value.
	 *
	 * \param midiEvent is an opaque data type representing the MIDI data,
	 * see IHWRemoteStandard::getMIDIData() and IHWRemoteStandard::getMIDISysexData()
	 */
	virtual void PLUGIN_API doMidiEvent (void *midiEvent) = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Updating data on HW control
	//------------------------------------------------------------------------
	//@{
	/** Transmit the value of a host parameter to the remote control object.
	 * This function is called by the host when a host parameter is changed and the
	 * HW remote control object needs to reflect the change; d.g. position of a 
	 * motor fader or the state of a switch with an LED.
	 *
	 * A MIDI remote device calls IHWRemoteStandard::sendControllerStandard()
	 * if no special coding of the MIDI data is required.
	 *
	 * A MIDI remote can also call IHWRemoteStandard::sendEventStandard() and/or
	 * IHWRemoteStandard::sendStandard() directly to transmit its own MIDI events.
	 *
	 * \param control is the detailed description of the control.
	 * \param addr is the ID of the control object.
	 * \param value is the value for the control object (the value is scaled from the
	 *        host internal range to the range of the control object).
	 */
	virtual void PLUGIN_API sendController (PHWRemoteControl *control, int32 addr, int32 value) = 0;

	/** Transmit a text message to the remote device.
	 * This function is called when the remote device should update text in its text display(s).
	 * A MIDI remote plugin will usually call IHWRemoteStandard::sendStandard() to send a
	 * Sysex Event with the text data to the remote device.
	 *
	 * \note The host already performed some redundancy checks for the text display and transmits only
	 * the portions which are actually changed, see IHWRemoteStandard::addTextDisplay()
	 *
	 * \param text is the actual text as 8-bit ASCII. C-String terminated with a null.
	 * \param pos is the position inside the text display.
	 * \param displId is the text display ID where the text will appear.
	 *
	 * \see \ref useTextDisplay
	 */
	virtual void PLUGIN_API sendText (const char *text, int32 pos, int32 displId) = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Notifications
	//------------------------------------------------------------------------
	//@{
	/** Notify the activation of a bank.
	 * This function is called when a bank gets activated, i.e. assigned to the remote device.
	 *
	 * \param bankName is the name of the bank as given in IHWRemoteStandard::createBank()
	 */
	virtual void PLUGIN_API activateBank (const char *bankName) = 0;

	/** Notify the deactivation of a bank.
	 * This function is called when a bank gets deactivated, i.e. removed from the remote device.
	 *
	 * \param bankName is the name of the bank as given in IHWRemoteStandard::createBank()
	 */
	virtual void PLUGIN_API deactivateBank (const char *bankName) = 0;

	/** Notify the activation of a layer.
	 * This function is called when a layer gets activated, i.e. assigned to the remote device.
	 *
	 * \param layerName is the name of the layer as given in IHWRemoteBank::addLayer()
	 */
	virtual void PLUGIN_API activateLayer (const char *layerName) = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Utility methods
	//------------------------------------------------------------------------
	//@{
	/** Called periodically in UI thread context.
	 * This function gets called periodically from the UI thread context.
	 *
	 * \note Remotes with other data transports then MIDI have to call
	 * IHWRemoteStandard::changeValueStandard() during this function.
	 */
	virtual void PLUGIN_API onIdle () = 0;
	//@}

	static const FUID iid;
};
DECLARE_CLASS_IID (IHWRemote, 0x83850D7B, 0xC12011D8, 0xA143000A, 0x959B31C6)


//----------------------------------------------------------------------
// callback interface of the HWRemote plugin into the host
//----------------------------------------------------------------------
/** Remote interface (as implemented by the host) */
class IHWRemoteStandard : public FUnknown
{
public:
	//------------------------------------------------------------------------
	/// \name Hardware Remote properties
	//------------------------------------------------------------------------
	//@{
	/** Set HW Remote Properties.
	 * Used to set properties for the remote.
	 *
	 * \param propertyID see \ref PRemoteProperties
	 * \param value is the value the property is set to.
	 *
	 * \return true if the property is successfully set
	 * \return false if the property could not be set or the property does not exist
	 */
	virtual bool PLUGIN_API setRemoteProperty (uint32 propertyID, int32 value) = 0;

	/** Set the name for the HW remote.
	 * Set the product specific name for the HW remote.
	 * \return true if the name is successfully set
	 * \return false if the name could not be set
	 */
	virtual bool PLUGIN_API setRemoteName (const char *name) = 0;

	/** Set a family ID for a HW remote.
	 * Set family ID for combining remotes of the same family to one surface.
	 * \return true if the family ID is successfully set
	 * \return false if the family ID could not be set
	 */
	virtual bool PLUGIN_API setRemoteFamilyID (const char *name) = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Standard implementations of Activation, intialization
	//------------------------------------------------------------------------
	//@{
	/** Standard Open functionality for HW remote.
	 * Performs the standard opening functionality for the HW Remote plugin.
	 *
	 * IHWRemote::open() needs to call this method to successfully
	 * complete the IHWRemote::open() function.
	 *
	 * \note After the plugin called openStandard():
	 * - the MIDI ports are active to transmit and receive MIDI data.
	 * - the first bank is connected and setup for operation.
	 * - the host will synchronize the state of the remote control objects on the HW remote device with its internal state
	 *
	 * \return true if opening standard functionality succeeded
	 * \return false if opening standard functionality failed
	 */
	virtual bool PLUGIN_API openStandard () = 0;

	/** Standard Close functionality for HW remote.
	 * Performs the standard closing functionality for the HW Remote plugin.
	 *
	 * IHWRemote::close() needs to call this method to successfully
	 * complete the IHWRemote::close() function.
	 *
	 * \note After the plugin called closeStandard():
	 * - the MIDI ports are not active anymore. Plugin initiated data transfers are discarded.
	 *   No MIDI data is received from the input MIDI port.
	 * - the host will not synchronize the state of the remote control objects on the
	 *   HW remote device
	 *
	 * \return true if closing standard functionality succeded successfully
	 * \return false if closing standard functionality failed
	 */
	virtual bool PLUGIN_API closeStandard () = 0;

	/** Standard Reset All functionality for HW remote.
	 * This call will bring the plugin/device into a defined state.
	 *
	 * IHWRemote::resetAll() needs to call this method to successfully
	 * complete the IHWRemote::resetAll() function.
	 */
	virtual void PLUGIN_API resetAllStandard () = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name User function support
	//------------------------------------------------------------------------
	//@{
	/** Initialize user functions.
	 * Inform the host that the remote provides handling for user costumizable function keys ("user commands")
	 *
	 * \param names need to be given in the following way:\code
	 *
	 * const char *userFunctions[] =
	 * {
	 *	//Key Name,    Category,     Action,
	 * 	"F1",          "Devices",    "Mixer",	// pre-assigned to open the Mixer window
	 * 	"Shift+F1",    "",           "",     	// not pre-assigned, the user can set it
	 *	...
	 * };\endcode
	 *
	 * \param count is the number of user function triplets
	 * 
	 * \see \ref IHWRemoteBank::createGroup("Function") and \ref functionKeyGroup
	 */
	virtual bool PLUGIN_API addUserFunctions (FIDString* names, int32 count) = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Bank support
	//------------------------------------------------------------------------
	//@{
	/** Create a bank.
	 * Create a new bank for control/application parameter connections (descriptions).
	 * At least one bank needs to be created for each HW Remote plugin.
	 *
	 * \note Each created bank by a HW Remote plugin instance requires a unique name.
	 *
	 * \param name is a plugin specific name/identifier for the bank. see also getBank().
	 *
	 * \return a IHWRemoteBank interface pointer to the created bank.
	 * \return 0 if no bank was created.
	 */
	virtual class IHWRemoteBank * PLUGIN_API createBank (const char *name) = 0;

	/** Retrieve a bank by name.
	 * Retrieve a bank by name, which was previously created with createBank().
	 * At least one bank needs to be created for each HW Remote plugin.
	 *
	 * \param name is a plugin specific name/identifier for the bank. See also createBank().
	 *
	 * \return a IHWRemoteBank interface pointer to the bank with the given name.
	 * \return 0 if no bank with the given name exists for the HW Remote plugin.
	 */
	virtual class IHWRemoteBank * PLUGIN_API getBank (const char *name) = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Receiving data from HW control
	//------------------------------------------------------------------------
	//@{
	/** Pass MIDI event to standard application handler for decoding.
	 * This function can be called when the HW remote plugin requires MIDI event
	 * decoding for a control.
	 *
	 * A HW Remote plugin, which does not require its own decoding of the MIDI event,
	 * should call doMidiEventStandard() directly from IHWRemote::doMidiEvent ().
	 *
	 * A plugin, which requires a special decoding for the MIDI data (i.e.
	 * Sysex or special data formats) will not necessarily call this method.
	 *
	 * \param midiEvent is an opaque data type representing the MIDI data
	 * see getMIDIData() and getMIDISysexData()
	 */
	virtual void PLUGIN_API doMidiEventStandard (void* midiEvent) = 0;

	/** Retrieve the Sysex data from the opaque midiEvent data type.
	 * This function will retrieve the System Exclusive (SYSEX) data from the
	 * opaque midiEvent data type.
	 *
	 * \param midiEvent is an opaque data type representing the MIDI data,
	 * see IHWRemote::doMidiEvent()
	 *
	 * \return a pointer to an 8 bit uint8 array with the Sysex data, the last
	 * valid data byte in the array is 0xF7, the End of System Exclusive
	 * \return 0 if the midiEvent does not represent a valid System Exclusive MIDI event message
	 */
	virtual const uint8 * PLUGIN_API getMIDISysexData (void* midiEvent) = 0;

	/** Retrieve the MIDI data bytes from the opaque midiEvent data type.
	 * This function will retrieve the MIDI data bytes from the
	 * opaque midiEvent data type.
	 *
	 * \param midiEvent is an opaque data type representing the MIDI data,
	 * see IHWRemote::doMidiEvent()
	 *
	 * \param status on return will receive the MIDI status byte
	 * \param data1 on return will receive the data byte 1 (if any)
	 * \param data2 on return will receive the data byte 2 (if any)
	 *
	 * \note MIDI System messages (Status 0xF0 through 0xFF) will retrieve the status
	 * but no data1 and data2.
	 *
	 * \return true if the midiEvent was a valid MIDI event
	 * \return false if the midiEvent was a valid MIDI event
	 */
	virtual bool PLUGIN_API getMIDIData (void* midiEvent, char &status, char &data1, char &data2) = 0;

	/** Pass the value change request to the host application.
	 * This function implements in the host application the change of a parameter.
	 *
	 * \param addr is the ID of the HW remote control object.
	 * \param value is the received value from the HW remote control object.
	 */
	virtual void PLUGIN_API changeValueStandard (int32 addr, int32 value) = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Updating data on HW control
	//------------------------------------------------------------------------
	//@{
	/** Standard Transmit the value of a host parameter to the remote control object.
	 * This function may be called by the plugin when a host parameter is changed and the
	 * HW remote control object needs to reflect the change. E.g. position of a 
	 * motor fader or the state of a switch with an LED.
	 *
	 * A MIDI remote plugin can call sendControllerStandard() from IHWRemote::sendController()
	 * if no special coding of the MIDI data is required.
	 *
	 * \note A MIDI remote plugin can also call sendEventStandard() and/or
	 * sendStandard() directly to transmit its own MIDI events.
	 *
	 * \param control is the detailed description of the control.
	 * \param addr is the ID of the control object.
	 * \param value is the value for the control object (the value is scaled from the
	 *        host's internal range to the range of the control object).
	 */
	virtual void PLUGIN_API sendControllerStandard (PHWRemoteControl *control, int32 addr, int32 value) = 0;

	/** Standard Transmission of a MIDI event.
	 * This function is to be called by the plugin when a MIDI event is to be sent to
	 * the remote device via MIDI.
	 *
	 * \param statusByte the MIDI Status byte of the event. For channel messages the channel needs to be already part of the statusByte.
	 * \param data1 the first data byte of the MIDI message. It should be 0 for 1 Byte Events.
	 * \param data2 the second data byte of the MIDI message. It should be 0 for 1 and 2 Byte Events.
	 */
	virtual void PLUGIN_API sendEventStandard (int32 statusByte, int32 data1, int32 data2) = 0;

	/** Standard Transmit of a MIDI System Exclusive event.
	 * This function is to be called by the plugin when a MIDI System Exclusive message is to be sent to
	 * the remote device via MIDI.
	 *
	 * \param data is a pointer to an uint8 array containing the System Exclusive message.
	 * It needs to be terminated by 0xF7 (End of System Exclusive).
	 */
	virtual void PLUGIN_API sendStandard (void* data) = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Control of the assignment state
	//------------------------------------------------------------------------
	//@{
	/** Select a new bank.
	 * This call selects a new bank for the control/application parameter assignment.
	 *
	 * \param bankName is the name of the bank as set when the bank was created. See createBank().
	 */
	virtual void PLUGIN_API setBankStandard (const char *bankName) = 0;

	/** Select a new layer.
	 * This call selects a new layer for the control/application parameter assignment.
	 *
	 * \param layerName is the name of the layer to be activated. See IHWRemoteBank::addLayer().
	 */
	virtual void PLUGIN_API setLayerStandard (const char *layerName) = 0;

	/** Request a reset.
	 * This call will inform the host that the HW remote plugin requires a reset.
	 *
	 * \note usually this is issued when the plugin requires a complete update of the
	 * state between the host application and the remote device.
	 *
	 * The host will perform a IHWRemote::resetAll() call at a "safe" time.
	 */
	virtual void PLUGIN_API requestReset () = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Control Objects (controls of the HW remote)
	//------------------------------------------------------------------------
	//@{
	/** Add a control object to the HW remote plugin.
	 * Add a control object to the HW remote plugin. The control object represents
	 * a physical control on the remote device.
	 *
	 * \param control is the address of a \ref PHWRemoteControl object definition.
	 *
	 * \return true if the control was added successfully
	 * \return false if adding the control failed
	 */
	virtual bool PLUGIN_API addRemoteControl (PHWRemoteControl *control) = 0;

	/** Retrieve control object information by enumeration.
	 * Retrieve control object information by enumeration.
	 *
	 * \param addr is enumeration index for the queried control object.
	 * \param control is a \ref PHWRemoteControl structure which receives the control object information.
	 *
	 * \return true if information was successfully returned
	 * \return false if addr was out of range
	 */
	virtual bool PLUGIN_API getRemoteControl (int32 addr, PHWRemoteControl &control) = 0;

	/** Query the enumeration index of a control object by name.
	 * Query the enumeration index of a control object by name.
	 *
	 * \param controlName is a \ref PHWRemoteControl structure which receives the control object information.
	 *
	 * \return >= 0 enumeration index of the control object if a control object with the name exists
	 * \return -1 if no control object with the name exists
	 */
	virtual int32 PLUGIN_API getRemoteControlAddr (const char *controlName) = 0;

	/** Force value update for a specific control.
	 * Force value update for a specific control.
	 *
	 * \param addr is the enumeration index for the control object.
	 */
	virtual void PLUGIN_API forceRemoteControlUpdate (int32 addr) = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Values (host application parameter)
	//------------------------------------------------------------------------
	//@{
	/** Retrieve properties of a value.
	 * Retrieve properties of a value which is connected to a HW remote control object.
	 *
	 * \param addr is the enumeration index for the control object.
	 * \param value is a \ref PHWRemoteValue which is filled with the value properties.
	 *
	 * \return true if the value was retrieved successfully.
	 * \return false on failure, also if no value is connected to HW control object.
	 */
	virtual bool PLUGIN_API getValueProperties (int32 addr, PHWRemoteValue &value) = 0;

	/** Retrieve text representation of a value.
	 * Retrieve text representation of a value which is connected to a HW remote control object.
	 * \note The value text is application specific and subject to language translations.
	 *
	 * \param addr is the enumeration index for the control object.
	 * \param text is a pointer to a memory location which receives a zero-terminated C-string.
	 * \param size is the maximum number of characters the parameter text can receive.
	 *
	 * \return true if the value text was retrieved successfully.
	 * \return false on failure, also if no value is connected to HW control object.
	 */
	virtual bool PLUGIN_API getValueText (int32 addr, char *text, int32 size) = 0;

	/** Retrieve name of a value.
	 * Retrieve the name of a value which is connected to a HW remote control object.
	 * \note The value name is application specific and subject to language translations.
	 *
	 * \param addr is the enumeration index for the control object.
	 * \param name is a pointer to a memory location which receives a zero-terminated C-string.
	 * \param size is the maximum number of characters the parameter name can receive.
	 *
	 * \return true if the value name was retrieved successfully.
	 * \return false on failure, also if no value is connected to the HW control object.
	 */
	virtual bool PLUGIN_API getValueName (int32 addr, char *name, int32 size) = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Text display handling
	//------------------------------------------------------------------------
	//@{
	/** Add a text display object to the HW remote plugin.
	 * Add a text display object to the HW remote plugin. 
	 * \param chars is the total number of displayable characters
	 * \param rows is the number of rows
	 * \param cellSize is the size of one text cell
	 * \param overhead is the needed number of equal chars or cells (see fullCells) to suspend a transmission.
	 * \param fullCells has to be set to true, if the remote accepts only data for full cells.
	 * \param titleLength has to be set, if not all cells in a row are of the same size. Set to -1 if all cells are of the same size
	 *
	 * \return text display ID
	 *
	 * \see \ref useTextDisplay
	 */
	virtual int32 PLUGIN_API addTextDisplay (int32 chars, int32 rows, int32 cellSize, int32 overhead, bool fullCells, int32 titleLength) = 0;

	/** Get the current text of one cell.
	 * Get the current text of one cell.
	 * \param displayIndex is the text display ID
	 * \param row is the row of the text display
	 * \param cell is the cell position in the row
	 * \param text is the cell text as 8-bit ASCII, zero-terminated C-String.
	 * \param textSize specifies the number of characters that are returned
	 *
	 * \return true if text display at displayIndex (text display ID) exists
	 */
	virtual bool PLUGIN_API getTextDisplayData (int32 displayIndex, int32 row, int32 cell, char *text, int32 textSize) = 0;

	/** Set the text of one cell.
	 * Set the text of one cell.
	 * \param displayIndex is the text display ID
	 * \param row is the row of the text display
	 * \param cell is the cell position in the row
	 * \param text is the cell text as 8-bit ASCII, zero-terminated C-String.
	 *
	 * \return true if text display at displayIndex (text display ID) exists
	 *
	 * \note The text display object is not aware of this text being sent to the remote. If a new page is displayed or a group change occurs, the text will be overwritten. 
	 */
	virtual bool PLUGIN_API setTextDisplayData (int32 displayIndex, int32 row, int32 cell, char *text) = 0;

	/** Get the current text of one row.
	 * Get the current text of one row.
	 * \param displayIndex is the text display ID
	 * \param row is the row of the text display
	 * \param text is the row text as 8-bit ASCII, zero-terminated C-String.
	 * \param textSize specifies the number of characters that are returned
	 *
	 * \return true if text display at displayIndex (text display ID) exists
	 */
	virtual bool PLUGIN_API getTextDisplayRow (int32 displayIndex, int32 row, char *text, int32 textSize) = 0;

	/** Set the text of one row.
	 * Set the text of one row.
	 * \param displayIndex is the text display ID
	 * \param row is the row of the text display
	 * \param text is the row text as 8-bit ASCII, zero-terminated C-String.
	 *
	 * \return true if text display at displayIndex (text display ID) exists
	 *
	 * \note The text display object is not aware of this text being sent to the remote. If a new page is displayed or a group change occurs, the text will be overwritten. 
	 */
	virtual bool PLUGIN_API setTextDisplayRow (int32 displayIndex, int32 row, char *text) = 0;

	/** Allow the text display object to switch the parameter display of one cell.
	 * Allow the text display object to switch the parameter display of one cell.\n
	 * If in a group both parameter names and parameter values are displayed in one row, and a change of the parameter value occurs, the text display object can only process a display switch, if this method is called before.
	 * \param displayIndex is the text display ID
	 * \param cell is the affected cell specified from 0 to the total number of cells (of the selected text display)
	 */
	virtual void PLUGIN_API requestDisplaySwitch (int32 displayIndex, int32 cell) = 0;
	//@}

	static const FUID iid;
};
DECLARE_CLASS_IID (IHWRemoteStandard, 0xE2F29E17, 0xC12011D8, 0xAD3E000A, 0x959B31C6)


//----------------------------------------------------------------------
// callback interface of the HWRemote plugin for the bank into the host
//----------------------------------------------------------------------
/** Remote bank interface (implemented by the host) */
class IHWRemoteBank : public FUnknown
{
public:
	//------------------------------------------------------------------------
	/// \name Properties
	//------------------------------------------------------------------------
	//@{
	/** Retrieve the bank's name.
	 * Retrieve the bank's name as it was given to the bank during creation, see IHWRemoteStandard::createBank ()
	 *
	 * \return pointer to the name.
	 */
	virtual const char * PLUGIN_API getName () = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Low level entry access
	//------------------------------------------------------------------------
	//@{
	/** Add a device to the list of handled devices by the bank.
	 * This function adds a device to the list of handled devices by the bank.
	 *
	 * \note The bank requires a device to be added before addValueEntry() can be called.
	 *
	 * \param deviceName is the symbolic name of the device.
	 *
	 * \return true if the device was added.
	 * \return false if the device was not added.
	 */
	virtual bool PLUGIN_API addDevice (const char *deviceName) = 0;

	/** Add a layer description to the list of layers of the bank.
	 * This function adds a layer description to the list of layers of the bank.
	 *
	 * \note The bank requires a layer to be added before addValueEntry(), addCommandEntry(),
	 * IHWRemoteGroup::addControl() or IHWRemoteGroup::addCommand() are called.
	 *
	 * \param layerName is the name of the layer. The same name is used when the layer is
	 * activated with IHWRemoteStandard::setLayerStandard()
	 *
	 * \return true if the layer was added.
	 * \return false if the layer was not added.
	 */
	virtual bool PLUGIN_API addLayer (const char *layerName) = 0;

	/** Add individual connection description for a value to a control.
	 * This function adds individual connection of a value to a control.
	 *
	 * \note See an external document for the available device and value names.
	 *
	 * \param layerName is the name of the layer, which should contain the connection.
	 * \param controlName is the name of the control, which should get the value connection.
	 * \param deviceName is the name of the device, whose value is required for the connection
	 * \param channel is the channel identifier, a positive number (0 to max integer range) or a member of \ref PRemoteBankEntrySpecialChannel
	 * \param valueName is the name of the value.
	 * \param flags is an additional description for the connection, see \ref PRemoteBankEntryFlags.
	 *
	 * \return true if the value/control connection description was added.
	 * \return false on failure.
	 */
	virtual bool PLUGIN_API addValueEntry (const char *layerName, const char *controlName, const char *deviceName, int32 channel, const char *valueName, uint32 flags) = 0;

	/** Add individual connection description for a command to a control.
	 * This function adds individual connection description for a command to a control.
	 *
	 * \note See an external document for the available category and action names.
	 * \note A special command is the "layer" switching command, its category is
	 * "Remote" and the action is the name of the layer.
	 *
	 * \param layerName is the name of the layer, which should contain the connection.
	 * \param controlName is the name of the control, which should get the value connection.
	 * \param category is the name of a category of commands.
	 * \param action is the name of the action.
	 * \param flags is an additional description for the connection, see \ref PRemoteBankEntryFlags.
	 *
	 * \return true if the command/control connection description was added.
	 * \return false on failure.
	 */
	virtual bool PLUGIN_API addCommandEntry (const char *layerName, const char *controlName, const char *category, const char *action, uint32 flags) = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Functional Groups
	//------------------------------------------------------------------------
	//@{
	/** Create a functional group.
	 * This function will create a new functional group, which is associated with the bank.
	 * \param groupKind determines the group's behaviour: "Encoder", "Fader", "Matrix", "Function", "Wheel", "Automation"
	 * \param groupName internal name to identify the functional group (given by the plugin)
	 * \param numColumns Number of identical controls, e.g. number of fader strips or number of encoders
	 * \param textDisplayID identifier of a text display associated with the functional group see IHWRemoteStandard::addTextDisplay()
	 * \param channelMap a channel map for the functional group, 0 will assign the default channel map see IHWRemoteBank::createChannelMap()
	 * \return pointer to the group instance.
	 * \return 0 if no proper group instance could be created
	 *
	 * \note Group Information:
	 * - \b "Encoder", supports columns, supports text display, supports channel map, \ref encoderGroup
	 * - \b "Fader", supports columns, supports channel map, \ref faderGroup
	 * - \b "Matrix", supports columns, \ref matrixGroup
	 * - \b "Function", IHWRemoteStandard::addUserFunctions(), \ref functionKeyGroup
	 * - \b "Wheel", \ref wheelGroup
	 * - \b "Automation", \ref automationGroup (\ref version2)
	 */
	virtual class IHWRemoteGroup * PLUGIN_API createGroup (const char *groupKind, const char *groupName, int32 numColumns, int32 textDisplayID = -1, class IHWRemoteChannelMap *channelMap = 0) = 0;

	/** Retrieve a functional group by name.
	 * Retrieve a functional group by name.
	 *
	 * \param groupName is the name of the functional group as it was given to the group during creation with createGroup ().
	 *
	 * \return pointer to the group instance.
	 * \return 0 if no proper group instance could be retrieved
	 */
	virtual const class IHWRemoteGroup * PLUGIN_API getGroup (const char *groupName) = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Channel Map
	//------------------------------------------------------------------------
	//@{
	/** Create a channel map.
	 * This function will create a channel map.
	 * \param mapKind defines what channel types will be mapped:
	 * - "All"
	 * - "Input"
	 * - "Output"
	 * - "Audio"
	 * - "MIDI"
	 * - "Groups"
	 * - "FX-Ret"
	 * - "Instr"
	 * - "Busses"
	 * - "Control-Room" (\ref version2)
	 * - "Control-Phones" (\ref version2)
	 * - "Control-Studio" (\ref version2)
	 *
	 * \param mapName internal name to identify the channel map (given by the plugin)
	 *
	 * \return pointer to channel map instance
	 * \return 0 if no proper channel map instance could be created.
	 *
	 * \note The symbolic names "Control-Room", "Control-Phones" and "Control-Studio" will only work
	 *       if Control Room functionality is available in the host.
	 *       If a IHWRemoteChannelMap is created with one of these kinds,
	 *       IHWRemoteChannelMap::setKind() will accept only another "Control-xxx"
	 *       symbolic name. (\ref version2)
	 */
	virtual class IHWRemoteChannelMap * PLUGIN_API createChannelMap (const char *mapKind, const char *mapName) = 0;
	//@}

	//------------------------------------------------------------------------
	/// \name Query bank entries
	//------------------------------------------------------------------------
	//@{
	/** Retrieve connection entry properties.
	 * This function will retrieve the properties of a bank entry.
	 *
	 * \param addr is the enumeration index of the HW remote control object.
	 * \param entry is a \ref PRemoteBankEntry receiving the property information.
	 *
	 * \return true if a bank entry exists and entry contains valid data
	 * \return false if no bank entry exists, entry contains undefined data
	 */
	virtual bool PLUGIN_API getEntryProperties (int32 addr, PRemoteBankEntry &entry) = 0;
	//@}

	static const FUID iid;
};
DECLARE_CLASS_IID (IHWRemoteBank, 0x0CC43156, 0xC12111D8, 0xBD2C000A, 0x959B31C6)

//----------------------------------------------------------------------
// callback interface of the HWRemote plugin for the bank into the host
//----------------------------------------------------------------------
/** Extended Remote bank interface (implemented by the host) (\ref version2) */
class IHWRemoteBank2 : public IHWRemoteBank
{
public:
	//------------------------------------------------------------------------
	/// \name Low level entry access
	//------------------------------------------------------------------------
	//@{

	/** Add individual connection description for a value to a control with a IHWRemoteChannelMap.
	 * This function adds individual connection of a value to a control with a IHWRemoteChannelMap.
	 *
	 * \note See an external document for the available device and value names.
	 *
	 * \param layerName is the name of the layer, which should contain the connection.
	 * \param controlName is the name of the control, which should get the value connection.
	 * \param deviceName is the name of the device, whose value is required for the connection
	 * \param channel is the channel identifier, a positive number (0 to max integer range) or a member of \ref PRemoteBankEntrySpecialChannel
	 * \param valueName is the name of the value.
	 * \param flags is an additional description for the connection, see \ref PRemoteBankEntryFlags.
	 * \param channelMap is the corresponding channel map, if the default channel map is needed use IHWRemoteBank::addValueEntry().
	 *
	 * \return true if the value/control connection description was added.
	 * \return false on failure.
	 */
	virtual bool PLUGIN_API addValueEntry (const char *layerName, const char *controlName, const char *deviceName, int32 channel, const char *valueName, uint32 flags, IHWRemoteChannelMap* channelMap) = 0;
	//@}

	static const FUID iid;
};
DECLARE_CLASS_IID (IHWRemoteBank2, 0x2D9172FF, 0x80C24C59, 0xBF23CB6F, 0x67415538)

//----------------------------------------------------------------------
// callback interface of the HWRemote plugin for the channel map/filter
//----------------------------------------------------------------------
class IHWRemoteChannelMap : public FUnknown
{
public:
	//----------------------------------------------------------------------
	/// \name Properties
	//----------------------------------------------------------------------
	//@{
	/** Retrieve the name of the channel map.
	 * This function will retrieve the name of a channel map. The name is set
	 * during creation of the channel map instance by IHWRemoteBank::createChannelMap ().
	 * \return pointer to the name of the channel map.
	 */
	virtual const char * PLUGIN_API getName () = 0;

	/** Set the kind of the channel map.
	 * This function will set the kind of the channel map.
	 *
	 * \param kind is the symbolic name of the new kind of the channel map.
	 * Possible symbolic names:
	 * - "All"
	 * - "Input"
	 * - "Output"
	 * - "Audio"
	 * - "MIDI"
	 * - "Groups"
	 * - "FX-Ret"
	 * - "Instr"
	 * - "Busses"
	 * - "Control-Room" (\ref version2)
	 * - "Control-Phones" (\ref version2)
	 * - "Control-Studio" (\ref version2)
	 *
	 * \return true if the kind was set successfully.
	 * \return false if the kind was not changed.
	 *
	 * \note The symbolic names "Control-Room", "Control-Phones" and "Control-Studio" will only work
	 *       if Control Room functionality is available in the host.
	 *       If a IHWRemoteChannelMap is created with one of these kinds,
	 *       IHWRemoteChannelMap::setKind() will accept only another "Control-xxx"
	 *       symbolic name. (\ref version2)
	 */
	virtual bool PLUGIN_API setKind (const char *kind) = 0;

	/** Set number of channels this channel map handles.
	 * This function will set the number of channels the map will handle.
	 * It is usually the number of faders or encoders on a HW remote device.
	 * \return true if the number of channels was set successful.
	 * \return false if the number of channels was not changed.
	 */
	virtual bool PLUGIN_API setNumChannels (int32 numChannels) = 0;
	//@}

	static const FUID iid;
};
DECLARE_CLASS_IID (IHWRemoteChannelMap, 0x284D3DE9, 0xC12111D8, 0x8836000A, 0x959B31C6)

//----------------------------------------------------------------------
// callback interface of the HWRemote plugin for the functional groups
//----------------------------------------------------------------------
class IHWRemoteGroup : public FUnknown
{
public:
	//----------------------------------------------------------------------
	/// \name Properties
	//----------------------------------------------------------------------
	//@{
	/** Retrieve the kind of the functional group.
	 * This function will retrieve the kind of the functional group. The kind is set
	 * during creation of the functional group instance by IHWRemoteBank::createGroup ().
	 * \return pointer to the kind of the functional group.
	 */
	virtual const char * PLUGIN_API getKind () = 0;

	/** Retrieve the name of the functional group.
	 * This function will retrieve the internal name of the functional group. The internal
	 * name is set during creation of the functional group instance by
	 * IHWRemoteBank::createGroup ().
	 * \return pointer to the name of the functional group.
	 */
	virtual const char * PLUGIN_API getName () = 0;
	//@}

	//----------------------------------------------------------------------
	/// \name Entry definition
	//----------------------------------------------------------------------
	//@{
	/** Add control with a role to a functional group.
	 * This function adds control to a function group and defines its role.
	 *
	 * \note See an external document for the roles for different functional groups.
	 *
	 * \param layerName is the name of the layer, which should contain the control and role.
	 * \param controlName is the name of the control, which should be added to the functional group.
	 * \param controlRole is the role for the control.
	 *
	 * \return true if the control/role was added to the funtional group.
	 * \return false on failure.
	 */
	virtual bool PLUGIN_API addControl (const char *layerName, const char *controlName, const char *controlRole) = 0;

	/** Add control with a command to a functional group.
	 * This function adds control with a command to a functional group.
	 *
	 * \note See an external document for the commands for different functional groups.
	 *
	 * \param layerName is the name of the layer, which should contain the control and command.
	 * \param controlName is the name of the control, which should be added to the functional group.
	 * \param command is the name of the command.
	 * \param value is an activation value, send to the HW remote when the command is activated. Will not be used if -1.
	 *
	 * \return true if the control/command was added to the funtional group.
	 * \return false on failure.
	 */
	virtual bool PLUGIN_API addCommand (const char *layerName, const char *controlName, const char *command, int32 value = 0) = 0;
	//@}

	//----------------------------------------------------------------------
	/// \name Command handling
	//----------------------------------------------------------------------
	//@{
	/** Executes a command for the functional group.
	 * This function adds control with a command to a functional group.
	 *
	 * \note See an external document for the commands for different functional groups.
	 *
	 * \param command is the name of the command.
	 *
	 * \return true if the command was executed by the funtional group.
	 * \return false if the command is unknown or invalid for the functional group.
	 */
	virtual bool PLUGIN_API handleCommand (const char *command) = 0;
	//@}

	static const FUID iid;
};
DECLARE_CLASS_IID (IHWRemoteGroup, 0x411DED7C, 0xC12111D8, 0x99CD000A, 0x959B31C6)

}

#endif

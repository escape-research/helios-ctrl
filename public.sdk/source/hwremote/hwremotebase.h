//-----------------------------------------------------------------------------
// Steinberg Remote Interface Software Development Kit (SDK)
// Version 2.0    Date : 2006
//
// Category     : HW Remote Controller
// Filename     : hwremotebase.h
// Created by   : Stefan Scheffler
// Description  : HW Remote Controller module
//-----------------------------------------------------------------------------
// LICENSE
// (c) 2006, Steinberg Media Technologies GmbH
//-----------------------------------------------------------------------------
// This Software Development Kit may not be distributed in parts or its entirety 
// without prior written agreement by Steinberg Media Technologies GmbH. This
// SDK must not be used to re-engineer or manipulate any technology used in any
// Steinberg or Third-party application or software module, unless permitted by
// law.
//-----------------------------------------------------------------------------

#ifndef __hwremotebase__
#define __hwremotebase__

//-----------------------------------------------------------------------------
// SDK Interface Headers
#ifndef __ihwremote__
#include "pluginterfaces/hwremote/ihwremote.h"
#endif
//-----------------------------------------------------------------------------

namespace Steinberg {

class HWRemoteBase : public IHWRemote
{
public:
	HWRemoteBase ();
	virtual ~HWRemoteBase ();


	//-----------------------------------------------------------------------------
	// Callback Methods into IHWRemoteStandard Interface methods:
	//-----------------------------------------------------------------------------
	virtual bool setRemoteProperty (uint32 propertyID, int32 value);	// used to set properties for the remote (has MIDI, or similar)
	virtual bool setRemoteName (const char *name);
	virtual bool setRemoteFamilyID (const char *name);	// set an ID for combining remotes of the same family to one surface

	// the activation, intialization
	// hidden through the IHWRemote implementation
	//virtual bool PLUGIN_API openStandard ();
	//virtual bool PLUGIN_API closeStandard ();
	//virtual void PLUGIN_API resetAllStandard ();

	// user function generation
	// prepare the remote to provide handling for user costumizable function keys ("user commands")
	virtual bool addUserFunctions (const char** names, int32 count);

	// bank creation
	virtual class IHWRemoteBank *createBank (const char *name);
	virtual class IHWRemoteBank *getBank (const char *name);
//	virtual bool setBank (const char *name);		// activate the appropiate bank
//	virtual bool setLayer (const char *name);	// activate the appropiate layer

	// receiving data from HW control
	//virtual void  doMidiEventStandard (void* midiEvent);
	virtual const unsigned char *getMIDISysexData (void* midiEvent);	// retrieve Sysex data from the midiEvent, valid until exit of doMidiEvent
	virtual bool getMIDIData (void* midiEvent, char &status, char &data1, char &data2); // retrieve MIDI data bytes
	virtual void changeValueStandard (int32 addr, int32 value);

	// Updating data on HW control
	//virtual void sendControllerStandard (PHWRemoteControl *control, long addr, long value);
	virtual void sendEventStandard (int32 statusByte, int32 data1, int32 data2);
	virtual void sendStandard (void* data);

	// control of the assignment state
	virtual void setBankStandard (const char *bankName);     // activation of a bank
	virtual void setLayerStandard (const char *layerName);   // activation of a layer
	virtual void requestReset ();      // plugin requires a reset

	// properties
	virtual bool addRemoteControl (PHWRemoteControl *control);
	virtual bool getRemoteControl (int32 addr, PHWRemoteControl &control);
	virtual int32 getRemoteControlAddr (const char *controlName);
	virtual void forceRemoteControlUpdate (int32 addr);   // force value update for a specific control

	// value access
	virtual bool getValueProperties (int32 addr, PHWRemoteValue &value);	// receive current value, false if no value is connected
	virtual bool getValueText (int32 addr, char *text, int32 size);  // receive text representation of the connected value, false if no text is returned
	virtual bool getValueName (int32 addr, char *name, int32 size);  // recevie name of the connected parameter, false if no name is returned

	// text display handling
	virtual int32 addTextDisplay (int32 chars, int32 rows, int32 cellSize, int32 overhead, bool fullCells, int32 titleLength);
	virtual bool getTextDisplayData (int32 displayIndex, int32 row, int32 cell, char *text, int32 textSize);
	virtual bool setTextDisplayData (int32 displayIndex, int32 row, int32 cell, char *text);
	virtual bool getTextDisplayRow (int32 displayIndex, int32 row, char *text, int32 textSize);
	virtual bool setTextDisplayRow (int32 displayIndex, int32 row, char *text);
	virtual void requestDisplaySwitch (int32 displayIndex, int32 cell);


	//-----------------------------------------------------------------------------
	// Interface methods:
	//-----------------------------------------------------------------------------
	DECLARE_FUNKNOWN_METHODS

	// FUnknown method
	virtual tresult PLUGIN_API initialize (FUnknown* context);
	virtual tresult PLUGIN_API terminate ();

	// connection to the host side implementation
	virtual bool PLUGIN_API init (class IHWRemoteStandard *standardImplementation);
	virtual bool PLUGIN_API term ();

	// activation and deactivation
	virtual bool PLUGIN_API open ();
	virtual bool PLUGIN_API close ();
	virtual void PLUGIN_API resetAll ();

	// receiving data from HW control
	virtual void PLUGIN_API changeValue (int32 addr, int32 value);
	virtual void PLUGIN_API doMidiEvent (void *midiEvent);

	// Updating data on HW control
	virtual void PLUGIN_API sendController (PHWRemoteControl *control, int32 addr, int32 value);
	virtual void PLUGIN_API sendText (const char *text, int32 pos, int32 displId);

	// notifications
	virtual void PLUGIN_API activateBank (const char *bankName);     // activation of a bank
	virtual void PLUGIN_API deactivateBank (const char *bankName);   // deactivation of a bank
	virtual void PLUGIN_API activateLayer (const char *layerName);   // activation of a layer

	// utility methods
	virtual void PLUGIN_API onIdle ();			// called periodically in UI thread context
protected:
	class IHWRemoteStandard *standardImplementation;
};

}
#endif

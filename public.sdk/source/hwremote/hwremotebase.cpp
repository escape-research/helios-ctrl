//-----------------------------------------------------------------------------
// Steinberg Remote Interface Software Development Kit (SDK)
// Version 2.0    Date : 2006
//
// Category     : HW Remote Controller
// Filename     : hwremotebase.cpp
// Created by   : Stefan Scheffler
// Description  : HW Remote Controller module
//-----------------------------------------------------------------------------
// LICENSE
// (c) 2006, Steinberg Media Technologies GmbH
//-----------------------------------------------------------------------------
// This Software Development Kit may not be distributed in parts or its entirety 
// without prior written agreement by Steinberg Media Technologies GmbH. This
// SDK must not be used to re-engineer or manipulate any technology used in any
// Steinberg or Third-party application or software module, unless permitted by
// law.
//-----------------------------------------------------------------------------

#ifndef __hwremotebase__
#include "hwremote/hwremotebase.h"
#endif

namespace Steinberg {

IMPLEMENT_REFCOUNT (HWRemoteBase)

tresult PLUGIN_API HWRemoteBase::queryInterface (const char* iid, void** obj)
{
	QUERY_INTERFACE (iid, obj, FUnknown::iid, IPluginBase)
	QUERY_INTERFACE (iid, obj, IPluginBase::iid, IPluginBase)
	QUERY_INTERFACE (iid, obj, IHWRemote::iid, IHWRemote)
	*obj = 0;
	return kNoInterface;
}

//-----------------------------------------------------------------------------
HWRemoteBase::HWRemoteBase ()
: standardImplementation (0)
{
	FUNKNOWN_CTOR
}

//-----------------------------------------------------------------------------
HWRemoteBase::~HWRemoteBase ()
{
	FUNKNOWN_DTOR
}

//-----------------------------------------------------------------------------
tresult PLUGIN_API HWRemoteBase::initialize (FUnknown* context)
{
	return kResultOk;
}

//-----------------------------------------------------------------------------
tresult PLUGIN_API HWRemoteBase::terminate ()
{
	return kResultOk;
}

//-----------------------------------------------------------------------------
// IHWRemote Interface methods:
//------------------------------------------------------------------------
bool PLUGIN_API HWRemoteBase::init (class IHWRemoteStandard *standardImplementation)
{
	this->standardImplementation = standardImplementation;
	return true;
}

//------------------------------------------------------------------------
bool PLUGIN_API HWRemoteBase::term ()
{
	return true;
}


//------------------------------------------------------------------------
// activation and deactivation
//------------------------------------------------------------------------
bool PLUGIN_API HWRemoteBase::open ()
{
	if (standardImplementation)
		return standardImplementation->openStandard ();
	return false;
}

//------------------------------------------------------------------------
bool PLUGIN_API HWRemoteBase::close ()
{
	if (standardImplementation)
		return standardImplementation->closeStandard ();
	return false;
}

//------------------------------------------------------------------------
void PLUGIN_API HWRemoteBase::resetAll ()
{
	if (standardImplementation)
		standardImplementation->resetAllStandard ();
}


//------------------------------------------------------------------------
// receiving data from HW control
//------------------------------------------------------------------------
void PLUGIN_API HWRemoteBase::changeValue (int32 addr, int32 value)
{
	if (standardImplementation)
		standardImplementation->changeValueStandard (addr, value);
}

//------------------------------------------------------------------------
void PLUGIN_API HWRemoteBase::doMidiEvent (void *midiEvent)
{
	if (standardImplementation)
		standardImplementation->doMidiEventStandard (midiEvent);
}


//------------------------------------------------------------------------
// Updating data on HW control
//------------------------------------------------------------------------
void PLUGIN_API HWRemoteBase::sendController (PHWRemoteControl *control, int32 addr, int32 value)
{
	if (standardImplementation)
		standardImplementation->sendControllerStandard (control, addr, value);
}

//------------------------------------------------------------------------
void PLUGIN_API HWRemoteBase::sendText (const char *text, int32 pos, int32 displId)
{	// no standard implementation available, differs for all devices
}


//------------------------------------------------------------------------
// notifications
//------------------------------------------------------------------------
void PLUGIN_API HWRemoteBase::activateBank (const char *bankName)
{	// no standard implementation available, differs for all devices
}

//------------------------------------------------------------------------
void PLUGIN_API HWRemoteBase::deactivateBank (const char *bankName)
{	// no standard implementation available, differs for all devices
}

//------------------------------------------------------------------------
void PLUGIN_API HWRemoteBase::activateLayer (const char *layerName)
{	// no standard implementation available, differs for all devices
}


//------------------------------------------------------------------------
// utility methods
//------------------------------------------------------------------------
void PLUGIN_API HWRemoteBase::onIdle ()
{	// no standard implementation available, differs for all devices
}



//-----------------------------------------------------------------------------
// IHWRemoteStandard Interface methods:
//------------------------------------------------------------------------

//------------------------------------------------------------------------
// remote properties
//------------------------------------------------------------------------
bool HWRemoteBase::setRemoteProperty (uint32 propertyID, int32 value)
{
	return standardImplementation->setRemoteProperty (propertyID, value);
}

//------------------------------------------------------------------------
bool HWRemoteBase::setRemoteName (const char *name)
{
	return standardImplementation->setRemoteName (name);
}

//------------------------------------------------------------------------
bool HWRemoteBase::setRemoteFamilyID (const char *name)
{
	return standardImplementation->setRemoteFamilyID (name);
}


//------------------------------------------------------------------------
// prepare the remote to provide handling for user costumizable function keys ("user commands")
bool HWRemoteBase::addUserFunctions (const char** names, int32 count)
{
	return standardImplementation->addUserFunctions (names, count);
}

//------------------------------------------------------------------------
// bank creation
//------------------------------------------------------------------------
class IHWRemoteBank *HWRemoteBase::createBank (const char *name)
{
	return standardImplementation->createBank (name);
}

//------------------------------------------------------------------------
class IHWRemoteBank *HWRemoteBase::getBank (const char *name)
{
	return standardImplementation->getBank (name);
}

//------------------------------------------------------------------------
//	bool HWRemoteBase::setBank (const char *name)
//------------------------------------------------------------------------
//	bool HWRemoteBase::setLayer (const char *name)

//------------------------------------------------------------------------
// receiving data from HW control
//------------------------------------------------------------------------
//------------------------------------------------------------------------
const unsigned char *HWRemoteBase::getMIDISysexData (void* midiEvent)
{
	return standardImplementation->getMIDISysexData (midiEvent);
}

//------------------------------------------------------------------------
bool HWRemoteBase::getMIDIData (void* midiEvent, char &status, char &data1, char &data2)
{
	return standardImplementation->getMIDIData (midiEvent, status, data1, data2);
}

//------------------------------------------------------------------------
void HWRemoteBase::changeValueStandard (int32 addr, int32 value)
{
	return standardImplementation->changeValueStandard (addr, value);
}

//------------------------------------------------------------------------
// Updating data on HW control
//------------------------------------------------------------------------
void HWRemoteBase::sendEventStandard (int32 statusByte, int32 data1, int32 data2)
{
	return standardImplementation->sendEventStandard (statusByte, data1, data2);
}

//------------------------------------------------------------------------
void HWRemoteBase::sendStandard (void* data)
{
	return standardImplementation->sendStandard (data);
}


//------------------------------------------------------------------------
// control of the assignment state
//------------------------------------------------------------------------
void HWRemoteBase::setBankStandard (const char *bankName)
{
	return standardImplementation->setBankStandard (bankName);
}

//------------------------------------------------------------------------
void HWRemoteBase::setLayerStandard (const char *layerName)
{
	return standardImplementation->setLayerStandard (layerName);
}

//------------------------------------------------------------------------
void HWRemoteBase::requestReset ()
{
	return standardImplementation->requestReset ();
}


//------------------------------------------------------------------------
// properties
//------------------------------------------------------------------------
bool HWRemoteBase::addRemoteControl (PHWRemoteControl *control)
{
	return standardImplementation->addRemoteControl (control);
}

//------------------------------------------------------------------------
bool HWRemoteBase::getRemoteControl (int32 addr, PHWRemoteControl &control)
{
	return standardImplementation->getRemoteControl (addr, control);
}

//------------------------------------------------------------------------
int32 HWRemoteBase::getRemoteControlAddr (const char *controlName)
{
	return standardImplementation->getRemoteControlAddr (controlName);
}

//------------------------------------------------------------------------
void HWRemoteBase::forceRemoteControlUpdate (int32 addr)
{
	return standardImplementation->forceRemoteControlUpdate (addr);
}


//------------------------------------------------------------------------
// value access
//------------------------------------------------------------------------
bool HWRemoteBase::getValueProperties (int32 addr, PHWRemoteValue &value)
{
	return standardImplementation->getValueProperties (addr, value);
}

//------------------------------------------------------------------------
bool HWRemoteBase::getValueText (int32 addr, char *text, int32 size)
{
	return standardImplementation->getValueText (addr, text, size);
}

//------------------------------------------------------------------------
bool HWRemoteBase::getValueName (int32 addr, char *name, int32 size)
{
	return standardImplementation->getValueName (addr, name, size);
}


//------------------------------------------------------------------------
// text display handling
//------------------------------------------------------------------------
int32 HWRemoteBase::addTextDisplay (int32 chars, int32 rows, int32 cellSize, int32 overhead, bool fullCells, int32 titleLength)
{
	return standardImplementation->addTextDisplay (chars, rows, cellSize, overhead, fullCells, titleLength);
}

//------------------------------------------------------------------------
bool HWRemoteBase::getTextDisplayData (int32 displayIndex, int32 row, int32 cell, char *text, int32 textSize)
{
	return standardImplementation->getTextDisplayData (displayIndex, row, cell, text, textSize);
}

//------------------------------------------------------------------------
bool HWRemoteBase::setTextDisplayData (int32 displayIndex, int32 row, int32 cell, char *text)
{
	return standardImplementation->setTextDisplayData (displayIndex, row, cell, text);
}

//------------------------------------------------------------------------
bool HWRemoteBase::getTextDisplayRow (int32 displayIndex, int32 row, char *text, int32 textSize)
{
	return standardImplementation->getTextDisplayRow (displayIndex, row, text, textSize);
}

//------------------------------------------------------------------------
bool HWRemoteBase::setTextDisplayRow (int32 displayIndex, int32 row, char *text)
{
	return standardImplementation->setTextDisplayRow (displayIndex, row, text);
}

//------------------------------------------------------------------------
void HWRemoteBase::requestDisplaySwitch (int32 displayIndex, int32 cell)
{
	return standardImplementation->requestDisplaySwitch (displayIndex, cell);
}

}
//-----------------------------------------------------------------------------
// Steinberg Remote Interface Software Development Kit (SDK)
// Version 2.1    Date : 2008
//
// Category     : HW Remote Controller
// Filename     : hwremote-us224.cpp
// Created by   : Steinberg
// Description  : HW Remote Controller Tascam US-224 Sample implementation
//-----------------------------------------------------------------------------
// LICENSE
// (c) 2010, Steinberg Media Technologies GmbH
//-----------------------------------------------------------------------------
// This Software Development Kit may not be distributed in parts or its entirety 
// without prior written agreement by Steinberg Media Technologies GmbH. This
// SDK must not be used to re-engineer or manipulate any technology used in any
// Steinberg or Third-party application or software module, unless permitted by
// law.
//-----------------------------------------------------------------------------

#include "hwremote-us224.h"

//------------------------------------------------------------------------
// descriptions of the controls for the HW Remote
//------------------------------------------------------------------------
struct PHWRemoteControl HWRemoteUS224CtrlMap[] =
{
	// Special handling:
	// Fader 1 to 4 actually do not need transmission, but the plugin require the programs
	// internal state of the fader values for the "Null" mode
	// During sendController () the message is intercepted and never past on to the device
	{"Fader 1",     kMIDIController, 15, 0x40, 127, kControlReceive | kControlTransmit | kControlNoFeedback},
	{"Fader 2",     kMIDIController, 15, 0x41, 127, kControlReceive | kControlTransmit | kControlNoFeedback},
	{"Fader 3",     kMIDIController, 15, 0x42, 127, kControlReceive | kControlTransmit | kControlNoFeedback},
	{"Fader 4",     kMIDIController, 15, 0x43, 127, kControlReceive | kControlTransmit | kControlNoFeedback},

	{"Fader Master",kMIDIController, 15, 0x4B, 127, kControlReceive},

	{"Data Wheel",  kMIDIController, 15, 0x60, 24, kControlReceive | kControlRelative},

	{"Mute 1",      kMIDIController, 15, 0x00, 127, kControlReceive},
	{"Mute 2",      kMIDIController, 15, 0x01, 127, kControlReceive},
	{"Mute 3",      kMIDIController, 15, 0x02, 127, kControlReceive},
	{"Mute 4",      kMIDIController, 15, 0x03, 127, kControlReceive},

	{"Solo",        kMIDIController, 15, 0x2A, 127, kControlReceive},

	{"Select 1",    kMIDIController, 15, 0x20, 127, kControlReceive},
	{"Select 2",    kMIDIController, 15, 0x21, 127, kControlReceive},
	{"Select 3",    kMIDIController, 15, 0x22, 127, kControlReceive},
	{"Select 4",    kMIDIController, 15, 0x23, 127, kControlReceive},

	{"Rec Enable",  kMIDIController, 15, 0x29, 127, kControlReceive},

	{"Null",        kMIDIController, 15, 0x28, 127, kControlReceive},

	{"Bank Left",   kMIDIController, 15, 0x10, 127, kControlReceive},
	{"Bank Right",  kMIDIController, 15, 0x11, 127, kControlReceive},

	{"Rewind",      kMIDIController, 15, 0x13, 127, kControlReceive},
	{"Forward",     kMIDIController, 15, 0x14, 127, kControlReceive},
	{"Stop",        kMIDIController, 15, 0x15, 127, kControlReceive},
	{"Play",        kMIDIController, 15, 0x16, 127, kControlReceive},
	{"Record",      kMIDIController, 15, 0x17, 127, kControlReceive},

	{"Locator 1",   kMIDIController, 15, 0x18, 127, kControlReceive},
	{"Locator 2",   kMIDIController, 15, 0x19, 127, kControlReceive},
	{"Locator Set", kMIDIController, 15, 0x1A, 127, kControlReceive},

	// Special handling:
	// LED controls are not really MIDI CC#, the address upper 8 bit is an indication
	// to what Sysex command group the appropriate LED belongs.
	// During sendController () LED entries get detected and the Sysex conversion starts
	{"Rewind LED",  kMIDIController, 15, 0x0113, 127, kControlTransmit},
	{"Forward LED", kMIDIController, 15, 0x0114, 127, kControlTransmit},
	{"Play LED",    kMIDIController, 15, 0x0116, 127, kControlTransmit},
	{"Record LED",  kMIDIController, 15, 0x0117, 127, kControlTransmit},

	{"Mute 1 LED",  kMIDIController, 15, 0x0200, 127, kControlTransmit},
	{"Mute 2 LED",  kMIDIController, 15, 0x0201, 127, kControlTransmit},
	{"Mute 3 LED",  kMIDIController, 15, 0x0202, 127, kControlTransmit},
	{"Mute 4 LED",  kMIDIController, 15, 0x0203, 127, kControlTransmit},

	{"Solo LED",    kMIDIController, 15, 0x06FF, 127, kControlTransmit},

	{"Select 1 LED",kMIDIController, 15, 0x0300, 127, kControlTransmit},
	{"Select 2 LED",kMIDIController, 15, 0x0301, 127, kControlTransmit},
	{"Select 3 LED",kMIDIController, 15, 0x0302, 127, kControlTransmit},
	{"Select 4 LED",kMIDIController, 15, 0x0303, 127, kControlTransmit},

	{"Rec 1 LED", kMIDIController, 15, 0x0400, 127, kControlTransmit},
	{"Rec 2 LED", kMIDIController, 15, 0x0401, 127, kControlTransmit},
	{"Rec 3 LED", kMIDIController, 15, 0x0402, 127, kControlTransmit},
	{"Rec 4 LED", kMIDIController, 15, 0x0403, 127, kControlTransmit},

	{"Null LED",    kMIDIController, 15, 0x05FF, 127, kControlTransmit},

	{"Bank Left LED",  kMIDIController, 15, 0x07FF, 127, kControlTransmit},
	{"Bank Right LED", kMIDIController, 15, 0x08FF, 127, kControlTransmit}
};
#define kHWRemoteUS224CtrlMapEntries   (sizeof (HWRemoteUS224CtrlMap) / sizeof (HWRemoteUS224CtrlMap[0]))


//------------------------------------------------------------------------
// user function definition
// Command Category and Action can also contain an empty string ("")
// If Command Category and Action are given they will be used as default, if no user settings in the host exist
const char *HWRemoteUS224UserFunctions[] =
{
	//   User
	// Function      Command       Command
	// Key Name      Category      Action
	"Set+Play",     "Devices",     "Mixer",
	"Set+Stop",     "File",        "Close"
};
#define kHWRemoteUS224UserFunctionsEntries   (sizeof (HWRemoteUS224UserFunctions) / sizeof (HWRemoteUS224UserFunctions[0])) / 3


//------------------------------------------------------------------------
HWRemoteUS224::HWRemoteUS224 ()
: HWRemoteBase ()
{
	int32 i;

	// initialize the LED state (used to suppress unnecessary retransmits of the LED states)
	for (i = 0; i < kUS224NumFaders; i++)
	{
		muteLED[i] = false;
		selectLED[i] = false;
		recordLED[i] = false;
	}

	for (i = 0; i < kUS224NumTransportLEDs; i++)
		transportLED[i] = false;

	soloLED = false;
	nullLED = false;
	bankLeftLED = false;
	bankRightLED = false;

	// initialize Null mode handling
	nullMode = false;
	for (i = 0; i < kUS224NumFaders; i++)
	{
		faderCurrentValue[i] = 0;
		programCurrentValue[i] = 0;
		faderLower[i] = false;
		faderHigher[i] = false;
	}
};

//------------------------------------------------------------------------
HWRemoteUS224::~HWRemoteUS224 ()
{
};

//------------------------------------------------------------------------
// create function required for plugin factory,
// it will be called to create new instances of this plugin
FUnknown* HWRemoteUS224::createInstance (void* context)
{
	return (IHWRemote*)new HWRemoteUS224 ();
}

//------------------------------------------------------------------------
bool PLUGIN_API HWRemoteUS224::init (IHWRemoteStandard *standardImplementation)
{
	if (HWRemoteBase::init (standardImplementation))
	{
		//------------------------------------------------------------------------
		// 1. step
		// set special device properties
		setRemoteProperty (kHWRemotePropertyMIDITransport, 1);
		setRemoteProperty (kHWRemotePropertySupportsFaderTouch, 0);
		setRemoteProperty (kHWRemotePropertyWheelRange, 24);

		// add user functions
		addUserFunctions (HWRemoteUS224UserFunctions, kHWRemoteUS224UserFunctionsEntries);

		//------------------------------------------------------------------------
		// 1. step
		// add control descriptions
		for (int32 i = 0; i < kHWRemoteUS224CtrlMapEntries; i++)
			addRemoteControl (&HWRemoteUS224CtrlMap[i]);

		//------------------------------------------------------------------------
		// 2. step
		// create a bank
		IHWRemoteBank  *bank = createBank ("Mixer");
		IHWRemoteBank2 *bank2 = 0;	// get the IHWRemoteBank2 interface in order to access the Control Room functionality
		bank->queryInterface (IHWRemoteBank2::iid, (void**)&bank2);

		//------------------------------------------------------------------------
		// 3. step
		// populate the bank with assignments

		//------------------------------------------------------------------------
		// create a few standard layers
		bank->addLayer ("No Shift");    // used for "double" assigning different operations
		bank->addLayer ("Shift");
		bank->addLayer ("No Flip");     // used to support encoder/fader flipping
		bank->addLayer ("Flip");
		bank->addLayer ("Mute");        // used to switch between mute/solo assignment
		bank->addLayer ("Solo");
		bank->addLayer ("Select");      // used to switch between select/record enable assignment
		bank->addLayer ("Record");
		bank->addLayer ("Null");        // used to switch between null/no null assignment
		bank->addLayer ("No Null");

		//------------------------------------------------------------------------
		// add the Shift-Layer command
		// keeping the switch pressed activates the "Shift" Layer, releasing the switch will activates the "No Shift" Layer again
		bank->addCommandEntry ("No Shift", "Locator Set", "Remote", "Shift", 0);
		bank->addCommandEntry ("Shift",    "Locator Set", "Remote", "No Shift", 0);

		// pressing the switch activates the "Solo" Layer, releasing the switch has no effect
		// pressing the switch again activates the "Mute" Layer, releasing the switch has no effect
		bank->addCommandEntry ("Mute",     "Solo", "Remote", "Solo", kBankEntryValuePushType);
		bank->addCommandEntry ("Solo",     "Solo", "Remote", "Mute", kBankEntryValuePushType);

		// pressing the switch activates the "Record" Layer, releasing the switch has no effect
		// pressing the switch again activates the "Select" Layer, releasing the switch has no effect
		bank->addCommandEntry ("Select",   "Rec Enable", "Remote", "Record", kBankEntryValuePushType);
		bank->addCommandEntry ("Record",   "Rec Enable", "Remote", "Select", kBankEntryValuePushType);

		// pressing the switch activates the "Null" Layer, releasing the switch has no effect
		// pressing the switch again activates the "No Null" Layer, releasing the switch has no effect
		bank->addCommandEntry ("No Null",  "Null", "Remote", "Null", kBankEntryValuePushType);
		bank->addCommandEntry ("Null",     "Null", "Remote", "No Null", kBankEntryValuePushType);

		//------------------------------------------------------------------------
		// add transport controls
		bank->addValueEntry ("No Shift", "Rewind",  "Transport", kBankEntryDeviceChannel, "rewind",  kBankEntryValueNotAutomated);
		bank->addValueEntry ("No Shift", "Forward", "Transport", kBankEntryDeviceChannel, "forward", kBankEntryValueNotAutomated);
		bank->addValueEntry ("No Shift", "Stop",    "Transport", kBankEntryDeviceChannel, "stop",    kBankEntryValuePushType | kBankEntryValueNotAutomated);
		bank->addValueEntry ("No Shift", "Play",    "Transport", kBankEntryDeviceChannel, "start",   kBankEntryValuePushType | kBankEntryValueNotAutomated);
		bank->addValueEntry ("No Shift", "Record",  "Transport", kBankEntryDeviceChannel, "record",  kBankEntryValuePushType | kBankEntryValueToggleType | kBankEntryValueNotAutomated);

		bank->addValueEntry ("No Shift", "Rewind LED",  "Transport", kBankEntryDeviceChannel, "rewind",  kBankEntryValuePushType | kBankEntryValueNotAutomated);
		bank->addValueEntry ("No Shift", "Forward LED", "Transport", kBankEntryDeviceChannel, "forward", kBankEntryValuePushType | kBankEntryValueNotAutomated);
		bank->addValueEntry ("No Shift", "Play LED",    "Transport", kBankEntryDeviceChannel, "start",   kBankEntryValuePushType | kBankEntryValueNotAutomated);
		bank->addValueEntry ("No Shift", "Record LED",  "Transport", kBankEntryDeviceChannel, "record",  kBankEntryValuePushType | kBankEntryValueToggleType | kBankEntryValueNotAutomated);

		bank->addCommandEntry ("Shift",  "Rewind",  "Transport", "Return to Zero",  kBankEntryValuePushType);
		bank->addCommandEntry ("Shift",  "Forward", "Transport", "Goto End", kBankEntryValuePushType);

		bank->addCommandEntry("No Shift", "Locator 1", "Transport", "To Left Locator", kBankEntryValuePushType);
		bank->addCommandEntry("No Shift", "Locator 2", "Transport", "To Right Locator", kBankEntryValuePushType);
		bank->addCommandEntry("Shift",    "Locator 1", "Transport", "Set Left Locator", kBankEntryValuePushType);
		bank->addCommandEntry("Shift",    "Locator 2", "Transport", "Set Right Locator", kBankEntryValuePushType);

		//------------------------------------------------------------------------
		// add wheel group
		// abuse Record button in Shift Layer as jog mode switch
		IHWRemoteGroup *wheelGroup = bank->createGroup ("Wheel", "Wheel", kUS224NumFaders);
		wheelGroup->addControl ("No Shift", "Data Wheel", "wheel");
		wheelGroup->addControl ("Shift", "Record", "jogMode");

		//------------------------------------------------------------------------
		// add fader group
		IHWRemoteGroup *faderGroup = bank->createGroup ("Fader", "Fader Bank", kUS224NumFaders);
		faderGroup->addControl ("No Flip", "Fader 1", "1:fader");
		faderGroup->addControl ("No Flip", "Fader 2", "2:fader");
		faderGroup->addControl ("No Flip", "Fader 3", "3:fader");
		faderGroup->addControl ("No Flip", "Fader 4", "4:fader");

		// shared entries for the mute/solo buttons
		faderGroup->addControl ("Mute", "Mute 1", "1:mute");
		faderGroup->addControl ("Mute", "Mute 2", "2:mute");
		faderGroup->addControl ("Mute", "Mute 3", "3:mute");
		faderGroup->addControl ("Mute", "Mute 4", "4:mute");

		faderGroup->addControl ("Solo", "Mute 1", "1:solo");
		faderGroup->addControl ("Solo", "Mute 2", "2:solo");
		faderGroup->addControl ("Solo", "Mute 3", "3:solo");
		faderGroup->addControl ("Solo", "Mute 4", "4:solo");

		// shared entries for the mute/solo LEDs
		faderGroup->addControl ("Mute", "Mute 1 LED", "1:mute");
		faderGroup->addControl ("Mute", "Mute 2 LED", "2:mute");
		faderGroup->addControl ("Mute", "Mute 3 LED", "3:mute");
		faderGroup->addControl ("Mute", "Mute 4 LED", "4:mute");

		faderGroup->addControl ("Solo", "Mute 1 LED", "1:solo");
		faderGroup->addControl ("Solo", "Mute 2 LED", "2:solo");
		faderGroup->addControl ("Solo", "Mute 3 LED", "3:solo");
		faderGroup->addControl ("Solo", "Mute 4 LED", "4:solo");

		// shared entries for the select/record buttons
		faderGroup->addControl ("Select", "Select 1", "1:select");
		faderGroup->addControl ("Select", "Select 2", "2:select");
		faderGroup->addControl ("Select", "Select 3", "3:select");
		faderGroup->addControl ("Select", "Select 4", "4:select");

		faderGroup->addControl ("Record", "Select 1", "1:recordEnable");
		faderGroup->addControl ("Record", "Select 2", "2:recordEnable");
		faderGroup->addControl ("Record", "Select 3", "3:recordEnable");
		faderGroup->addControl ("Record", "Select 4", "4:recordEnable");

		// seperate entries for the select/record LED's
		faderGroup->addControl ("No Null", "Select 1 LED", "1:select");
		faderGroup->addControl ("No Null", "Select 2 LED", "2:select");
		faderGroup->addControl ("No Null", "Select 3 LED", "3:select");
		faderGroup->addControl ("No Null", "Select 4 LED", "4:select");
		faderGroup->addControl ("No Null", "Rec 1 LED", "1:recordEnable");
		faderGroup->addControl ("No Null", "Rec 2 LED", "2:recordEnable");
		faderGroup->addControl ("No Null", "Rec 3 LED", "3:recordEnable");
		faderGroup->addControl ("No Null", "Rec 4 LED", "4:recordEnable");

		// use volume level for the select/record LED's for the "Null" mode (this can be considered to be a hack!)
		// please also look at sendController () below, how this is dealt with
		faderGroup->addControl ("Null", "Select 1 LED", "1:fader");
		faderGroup->addControl ("Null", "Select 2 LED", "2:fader");
		faderGroup->addControl ("Null", "Select 3 LED", "3:fader");
		faderGroup->addControl ("Null", "Select 4 LED", "4:fader");
		faderGroup->addControl ("Null", "Rec 1 LED", "1:fader");
		faderGroup->addControl ("Null", "Rec 2 LED", "2:fader");
		faderGroup->addControl ("Null", "Rec 3 LED", "3:fader");
		faderGroup->addControl ("Null", "Rec 4 LED", "4:fader");

		// fader bank navigation
		faderGroup->addCommand ("No Shift", "Bank Left",  "Channel Bank Left");
		faderGroup->addCommand ("No Shift", "Bank Right", "Channel Bank Right");
		faderGroup->addCommand ("Shift",    "Bank Left",   "Channel Shift Left");
		faderGroup->addCommand ("Shift",    "Bank Right",  "Channel Shift Right");

		//------------------------------------------------------------------------
		// add master fader
		if (bank2)
		{	// assign the master fader to the Control Room Volume and the Control Room Phones Volume
			// it has its own channel map, but no fader group
			IHWRemoteChannelMap *masterChannelMap = bank->createChannelMap ("Control-Room", "Master");
			IHWRemoteChannelMap *phoneChannelMap = bank->createChannelMap ("Control-Phones", "Phone");
			bank2->addValueEntry ("No Shift", "Fader Master", "VST Control Room", 0, "volume", 0, masterChannelMap);
			bank2->addValueEntry ("Shift",    "Fader Master", "VST Control Room", 0, "volume", 0, phoneChannelMap);
		}
		else
		{
			// assign the master fader to the main mix channel
			// it has its own fader group and its own channel map
			IHWRemoteChannelMap *masterChannelMap = bank->createChannelMap ("Output", "Master");
			IHWRemoteGroup *masterGroup = bank->createGroup ("Fader", "Master Fader", 1, -1, masterChannelMap);
			masterGroup->addControl ("No Flip", "Fader Master", "1:fader");
		}

		//------------------------------------------------------------------------
		// add user function group
		// please look at HWRemoteUS224UserFunctions, where the user commands are defined
		// abuse Play/Stop buttons in Shift Layer as user commands
		IHWRemoteGroup *functionGroup = bank->createGroup ("Function", "User Function", 0);
		functionGroup->addCommand ("Shift", "Play",  "Set+Play");
		functionGroup->addCommand ("Shift", "Stop",  "Set+Stop");
		return true;
	}
	return false;
}

//------------------------------------------------------------------------
bool PLUGIN_API HWRemoteUS224::term ()
{
	// currrently there's nothing to do

	return true;
}

//------------------------------------------------------------------------
bool PLUGIN_API HWRemoteUS224::close ()
{
	// iterate through all controls and turn off the LED controls
	int32 addr = 0;
	PHWRemoteControl control;
	while (getRemoteControl (addr, control))
	{
		if (control.addr & 0x7f00)
			sendLEDController (addr, 0);
		addr++;
	}

	return HWRemoteBase::close ();
}

//------------------------------------------------------------------------
void PLUGIN_API HWRemoteUS224::onIdle ()
{
	if (nullMode)
	{	// try to transmit the current lower/higher state periodically (unnecessary changes are catched with the LED state cache)
		for (int32 i = 0; i < kUS224NumFaders; i++)
		{
			bool isLower, isHigher;
			isLower = faderCurrentValue[i] <= programCurrentValue[i];
			isHigher = faderCurrentValue[i] >= programCurrentValue[i];
			faderLower[i] = isLower;
			faderHigher[i] = isHigher;
		}

		// update the LED's on the device
		updateLowerHigherLED("Select 1 LED", faderLower[0], "Rec 1 LED", faderHigher[0]);
		updateLowerHigherLED("Select 2 LED", faderLower[1], "Rec 2 LED", faderHigher[1]);
		updateLowerHigherLED("Select 3 LED", faderLower[2], "Rec 3 LED", faderHigher[2]);
		updateLowerHigherLED("Select 4 LED", faderLower[3], "Rec 4 LED", faderHigher[3]);
	}
	HWRemoteBase::onIdle ();
}

//------------------------------------------------------------------------
void PLUGIN_API HWRemoteUS224::activateLayer (const char *layerName)
{	// activation of a layer

	// check whether the Solo LED should light up
	if (strcmp (layerName, "Mute") == 0 || strcmp (layerName, "Solo") == 0)
		activateLayerLED(layerName, "Solo", "Solo LED");
	// check whether the Null LED should light up
	else if (strcmp (layerName, "Null") == 0 || strcmp (layerName, "No Null") == 0)
	{
		nullMode = strcmp (layerName, "Null") == 0 ? true : false;
		activateLayerLED(layerName, "Null", "Null LED");
		if (nullMode)
		{
			unsigned char us428RequestFader[] = {kMIDISysexStart, 0x4E, 0x00, 0x12, 0x10, kMIDISysexEnd};
			sendStandard (us428RequestFader);
		}
		else
		{	// Null mode gets deactivated
			// we need to force an update of the abused LED's (host applications internal state does not know about this abuse)
			forceControlUpdate ("Select 1 LED");
			forceControlUpdate ("Select 2 LED");
			forceControlUpdate ("Select 3 LED");
			forceControlUpdate ("Select 4 LED");
			forceControlUpdate ("Rec 1 LED");
			forceControlUpdate ("Rec 2 LED");
			forceControlUpdate ("Rec 3 LED");
			forceControlUpdate ("Rec 4 LED");
		}
	}
}

//------------------------------------------------------------------------
void PLUGIN_API HWRemoteUS224::changeValue (int32 addr, int32 value)
{
	if (addr >= kUS224AddrFader1 && addr <= kUS224AddrFader4)
	{
		// store the current value of the fader for comparison in null mode
		faderCurrentValue[addr - kUS224AddrFader1] = value;
		if (nullMode)
			return;
	}

	HWRemoteBase::changeValue (addr, value);
}

//------------------------------------------------------------------------
void PLUGIN_API HWRemoteUS224::sendController (PHWRemoteControl *control, int32 addr, int32 value)
{
	if (addr >= kUS224AddrFader1 && addr <= kUS224AddrFader4)
	{	// store the value for the comparison in null mode, don't send it out
		programCurrentValue[addr - kUS224AddrFader1] = value;
		return;
	}
	else if (control->status == kMIDIController && control->addr > 0x7F)
	{
		// the hack for null mode, we intercept the LED controls for Null-Mode difference indication
		if (nullMode)
		{
			if ((control->addr >= 0x0300 && control->addr <= 0x303)
			    || (control->addr >= 0x0400 && control->addr <= 0x403))
			    return;
		}

		sendLEDController (addr, value);
		return;
	}
	HWRemoteBase::sendController (control, addr, value);
}


//------------------------------------------------------------------------ 
void HWRemoteUS224::forceControlUpdate (const char *control)
{
	int32 index = getRemoteControlAddr (control);
	if (index >= 0)
		forceRemoteControlUpdate (index);
}

//------------------------------------------------------------------------ 
void HWRemoteUS224::activateLayerLED(const char *activatedLayerName, const char *layer, const char *controlLED)
{	// detects whether the LED should activated for a specific layer
	int32 index = getRemoteControlAddr (controlLED);
	if (index >= 0)
		sendLEDController (index, strcmp(activatedLayerName, layer) == 0 ? 0x7f : 0);
}

//------------------------------------------------------------------------ 
void HWRemoteUS224::updateLowerHigherLED(const char *lowerLED, bool isLower, const char *higherLED, bool isHigher)
{	// transmits the lower/higher state via the given LED controls
	int32 index = getRemoteControlAddr (lowerLED);
	if (index >= 0)
		sendLEDController (index, isLower ? 0x7f : 0);

	index = getRemoteControlAddr (higherLED);
	if (index >= 0)
		sendLEDController (index, isHigher ? 0x7f : 0);
}

//------------------------------------------------------------------------ 
void HWRemoteUS224::sendLEDController (int32 addr, int32 value)
{
	// check whether the LED state needs to be transmitted (Sysex is expensive !)
	PHWRemoteControl control;
	if (getRemoteControl (addr, control))
	{
		bool *ledState;
		switch (control.addr >> 8)
		{
			case 1:
				ledState = &transportLED[(control.addr & 0x7f) - 0x13];
				break;
			case 2:
				ledState = &muteLED[control.addr & 0x7f];
				break;
			case 3:
				ledState = &selectLED[control.addr & 0x7f];
				break;
			case 4:
				ledState = &recordLED[control.addr & 0x7f];
				break;
			case 5:
				ledState = &nullLED;
				break;
			case 6:
				ledState = &soloLED;
				break;
			case 7:
				ledState = &bankLeftLED;
				break;
			case 8:
				ledState = &bankRightLED;
				break;
			default:
				return;
		}
		if ((value != 0) != (*ledState != false))
		{
			if (value)
				*ledState = true;
			else
				*ledState = false;

			unsigned char msg[8] = {kMIDISysexStart, 0x4e, 0x00, 0x12};
			if ((control.addr & 0x00FF) == 0x00FF)
			{	// "single byte" LED's
				msg[4] = (unsigned char)(control.addr >> 8) & 0x7f;
				msg[5] = (unsigned char)(value != 0 ? 0x7f : 0x00);
				msg[6] = kMIDISysexEnd;
			}
			else
			{	// "multi byte" LED's
				msg[4] = (unsigned char)((control.addr >> 8) & 0x7f);
				msg[5] = (unsigned char)(control.addr & 0x7f);
				msg[6] = (unsigned char)(value != 0 ? 0x7f : 0x00);
				msg[7] = kMIDISysexEnd;
			}
			sendStandard (msg);
		}
	}
}

//-----------------------------------------------------------------------------
// Steinberg Remote Interface Software Development Kit (SDK)
// Version 2.1    Date : 2008
//
// Category     : Devices
// Filename     : hwremote-us224-detect.h
// Created by   : Steinberg
// Description  : Device Detection Tascam US-224 Sample
//-----------------------------------------------------------------------------
// LICENSE
// (c) 2010, Steinberg Media Technologies GmbH
//-----------------------------------------------------------------------------
// This Software Development Kit may not be distributed in parts or its entirety 
// without prior written agreement by Steinberg Media Technologies GmbH. This
// SDK must not be used to re-engineer or manipulate any technology used in any
// Steinberg or Third-party application or software module, unless permitted by
// law.
//-----------------------------------------------------------------------------

#ifndef __hwremoteus224detect__
#define __hwremoteus224detect__

//-----------------------------------------------------------------------------
// SDK Interface Headers
#ifndef __idetector__
#include "pluginterfaces/devices/idetector.h"
#endif
#include "pluginterfaces/host/ihostclasses.h"

using namespace Steinberg;

//-----------------------------------------------------------------------------

class HWRemoteUS224Detect : public IPluginBase, IDetector
{
public:
	HWRemoteUS224Detect ();
	~HWRemoteUS224Detect ();

	//------------------------------------------------------------------------
	// create function required for plugin factory,
	// it will be called to create new instances of this plugin
	static FUnknown* createInstance (void* context);

	//-----------------------------------------------------------------------------
	// Interface methods:
	//-----------------------------------------------------------------------------
	DECLARE_FUNKNOWN_METHODS

	// IPluginBase methods
	tresult PLUGIN_API initialize (FUnknown* context);
	tresult PLUGIN_API terminate ();

	// IDetector methods
	tresult PLUGIN_API detect ();
	tresult PLUGIN_API getDeviceTypeInfo (struct IDetectorDeviceInfo* devInfo);

private:
	IHostClasses* hostClasses;
};

// HW Remote US224 Component UID definition
#define kHWRemoteUS224DetectUID     INLINE_UID (0xA3A365AD, 0xF92E49D6, 0xA99C1E05, 0xD81C3CD5)

#endif

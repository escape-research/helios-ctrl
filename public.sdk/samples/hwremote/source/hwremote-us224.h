//-----------------------------------------------------------------------------
// Steinberg Remote Interface Software Development Kit (SDK)
// Version 2.1    Date : 2008
//
// Category     : HW Remote Controller
// Filename     : hwremote-us224.h
// Created by   : Steinberg
// Description  : HW Remote Controller Tascam US-224 Sample implementation
//-----------------------------------------------------------------------------
// LICENSE
// (c) 2010, Steinberg Media Technologies GmbH
//-----------------------------------------------------------------------------
// This Software Development Kit may not be distributed in parts or its entirety 
// without prior written agreement by Steinberg Media Technologies GmbH. This
// SDK must not be used to re-engineer or manipulate any technology used in any
// Steinberg or Third-party application or software module, unless permitted by
// law.
//-----------------------------------------------------------------------------

#ifndef __hwremoteus224__
#define __hwremoteus224__

#ifndef __hwremotebase__
#include "hwremote/hwremotebase.h"
#endif

using namespace Steinberg;

enum
{
	// data ranges
	kUS224NumFaders = 4,             // 4 faders for the fader group
	kUS224NumTransportLEDs = 5,

	// pre-catched addresses for the faders (need to be in sync with the position in the HWRemoteUS224CtrlMap)
	kUS224AddrFader1 = 0,
	kUS224AddrFader2 = 1,
	kUS224AddrFader3 = 2,
	kUS224AddrFader4 = 3
};


class HWRemoteUS224 : public Steinberg::HWRemoteBase
{
public:
	//------------------------------------------------------------------------
	HWRemoteUS224 ();
	virtual ~HWRemoteUS224 ();

	//------------------------------------------------------------------------
	// create function required for plugin factory,
	// it will be called to create new instances of this plugin
	static FUnknown* createInstance (void* context);

	//------------------------------------------------------------------------
	// overloaded methods implement the hw remote specific functionality
	bool PLUGIN_API init (class IHWRemoteStandard *standardImplementation);
	bool PLUGIN_API term ();

	bool PLUGIN_API close ();

	void PLUGIN_API changeValue (int32 addr, int32 value);
	void PLUGIN_API sendController (PHWRemoteControl *control, int32 addr, int32 value);

	void PLUGIN_API activateLayer (const char *layerName);   // activation of a layer

	void PLUGIN_API onIdle ();			// called periodically in UI thread context

private:
	void forceControlUpdate (const char *control);
	void updateLowerHigherLED(const char *lowerLED, bool isLower, const char *higherLED, bool isHigher);
	void activateLayerLED(const char *activatedLayerName, const char *layer, const char *controlLED);
	void sendLEDController (int32 addr, int32 value);

	// cache of the LED states
	bool muteLED[kUS224NumFaders];
	bool selectLED[kUS224NumFaders];
	bool recordLED[kUS224NumFaders];
	bool transportLED[kUS224NumTransportLEDs];
	bool soloLED;
	bool nullLED;
	bool bankLeftLED;
	bool bankRightLED;

	// Null mode handling
	bool nullMode;
	int32 faderCurrentValue[kUS224NumFaders];
	int32 programCurrentValue[kUS224NumFaders];
	bool faderLower[kUS224NumFaders];
	bool faderHigher[kUS224NumFaders];
};

#define kHWRemoteUS224UID     INLINE_UID (0x4D8CDC09, 0xDA6111D8, 0xB5CA000A, 0x959B31C6)

#endif

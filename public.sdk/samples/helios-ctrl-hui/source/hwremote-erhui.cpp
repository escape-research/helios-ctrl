//-----------------------------------------------------------------------------
// Steinberg Remote Interface Software Development Kit (SDK)
// Version 2.1    Date : 2008
//
// Category     : HW Remote Controller
// Filename     : hwremote-us224.cpp
// Created by   : Steinberg
// Description  : HW Remote Controller Tascam US-224 Sample implementation
//-----------------------------------------------------------------------------
// LICENSE
// (c) 2010, Steinberg Media Technologies GmbH
//-----------------------------------------------------------------------------
// This Software Development Kit may not be distributed in parts or its entirety 
// without prior written agreement by Steinberg Media Technologies GmbH. This
// SDK must not be used to re-engineer or manipulate any technology used in any
// Steinberg or Third-party application or software module, unless permitted by
// law.
//-----------------------------------------------------------------------------

#include "hwremote-erhui.h"

//------------------------------------------------------------------------
// descriptions of the controls for the HW Remote
//------------------------------------------------------------------------
struct PHWRemoteControl HWRemoteERHUICtrlMap[] =
{
	// The special HUI keep alive message
	{ "KeepAlive", kMIDINoteOn, 0, 0x00, 127, kControlReceive | kControlTransmit },

	{ "Shift On", kMIDIController, 0, 0x0F, 0x0841, kControlReceive | kControlMSBLSB },
	{ "Shift Off", kMIDIController, 0, 0x0F, 0x0801, kControlReceive | kControlMSBLSB },

	{ "Fader 1", kMIDIController, 0, 0x00, 0x3FF0, kControlReceive | kControlTransmit | kControlMSBLSB | kControlRequiresRetransmit },
	{ "Fader 2", kMIDIController, 0, 0x01, 0x3FF0, kControlReceive | kControlTransmit | kControlMSBLSB | kControlRequiresRetransmit },
	{ "Fader 3", kMIDIController, 0, 0x02, 0x3FF0, kControlReceive | kControlTransmit | kControlMSBLSB | kControlRequiresRetransmit },
	{ "Fader 4", kMIDIController, 0, 0x03, 0x3FF0, kControlReceive | kControlTransmit | kControlMSBLSB | kControlRequiresRetransmit },
	{ "Fader 5", kMIDIController, 0, 0x04, 0x3FF0, kControlReceive | kControlTransmit | kControlMSBLSB | kControlRequiresRetransmit },
	{ "Fader 6", kMIDIController, 0, 0x05, 0x3FF0, kControlReceive | kControlTransmit | kControlMSBLSB | kControlRequiresRetransmit },
	{ "Fader 7", kMIDIController, 0, 0x06, 0x3FF0, kControlReceive | kControlTransmit | kControlMSBLSB | kControlRequiresRetransmit },
	{ "Fader 8", kMIDIController, 0, 0x07, 0x3FF0, kControlReceive | kControlTransmit | kControlMSBLSB | kControlRequiresRetransmit },

	{ "Fader 8 Touch", kMIDIController, 0, 0x0F, 0x4007, kControlReceive | kControlMSBLSB },
	{ "Fader 7 Touch", kMIDIController, 0, 0x0F, 0x4006, kControlReceive | kControlMSBLSB },
	{ "Fader 6 Touch", kMIDIController, 0, 0x0F, 0x4005, kControlReceive | kControlMSBLSB },
	{ "Fader 5 Touch", kMIDIController, 0, 0x0F, 0x4004, kControlReceive | kControlMSBLSB },
	{ "Fader 4 Touch", kMIDIController, 0, 0x0F, 0x4003, kControlReceive | kControlMSBLSB },
	{ "Fader 3 Touch", kMIDIController, 0, 0x0F, 0x4002, kControlReceive | kControlMSBLSB },
	{ "Fader 2 Touch", kMIDIController, 0, 0x0F, 0x4001, kControlReceive | kControlMSBLSB },
	{ "Fader 1 Touch", kMIDIController, 0, 0x0F, 0x4000, kControlReceive | kControlMSBLSB },

	/*
	{ "Fader 8 Release", kMIDIController, 0, 0x0F, 0x0007, kControlReceive | kControlMSBLSB },
	{ "Fader 7 Release", kMIDIController, 0, 0x0F, 0x0006, kControlReceive | kControlMSBLSB },
	{ "Fader 6 Release", kMIDIController, 0, 0x0F, 0x0005, kControlReceive | kControlMSBLSB },
	{ "Fader 5 Release", kMIDIController, 0, 0x0F, 0x0004, kControlReceive | kControlMSBLSB },
	{ "Fader 4 Release", kMIDIController, 0, 0x0F, 0x0003, kControlReceive | kControlMSBLSB },
	{ "Fader 3 Release", kMIDIController, 0, 0x0F, 0x0002, kControlReceive | kControlMSBLSB },
	{ "Fader 2 Release", kMIDIController, 0, 0x0F, 0x0001, kControlReceive | kControlMSBLSB },
	{ "Fader 1 Release", kMIDIController, 0, 0x0F, 0x0000, kControlReceive | kControlMSBLSB },
	*/

	{ "Mute 1", kControlJLCCtrl, 0, 0x0F, 0x0042, kControlReceive | kControlMSBLSB },
	{ "Mute 2", kControlJLCCtrl, 0, 0x0F, 0x0142, kControlReceive | kControlMSBLSB },
	{ "Mute 3", kControlJLCCtrl, 0, 0x0F, 0x0242, kControlReceive | kControlMSBLSB },
	{ "Mute 4", kControlJLCCtrl, 0, 0x0F, 0x0342, kControlReceive | kControlMSBLSB },
	{ "Mute 5", kControlJLCCtrl, 0, 0x0F, 0x0442, kControlReceive | kControlMSBLSB },
	{ "Mute 6", kControlJLCCtrl, 0, 0x0F, 0x0542, kControlReceive | kControlMSBLSB },
	{ "Mute 7", kControlJLCCtrl, 0, 0x0F, 0x0642, kControlReceive | kControlMSBLSB },
	{ "Mute 8", kControlJLCCtrl, 0, 0x0F, 0x0742, kControlReceive | kControlMSBLSB },

	{ "Mute 1 LED", kControlJLCCtrl, 0, 0x0C, 0x0042, kControlTransmit | kControlMSBLSB },
	{ "Mute 2 LED", kControlJLCCtrl, 0, 0x0C, 0x0142, kControlTransmit | kControlMSBLSB },
	{ "Mute 3 LED", kControlJLCCtrl, 0, 0x0C, 0x0242, kControlTransmit | kControlMSBLSB },
	{ "Mute 4 LED", kControlJLCCtrl, 0, 0x0C, 0x0342, kControlTransmit | kControlMSBLSB },
	{ "Mute 5 LED", kControlJLCCtrl, 0, 0x0C, 0x0442, kControlTransmit | kControlMSBLSB },
	{ "Mute 6 LED", kControlJLCCtrl, 0, 0x0C, 0x0542, kControlTransmit | kControlMSBLSB },
	{ "Mute 7 LED", kControlJLCCtrl, 0, 0x0C, 0x0642, kControlTransmit | kControlMSBLSB },
	{ "Mute 8 LED", kControlJLCCtrl, 0, 0x0C, 0x0742, kControlTransmit | kControlMSBLSB },

	{ "Solo 1", kControlJLCCtrl, 0, 0x0F, 0x0043, kControlReceive | kControlMSBLSB },
	{ "Solo 2", kControlJLCCtrl, 0, 0x0F, 0x0143, kControlReceive | kControlMSBLSB },
	{ "Solo 3", kControlJLCCtrl, 0, 0x0F, 0x0243, kControlReceive | kControlMSBLSB },
	{ "Solo 4", kControlJLCCtrl, 0, 0x0F, 0x0343, kControlReceive | kControlMSBLSB },
	{ "Solo 5", kControlJLCCtrl, 0, 0x0F, 0x0443, kControlReceive | kControlMSBLSB },
	{ "Solo 6", kControlJLCCtrl, 0, 0x0F, 0x0543, kControlReceive | kControlMSBLSB },
	{ "Solo 7", kControlJLCCtrl, 0, 0x0F, 0x0643, kControlReceive | kControlMSBLSB },
	{ "Solo 8", kControlJLCCtrl, 0, 0x0F, 0x0743, kControlReceive | kControlMSBLSB },

	{ "Solo 1 LED", kControlJLCCtrl, 0, 0x0C, 0x0043, kControlTransmit | kControlMSBLSB },
	{ "Solo 2 LED", kControlJLCCtrl, 0, 0x0C, 0x0143, kControlTransmit | kControlMSBLSB },
	{ "Solo 3 LED", kControlJLCCtrl, 0, 0x0C, 0x0243, kControlTransmit | kControlMSBLSB },
	{ "Solo 4 LED", kControlJLCCtrl, 0, 0x0C, 0x0343, kControlTransmit | kControlMSBLSB },
	{ "Solo 5 LED", kControlJLCCtrl, 0, 0x0C, 0x0443, kControlTransmit | kControlMSBLSB },
	{ "Solo 6 LED", kControlJLCCtrl, 0, 0x0C, 0x0543, kControlTransmit | kControlMSBLSB },
	{ "Solo 7 LED", kControlJLCCtrl, 0, 0x0C, 0x0643, kControlTransmit | kControlMSBLSB },
	{ "Solo 8 LED", kControlJLCCtrl, 0, 0x0C, 0x0743, kControlTransmit | kControlMSBLSB },

	{ "Mute Defeat", kControlJLCCtrl, 0, 0X0F, 0x0C44, kControlReceive | kControlMSBLSB },
	{ "Solo Defeat", kControlJLCCtrl, 0, 0X0F, 0x0C45, kControlReceive | kControlMSBLSB },

	{ "Select 1", kControlJLCCtrl, 0, 0x0F, 0x0041, kControlReceive | kControlMSBLSB },
	{ "Select 2", kControlJLCCtrl, 0, 0x0F, 0x0141, kControlReceive | kControlMSBLSB },
	{ "Select 3", kControlJLCCtrl, 0, 0x0F, 0x0241, kControlReceive | kControlMSBLSB },
	{ "Select 4", kControlJLCCtrl, 0, 0x0F, 0x0341, kControlReceive | kControlMSBLSB },
	{ "Select 5", kControlJLCCtrl, 0, 0x0F, 0x0441, kControlReceive | kControlMSBLSB },
	{ "Select 6", kControlJLCCtrl, 0, 0x0F, 0x0541, kControlReceive | kControlMSBLSB },
	{ "Select 7", kControlJLCCtrl, 0, 0x0F, 0x0641, kControlReceive | kControlMSBLSB },
	{ "Select 8", kControlJLCCtrl, 0, 0x0F, 0x0741, kControlReceive | kControlMSBLSB },

	{ "Select 1 LED", kControlJLCCtrl, 0, 0x0C, 0x0041, kControlTransmit | kControlMSBLSB },
	{ "Select 2 LED", kControlJLCCtrl, 0, 0x0C, 0x0141, kControlTransmit | kControlMSBLSB },
	{ "Select 3 LED", kControlJLCCtrl, 0, 0x0C, 0x0241, kControlTransmit | kControlMSBLSB },
	{ "Select 4 LED", kControlJLCCtrl, 0, 0x0C, 0x0341, kControlTransmit | kControlMSBLSB },
	{ "Select 5 LED", kControlJLCCtrl, 0, 0x0C, 0x0441, kControlTransmit | kControlMSBLSB },
	{ "Select 6 LED", kControlJLCCtrl, 0, 0x0C, 0x0541, kControlTransmit | kControlMSBLSB },
	{ "Select 7 LED", kControlJLCCtrl, 0, 0x0C, 0x0641, kControlTransmit | kControlMSBLSB },
	{ "Select 8 LED", kControlJLCCtrl, 0, 0x0C, 0x0741, kControlTransmit | kControlMSBLSB },

	{ "Rec 1", kControlJLCCtrl, 0, 0x0F, 0x0048, kControlReceive | kControlMSBLSB },
	{ "Rec 2", kControlJLCCtrl, 0, 0x0F, 0x0148, kControlReceive | kControlMSBLSB },
	{ "Rec 3", kControlJLCCtrl, 0, 0x0F, 0x0248, kControlReceive | kControlMSBLSB },
	{ "Rec 4", kControlJLCCtrl, 0, 0x0F, 0x0348, kControlReceive | kControlMSBLSB },
	{ "Rec 5", kControlJLCCtrl, 0, 0x0F, 0x0448, kControlReceive | kControlMSBLSB },
	{ "Rec 6", kControlJLCCtrl, 0, 0x0F, 0x0548, kControlReceive | kControlMSBLSB },
	{ "Rec 7", kControlJLCCtrl, 0, 0x0F, 0x0648, kControlReceive | kControlMSBLSB },
	{ "Rec 8", kControlJLCCtrl, 0, 0x0F, 0x0748, kControlReceive | kControlMSBLSB },

	{ "Rec 1 LED", kControlJLCCtrl, 0, 0x0C, 0x0048, kControlTransmit | kControlMSBLSB },
	{ "Rec 2 LED", kControlJLCCtrl, 0, 0x0C, 0x0148, kControlTransmit | kControlMSBLSB },
	{ "Rec 3 LED", kControlJLCCtrl, 0, 0x0C, 0x0248, kControlTransmit | kControlMSBLSB },
	{ "Rec 4 LED", kControlJLCCtrl, 0, 0x0C, 0x0348, kControlTransmit | kControlMSBLSB },
	{ "Rec 5 LED", kControlJLCCtrl, 0, 0x0C, 0x0448, kControlTransmit | kControlMSBLSB },
	{ "Rec 6 LED", kControlJLCCtrl, 0, 0x0C, 0x0548, kControlTransmit | kControlMSBLSB },
	{ "Rec 7 LED", kControlJLCCtrl, 0, 0x0C, 0x0648, kControlTransmit | kControlMSBLSB },
	{ "Rec 8 LED", kControlJLCCtrl, 0, 0x0C, 0x0748, kControlTransmit | kControlMSBLSB },

	{ "Bank Left", kControlJLCCtrl, 0, 0x0F, 0x0A41, kControlReceive | kControlMSBLSB },
	{ "Bank Right", kControlJLCCtrl, 0, 0x0F, 0x0A43, kControlReceive | kControlMSBLSB },
	{ "Channel Left", kControlJLCCtrl, 0, 0x0F, 0x0A40, kControlReceive | kControlMSBLSB },
	{ "Channel Right", kControlJLCCtrl, 0, 0x0F, 0x0A42, kControlReceive | kControlMSBLSB },

	{ "Rewind", kControlJLCCtrl, 0, 0x0F, 0x0E41, kControlReceive | kControlMSBLSB },
	{ "Forward", kControlJLCCtrl, 0, 0x0F, 0x0E42, kControlReceive | kControlMSBLSB },
	{ "Stop", kControlJLCCtrl, 0, 0x0F, 0x0E43, kControlReceive | kControlMSBLSB },
	{ "Play", kControlJLCCtrl, 0, 0x0F, 0x0E44, kControlReceive | kControlMSBLSB },
	{ "Record", kControlJLCCtrl, 0, 0x0F, 0x0E45, kControlReceive | kControlMSBLSB },

	{ "Rewind LED", kControlJLCCtrl, 0, 0x0C, 0x0E41, kControlTransmit | kControlMSBLSB },
	{ "Forward LED", kControlJLCCtrl, 0, 0x0C, 0x0E42, kControlTransmit | kControlMSBLSB },
	{ "Stop LED", kControlJLCCtrl, 0, 0x0C, 0x0E43, kControlTransmit | kControlMSBLSB },
	{ "Play LED", kControlJLCCtrl, 0, 0x0C, 0x0E44, kControlTransmit | kControlMSBLSB },
	{ "Record LED", kControlJLCCtrl, 0, 0x0C, 0x0E45, kControlTransmit | kControlMSBLSB },

	{ "Return to Zero", kControlJLCCtrl, 0, 0x0F, 0x0F40, kControlReceive | kControlMSBLSB },
	{ "Goto End", kControlJLCCtrl, 0, 0x0F, 0x0F41, kControlReceive | kControlMSBLSB },

	/*
	// Special handling:
	// Fader 1 to 4 actually do not need transmission, but the plugin require the programs
	// internal state of the fader values for the "Null" mode
	// During sendController () the message is intercepted and never past on to the device
	{"Fader Master",kMIDIController, 15, 0x4B, 127, kControlReceive},

	{"Data Wheel",  kMIDIController, 15, 0x60, 24, kControlReceive | kControlRelative},

	{"Mute 1",      kMIDIController, 15, 0x00, 127, kControlReceive},
	{"Mute 2",      kMIDIController, 15, 0x01, 127, kControlReceive},
	{"Mute 3",      kMIDIController, 15, 0x02, 127, kControlReceive},
	{"Mute 4",      kMIDIController, 15, 0x03, 127, kControlReceive},

	{"Solo",        kMIDIController, 15, 0x2A, 127, kControlReceive},

	{"Select 1",    kMIDIController, 15, 0x20, 127, kControlReceive},
	{"Select 2",    kMIDIController, 15, 0x21, 127, kControlReceive},
	{"Select 3",    kMIDIController, 15, 0x22, 127, kControlReceive},
	{"Select 4",    kMIDIController, 15, 0x23, 127, kControlReceive},

	{"Rec Enable",  kMIDIController, 15, 0x29, 127, kControlReceive},

	{"Null",        kMIDIController, 15, 0x28, 127, kControlReceive},

	{"Bank Left",   kMIDIController, 15, 0x10, 127, kControlReceive},
	{"Bank Right",  kMIDIController, 15, 0x11, 127, kControlReceive},

	{"Rewind",      kMIDIController, 15, 0x13, 127, kControlReceive},
	{"Forward",     kMIDIController, 15, 0x14, 127, kControlReceive},
	{"Stop",        kMIDIController, 15, 0x15, 127, kControlReceive},
	{"Play",        kMIDIController, 15, 0x16, 127, kControlReceive},
	{"Record",      kMIDIController, 15, 0x17, 127, kControlReceive},

	{"Locator 1",   kMIDIController, 15, 0x18, 127, kControlReceive},
	{"Locator 2",   kMIDIController, 15, 0x19, 127, kControlReceive},
	{"Locator Set", kMIDIController, 15, 0x1A, 127, kControlReceive},

	// Special handling:
	// LED controls are not really MIDI CC#, the address upper 8 bit is an indication
	// to what Sysex command group the appropriate LED belongs.
	// During sendController () LED entries get detected and the Sysex conversion starts
	{"Rewind LED",  kMIDIController, 15, 0x0113, 127, kControlTransmit},
	{"Forward LED", kMIDIController, 15, 0x0114, 127, kControlTransmit},
	{"Play LED",    kMIDIController, 15, 0x0116, 127, kControlTransmit},
	{"Record LED",  kMIDIController, 15, 0x0117, 127, kControlTransmit},

	{"Mute 1 LED",  kMIDIController, 15, 0x0200, 127, kControlTransmit},
	{"Mute 2 LED",  kMIDIController, 15, 0x0201, 127, kControlTransmit},
	{"Mute 3 LED",  kMIDIController, 15, 0x0202, 127, kControlTransmit},
	{"Mute 4 LED",  kMIDIController, 15, 0x0203, 127, kControlTransmit},

	{"Solo LED",    kMIDIController, 15, 0x06FF, 127, kControlTransmit},

	{"Select 1 LED",kMIDIController, 15, 0x0300, 127, kControlTransmit},
	{"Select 2 LED",kMIDIController, 15, 0x0301, 127, kControlTransmit},
	{"Select 3 LED",kMIDIController, 15, 0x0302, 127, kControlTransmit},
	{"Select 4 LED",kMIDIController, 15, 0x0303, 127, kControlTransmit},

	{"Rec 1 LED", kMIDIController, 15, 0x0400, 127, kControlTransmit},
	{"Rec 2 LED", kMIDIController, 15, 0x0401, 127, kControlTransmit},
	{"Rec 3 LED", kMIDIController, 15, 0x0402, 127, kControlTransmit},
	{"Rec 4 LED", kMIDIController, 15, 0x0403, 127, kControlTransmit},

	{"Null LED",    kMIDIController, 15, 0x05FF, 127, kControlTransmit},

	{"Bank Left LED",  kMIDIController, 15, 0x07FF, 127, kControlTransmit},
	{"Bank Right LED", kMIDIController, 15, 0x08FF, 127, kControlTransmit} */
};
#define kHWRemoteERHUICtrlMapEntries   (sizeof (HWRemoteERHUICtrlMap) / sizeof (HWRemoteERHUICtrlMap[0]))


//------------------------------------------------------------------------
// user function definition
// Command Category and Action can also contain an empty string ("")
// If Command Category and Action are given they will be used as default, if no user settings in the host exist
const char *HWRemoteERHUIUserFunctions[] =
{
	//   User
	// Function      Command       Command
	// Key Name      Category      Action
	"Set+Play",     "Devices",     "Mixer",
	"Set+Stop",     "File",        "Close"
};
#define kHWRemoteERHUIUserFunctionsEntries   (sizeof (HWRemoteERHUIUserFunctions) / sizeof (HWRemoteERHUIUserFunctions[0])) / 3


//------------------------------------------------------------------------
HWRemoteERHUI::HWRemoteERHUI ()
: HWRemoteBase ()
{

	ChanneldisplayID = 0;	// initilize the channel display ID

/*	int32 i;

	// initialize the LED state (used to suppress unnecessary retransmits of the LED states)
	for (i = 0; i < kERHUINumFaders; i++)
	{
		muteLED[i] = false;
		selectLED[i] = false;
		recordLED[i] = false;
	}

	for (i = 0; i < kERHUINumTransportLEDs; i++)
		transportLED[i] = false;

	soloLED = false;
	nullLED = false;
	bankLeftLED = false;
	bankRightLED = false;

	// initialize Null mode handling
	nullMode = false;
	for (i = 0; i < kERHUINumFaders; i++)
	{
		faderCurrentValue[i] = 0;
		programCurrentValue[i] = 0;
		faderLower[i] = false;
		faderHigher[i] = false;
	}*/
};

//------------------------------------------------------------------------
HWRemoteERHUI::~HWRemoteERHUI ()
{
};

//------------------------------------------------------------------------
// create function required for plugin factory,
// it will be called to create new instances of this plugin
FUnknown* HWRemoteERHUI::createInstance (void* context)
{
	return (IHWRemote*)new HWRemoteERHUI ();
}

//------------------------------------------------------------------------
bool PLUGIN_API HWRemoteERHUI::init (IHWRemoteStandard *standardImplementation)
{
	if (HWRemoteBase::init (standardImplementation))
	{
		// CReate the 1 sec timer for the keep Alive
		

		//------------------------------------------------------------------------
		// 1. step
		// set special device properties
		setRemoteProperty (kHWRemotePropertyMIDITransport, 1);
		setRemoteProperty (kHWRemotePropertySupportsFaderTouch, 1);
		setRemoteProperty (kHWRemotePropertyWheelRange, 24);

		// add user functions
		addUserFunctions (HWRemoteERHUIUserFunctions, kHWRemoteERHUIUserFunctionsEntries);

		//------------------------------------------------------------------------
		// 1. step
		// add control descriptions
		for (int32 i = 0; i < kHWRemoteERHUICtrlMapEntries; i++)
			addRemoteControl (&HWRemoteERHUICtrlMap[i]);

		//------------------------------------------------------------------------
		// 2. step
		// create a bank
		IHWRemoteBank  *bank = createBank ("Mixer");
		IHWRemoteBank2 *bank2 = 0;	// get the IHWRemoteBank2 interface in order to access the Control Room functionality
		bank->queryInterface (IHWRemoteBank2::iid, (void**)&bank2);

		//------------------------------------------------------------------------
		// 3. step
		// populate the bank with assignments

		//------------------------------------------------------------------------
		// create a few standard layers
		bank->addLayer ("No Shift");    // used for "double" assigning different operations
		bank->addLayer ("Shift");
		bank->addLayer ("No Flip");     // used to support encoder/fader flipping
		bank->addLayer ("Flip");
		bank->addLayer ("No Default");	// used to reset channel fader and vpots to default value
		bank->addLayer ("Default");
		//bank->addLayer ("Mute");        // used to switch between mute/solo assignment
		//bank->addLayer ("Solo");
		//bank->addLayer ("Select");      // used to switch between select/record enable assignment
		//bank->addLayer ("Record");
		//bank->addLayer ("Null");        // used to switch between null/no null assignment
		//bank->addLayer ("No Null");

		//------------------------------------------------------------------------
		// add the Shift-Layer command
		// keeping the switch pressed activates the "Shift" Layer, releasing the switch will activates the "No Shift" Layer again
		bank->addCommandEntry ("No Shift", "Shift On", "Remote", "Shift", 0);
		bank->addCommandEntry ("Shift",    "Shift Off", "Remote", "No Shift", 0);

		// pressing the switch activates the "Solo" Layer, releasing the switch has no effect
		// pressing the switch again activates the "Mute" Layer, releasing the switch has no effect
		//bank->addCommandEntry ("Mute",     "Solo", "Remote", "Solo", kBankEntryValuePushType);
		//bank->addCommandEntry ("Solo",     "Solo", "Remote", "Mute", kBankEntryValuePushType);

		// pressing the switch activates the "Record" Layer, releasing the switch has no effect
		// pressing the switch again activates the "Select" Layer, releasing the switch has no effect
		//bank->addCommandEntry ("Select",   "Rec Enable", "Remote", "Record", kBankEntryValuePushType);
		//bank->addCommandEntry ("Record",   "Rec Enable", "Remote", "Select", kBankEntryValuePushType);

		// pressing the switch activates the "Null" Layer, releasing the switch has no effect
		// pressing the switch again activates the "No Null" Layer, releasing the switch has no effect
		//bank->addCommandEntry ("No Null",  "Null", "Remote", "Null", kBankEntryValuePushType);
		//bank->addCommandEntry ("Null",     "Null", "Remote", "No Null", kBankEntryValuePushType);

		//------------------------------------------------------------------------
		// add transport controls
		bank->addValueEntry ("No Shift", "Rewind",  "Transport", kBankEntryDeviceChannel, "rewind",  kBankEntryValueNotAutomated);
		bank->addValueEntry ("No Shift", "Forward", "Transport", kBankEntryDeviceChannel, "forward", kBankEntryValueNotAutomated);
		bank->addValueEntry ("No Shift", "Stop",    "Transport", kBankEntryDeviceChannel, "stop",    kBankEntryValuePushType | kBankEntryValueNotAutomated);
		bank->addValueEntry ("No Shift", "Play",    "Transport", kBankEntryDeviceChannel, "start",   kBankEntryValuePushType | kBankEntryValueNotAutomated);
		bank->addValueEntry ("No Shift", "Record",  "Transport", kBankEntryDeviceChannel, "record",  kBankEntryValuePushType | kBankEntryValueToggleType | kBankEntryValueNotAutomated);

		bank->addValueEntry ("No Shift", "Rewind LED",  "Transport", kBankEntryDeviceChannel, "rewind",  kBankEntryValuePushType | kBankEntryValueNotAutomated);
		bank->addValueEntry ("No Shift", "Forward LED", "Transport", kBankEntryDeviceChannel, "forward", kBankEntryValuePushType | kBankEntryValueNotAutomated);
		bank->addValueEntry ("No Shift", "Stop LED", "Transport", kBankEntryDeviceChannel, "stop", kBankEntryValuePushType | kBankEntryValueNotAutomated);
		bank->addValueEntry ("No Shift", "Play LED", "Transport", kBankEntryDeviceChannel, "start", kBankEntryValuePushType | kBankEntryValueNotAutomated);
		bank->addValueEntry ("No Shift", "Record LED",  "Transport", kBankEntryDeviceChannel, "record",  kBankEntryValuePushType | kBankEntryValueToggleType | kBankEntryValueNotAutomated);

		bank->addCommandEntry ("Shift",  "Return to Zero",  "Transport", "Return to Zero",  kBankEntryValuePushType);
		bank->addCommandEntry ("Shift",  "Goto End", "Transport", "Goto End", kBankEntryValuePushType);

		//bank->addValueEntry ("No Shift", "Rewind",  "Transport", kBankEntryDeviceChannel, "rewind",  kBankEntryValueNotAutomated);
		//bank->addValueEntry ("No Shift", "Forward", "Transport", kBankEntryDeviceChannel, "forward", kBankEntryValueNotAutomated);
		//bank->addValueEntry ("No Shift", "Stop",    "Transport", kBankEntryDeviceChannel, "stop",    kBankEntryValuePushType | kBankEntryValueNotAutomated);
		//bank->addValueEntry ("No Shift", "Play",    "Transport", kBankEntryDeviceChannel, "start",   kBankEntryValuePushType | kBankEntryValueNotAutomated);
		//bank->addValueEntry ("No Shift", "Record",  "Transport", kBankEntryDeviceChannel, "record",  kBankEntryValuePushType | kBankEntryValueToggleType | kBankEntryValueNotAutomated);

		//bank->addValueEntry ("No Shift", "Rewind LED",  "Transport", kBankEntryDeviceChannel, "rewind",  kBankEntryValuePushType | kBankEntryValueNotAutomated);
		//bank->addValueEntry ("No Shift", "Forward LED", "Transport", kBankEntryDeviceChannel, "forward", kBankEntryValuePushType | kBankEntryValueNotAutomated);
		//bank->addValueEntry ("No Shift", "Play LED",    "Transport", kBankEntryDeviceChannel, "start",   kBankEntryValuePushType | kBankEntryValueNotAutomated);
		//bank->addValueEntry ("No Shift", "Record LED",  "Transport", kBankEntryDeviceChannel, "record",  kBankEntryValuePushType | kBankEntryValueToggleType | kBankEntryValueNotAutomated);

		//bank->addCommandEntry ("Shift",  "Rewind",  "Transport", "Return to Zero",  kBankEntryValuePushType);
		//bank->addCommandEntry ("Shift",  "Forward", "Transport", "Goto End", kBankEntryValuePushType);

		//bank->addCommandEntry("No Shift", "Locator 1", "Transport", "To Left Locator", kBankEntryValuePushType);
		//bank->addCommandEntry("No Shift", "Locator 2", "Transport", "To Right Locator", kBankEntryValuePushType);
		//bank->addCommandEntry("Shift",    "Locator 1", "Transport", "Set Left Locator", kBankEntryValuePushType);
		//bank->addCommandEntry("Shift",    "Locator 2", "Transport", "Set Right Locator", kBankEntryValuePushType);

		//------------------------------------------------------------------------
		// add wheel group
		// abuse Record button in Shift Layer as jog mode switch
		//IHWRemoteGroup *wheelGroup = bank->createGroup ("Wheel", "Wheel", kERHUINumFaders);
		//wheelGroup->addControl ("No Shift", "Data Wheel", "wheel");
		//wheelGroup->addControl ("Shift", "Record", "jogMode");

		//------------------------------------------------------------------------
		// add fader group
		ChanneldisplayID = addTextDisplay (32, 1, 4, 7, false, -1);
		IHWRemoteGroup *faderGroup = bank->createGroup ("Fader", "Fader Bank", kERHUINumFaders, ChanneldisplayID );

		faderGroup->addControl ("No Flip", "Fader 1", "1:fader");
		faderGroup->addControl ("No Flip", "Fader 2", "2:fader");
		faderGroup->addControl ("No Flip", "Fader 3", "3:fader");
		faderGroup->addControl ("No Flip", "Fader 4", "4:fader");
		faderGroup->addControl ("No Flip", "Fader 5", "5:fader");
		faderGroup->addControl ("No Flip", "Fader 6", "6:fader");
		faderGroup->addControl ("No Flip", "Fader 7", "7:fader");
		faderGroup->addControl ("No Flip", "Fader 8", "8:fader");

		faderGroup->addControl("No Flip", "Fader 1 Touch", "1:faderTouch");
		faderGroup->addControl("No Flip", "Fader 2 Touch", "2:faderTouch");
		faderGroup->addControl("No Flip", "Fader 3 Touch", "3:faderTouch");
		faderGroup->addControl("No Flip", "Fader 4 Touch", "4:faderTouch");
		faderGroup->addControl("No Flip", "Fader 5 Touch", "5:faderTouch");
		faderGroup->addControl("No Flip", "Fader 6 Touch", "6:faderTouch");
		faderGroup->addControl("No Flip", "Fader 7 Touch", "7:faderTouch");
		faderGroup->addControl("No Flip", "Fader 8 Touch", "8:faderTouch");

		/*
		faderGroup->addControl("No Flip", "Fader 1 Release", "1:faderTouch");
		faderGroup->addControl("No Flip", "Fader 2 Release", "2:faderTouch");
		faderGroup->addControl("No Flip", "Fader 3 Release", "3:faderTouch");
		faderGroup->addControl("No Flip", "Fader 4 Release", "4:faderTouch");
		faderGroup->addControl("No Flip", "Fader 5 Release", "5:faderTouch");
		faderGroup->addControl("No Flip", "Fader 6 Release", "6:faderTouch");
		faderGroup->addControl("No Flip", "Fader 7 Release", "7:faderTouch");
		faderGroup->addControl("No Flip", "Fader 8 Release", "8:faderTouch");
		*/

		// shared entries for the mute/solo buttons
		faderGroup->addControl ("No flip", "Mute 1", "1:mute");
		faderGroup->addControl ("No flip", "Mute 2", "2:mute");
		faderGroup->addControl ("No flip", "Mute 3", "3:mute");
		faderGroup->addControl ("No flip", "Mute 4", "4:mute");
		faderGroup->addControl ("No flip", "Mute 5", "5:mute");
		faderGroup->addControl ("No flip", "Mute 6", "6:mute");
		faderGroup->addControl ("No flip", "Mute 7", "7:mute");
		faderGroup->addControl ("No flip", "Mute 8", "8:mute");

		faderGroup->addControl ("No shift", "Solo 1", "1:solo");
		faderGroup->addControl ("No shift", "Solo 2", "2:solo");
		faderGroup->addControl ("No shift", "Solo 3", "3:solo");
		faderGroup->addControl ("No shift", "Solo 4", "4:solo");
		faderGroup->addControl ("No shift", "Solo 5", "5:solo");
		faderGroup->addControl ("No shift", "Solo 6", "6:solo");
		faderGroup->addControl ("No shift", "Solo 7", "7:solo");
		faderGroup->addControl ("No shift", "Solo 8", "8:solo");

		faderGroup->addControl("No flip", "Mute Defeat", "muteDefeat");
		faderGroup->addControl("No shift", "Solo Defeat", "soloDefeat");

		// shared entries for the mute/solo LEDs
		faderGroup->addControl ("No flip", "Mute 1 LED", "1:mute");
		faderGroup->addControl ("No flip", "Mute 2 LED", "2:mute");
		faderGroup->addControl ("No flip", "Mute 3 LED", "3:mute");
		faderGroup->addControl ("No flip", "Mute 4 LED", "4:mute");
		faderGroup->addControl ("No flip", "Mute 5 LED", "5:mute");
		faderGroup->addControl ("No flip", "Mute 6 LED", "6:mute");
		faderGroup->addControl ("No flip", "Mute 7 LED", "7:mute");
		faderGroup->addControl ("No flip", "Mute 8 LED", "8:mute");

		faderGroup->addControl ("No shift", "Solo 1 LED", "1:solo");
		faderGroup->addControl ("No shift", "Solo 2 LED", "2:solo");
		faderGroup->addControl ("No shift", "Solo 3 LED", "3:solo");
		faderGroup->addControl ("No shift", "Solo 4 LED", "4:solo");
		faderGroup->addControl ("No shift", "Solo 5 LED", "5:solo");
		faderGroup->addControl ("No shift", "Solo 6 LED", "6:solo");
		faderGroup->addControl ("No shift", "Solo 7 LED", "7:solo");
		faderGroup->addControl ("No shift", "Solo 8 LED", "8:solo");

		// shared entries for the select/record buttons
		faderGroup->addControl ("No default", "Select 1", "1:select");
		faderGroup->addControl ("No default", "Select 2", "2:select");
		faderGroup->addControl ("No default", "Select 3", "3:select");
		faderGroup->addControl ("No default", "Select 4", "4:select");
		faderGroup->addControl ("No default", "Select 5", "5:select");
		faderGroup->addControl ("No default", "Select 6", "6:select");
		faderGroup->addControl ("No default", "Select 7", "7:select");
		faderGroup->addControl ("No default", "Select 8", "8:select");

		faderGroup->addControl ("No shift", "Rec 1", "1:recordEnable");
		faderGroup->addControl ("No shift", "Rec 2", "2:recordEnable");
		faderGroup->addControl ("No shift", "Rec 3", "3:recordEnable");
		faderGroup->addControl ("No shift", "Rec 4", "4:recordEnable");
		faderGroup->addControl ("No shift", "Rec 5", "5:recordEnable");
		faderGroup->addControl ("No shift", "Rec 6", "6:recordEnable");
		faderGroup->addControl ("No shift", "Rec 7", "7:recordEnable");
		faderGroup->addControl ("No shift", "Rec 8", "8:recordEnable");

		// seperate entries for the select/record LED's
		faderGroup->addControl ("No default", "Select 1 LED", "1:select");
		faderGroup->addControl ("No default", "Select 2 LED", "2:select");
		faderGroup->addControl ("No default", "Select 3 LED", "3:select");
		faderGroup->addControl ("No default", "Select 4 LED", "4:select");
		faderGroup->addControl ("No default", "Select 5 LED", "5:select");
		faderGroup->addControl ("No default", "Select 6 LED", "6:select");
		faderGroup->addControl ("No default", "Select 7 LED", "7:select");
		faderGroup->addControl ("No default", "Select 8 LED", "8:select");

		faderGroup->addControl ("No shift", "Rec 1 LED", "1:recordEnable");
		faderGroup->addControl ("No shift", "Rec 2 LED", "2:recordEnable");
		faderGroup->addControl ("No shift", "Rec 3 LED", "3:recordEnable");
		faderGroup->addControl ("No shift", "Rec 4 LED", "4:recordEnable");
		faderGroup->addControl ("No shift", "Rec 5 LED", "5:recordEnable");
		faderGroup->addControl ("No shift", "Rec 6 LED", "6:recordEnable");
		faderGroup->addControl ("No shift", "Rec 7 LED", "7:recordEnable");
		faderGroup->addControl ("No shift", "Rec 8 LED", "8:recordEnable");

		// fader bank navigation
		faderGroup->addCommand ("No Shift", "Bank Left",  "Channel Bank Left");
		faderGroup->addCommand ("No Shift", "Bank Right", "Channel Bank Right");
		faderGroup->addCommand ("No Shift", "Channel Left",   "Channel Shift Left");
		faderGroup->addCommand ("No Shift", "Channel Right",  "Channel Shift Right");

		//------------------------------------------------------------------------
		// add master fader
		if (bank2)
		{	// assign the master fader to the Control Room Volume and the Control Room Phones Volume
			// it has its own channel map, but no fader group
			IHWRemoteChannelMap *masterChannelMap = bank->createChannelMap ("Control-Room", "Master");
			IHWRemoteChannelMap *phoneChannelMap = bank->createChannelMap ("Control-Phones", "Phone");
			bank2->addValueEntry ("No Shift", "Fader Master", "VST Control Room", 0, "volume", 0, masterChannelMap);
			bank2->addValueEntry ("Shift",    "Fader Master", "VST Control Room", 0, "volume", 0, phoneChannelMap);
		}
		else
		{
			// assign the master fader to the main mix channel
			// it has its own fader group and its own channel map
			IHWRemoteChannelMap *masterChannelMap = bank->createChannelMap ("Output", "Master");
			IHWRemoteGroup *masterGroup = bank->createGroup ("Fader", "Master Fader", 1, -1, masterChannelMap);
			masterGroup->addControl ("No Flip", "Fader Master", "1:fader");
		}

		//------------------------------------------------------------------------
		// add user function group
		// please look at HWRemoteUS224UserFunctions, where the user commands are defined
		// abuse Play/Stop buttons in Shift Layer as user commands
		IHWRemoteGroup *functionGroup = bank->createGroup ("Function", "User Function", 0);
		functionGroup->addCommand ("Shift", "Play",  "Set+Play");
		functionGroup->addCommand ("Shift", "Stop",  "Set+Stop");
		return true;
	}
	return false;
}

//------------------------------------------------------------------------
bool PLUGIN_API HWRemoteERHUI::term ()
{
	// currrently there's nothing to do

	return true;
}

//------------------------------------------------------------------------
bool PLUGIN_API HWRemoteERHUI::close ()
{
	// iterate through all controls and turn off the LED controls
	int32 addr = 0;
	PHWRemoteControl control;
	while (getRemoteControl (addr, control))
	{
		if (control.addr & 0x7f00)
			sendLEDController (addr, 0);
		addr++;
	}

	return HWRemoteBase::close ();
}

//------------------------------------------------------------------------
void PLUGIN_API HWRemoteERHUI::onIdle ()
{
	/*if (nullMode)
	{	// try to transmit the current lower/higher state periodically (unnecessary changes are catched with the LED state cache)
		for (int32 i = 0; i < kERHUINumFaders; i++)
		{
			bool isLower, isHigher;
			isLower = faderCurrentValue[i] <= programCurrentValue[i];
			isHigher = faderCurrentValue[i] >= programCurrentValue[i];
			faderLower[i] = isLower;
			faderHigher[i] = isHigher;
		}

		// update the LED's on the device
		updateLowerHigherLED("Select 1 LED", faderLower[0], "Rec 1 LED", faderHigher[0]);
		updateLowerHigherLED("Select 2 LED", faderLower[1], "Rec 2 LED", faderHigher[1]);
		updateLowerHigherLED("Select 3 LED", faderLower[2], "Rec 3 LED", faderHigher[2]);
		updateLowerHigherLED("Select 4 LED", faderLower[3], "Rec 4 LED", faderHigher[3]);
	}*/

	// Keep HUI online
	static int IdleCounter = 0;
	if (!(++IdleCounter % 50))
	{
		sendEventStandard(0x90, 0x00, 0x00);
		IdleCounter = 0;
	}

	HWRemoteBase::onIdle ();
}

//------------------------------------------------------------------------
/*void PLUGIN_API HWRemoteERHUI::activateLayer (const char *layerName)
{	// activation of a layer

	// check whether the Solo LED should light up
	if (strcmp (layerName, "Mute") == 0 || strcmp (layerName, "Solo") == 0)
		activateLayerLED(layerName, "Solo", "Solo LED");
	// check whether the Null LED should light up
	else if (strcmp (layerName, "Null") == 0 || strcmp (layerName, "No Null") == 0)
	{
		nullMode = strcmp (layerName, "Null") == 0 ? true : false;
		activateLayerLED(layerName, "Null", "Null LED");
		if (nullMode)
		{
			unsigned char us428RequestFader[] = {kMIDISysexStart, 0x4E, 0x00, 0x12, 0x10, kMIDISysexEnd};
			sendStandard (us428RequestFader);
		}
		else
		{	// Null mode gets deactivated
			// we need to force an update of the abused LED's (host applications internal state does not know about this abuse)
			forceControlUpdate ("Select 1 LED");
			forceControlUpdate ("Select 2 LED");
			forceControlUpdate ("Select 3 LED");
			forceControlUpdate ("Select 4 LED");
			forceControlUpdate ("Rec 1 LED");
			forceControlUpdate ("Rec 2 LED");
			forceControlUpdate ("Rec 3 LED");
			forceControlUpdate ("Rec 4 LED");
		}
	}
}
*/
//------------------------------------------------------------------------
/*void PLUGIN_API HWRemoteERHUI::changeValue (int32 addr, int32 value)
{
	//if (addr >= kERHUIAddrFader1 && addr <= kERHUIAddrFader8)
	//{
	//	// store the current value of the fader for comparison in null mode
	//	faderCurrentValue[addr - kERHUIAddrFader1] = value;
	//	if (nullMode)
	//		return;
	//}

	HWRemoteBase::changeValue (addr, value);
}
*/
//------------------------------------------------------------------------
void PLUGIN_API HWRemoteERHUI::sendController (PHWRemoteControl *control, int32 addr, int32 value)
{
	if ((control->status == kMIDIController) && (addr >= kERHUIAddrFader1 && addr <= kERHUIAddrFader8))
	{
		int32 m = value % 16;
		if (m < 8)
			value = value - m;
		else
			value = value + (16 - m);
	}

	/*if (addr >= kERHUIAddrFader1 && addr <= kERHUIAddrFader8)
	{	// store the value for the comparison in null mode, don't send it out
		programCurrentValue[addr - kERHUIAddrFader1] = value;
		return;
	}
	else if (control->status == kMIDIController && control->addr > 0x7F)
	{
		// the hack for null mode, we intercept the LED controls for Null-Mode difference indication
		if (nullMode)
		{
			if ((control->addr >= 0x0300 && control->addr <= 0x303)
			    || (control->addr >= 0x0400 && control->addr <= 0x403))
			    return;
		}

		sendLEDController (addr, value);
		return;
	}*/
	HWRemoteBase::sendController (control, addr, value);
}


//------------------------------------------------------------------------ 
void HWRemoteERHUI::forceControlUpdate (const char *control)
{
	int32 index = getRemoteControlAddr (control);
	if (index >= 0)
		forceRemoteControlUpdate (index);
}

//------------------------------------------------------------------------ 
void HWRemoteERHUI::activateLayerLED(const char *activatedLayerName, const char *layer, const char *controlLED)
{	// detects whether the LED should activated for a specific layer
	int32 index = getRemoteControlAddr (controlLED);
	if (index >= 0)
		sendLEDController (index, strcmp(activatedLayerName, layer) == 0 ? 0x7f : 0);
}

//------------------------------------------------------------------------ 
void HWRemoteERHUI::updateLowerHigherLED(const char *lowerLED, bool isLower, const char *higherLED, bool isHigher)
{	// transmits the lower/higher state via the given LED controls
	int32 index = getRemoteControlAddr (lowerLED);
	if (index >= 0)
		sendLEDController (index, isLower ? 0x7f : 0);

	index = getRemoteControlAddr (higherLED);
	if (index >= 0)
		sendLEDController (index, isHigher ? 0x7f : 0);
}

//------------------------------------------------------------------------ 
void HWRemoteERHUI::sendLEDController(int32 addr, int32 value)
{
	// check whether the LED state needs to be transmitted (Sysex is expensive !)
	PHWRemoteControl control;
	if (getRemoteControl(addr, control))
	{
		bool *ledState;
		switch (control.addr >> 8)
		{
		case 1:
			ledState = &transportLED[(control.addr & 0x7f) - 0x13];
			break;
		case 2:
			ledState = &muteLED[control.addr & 0x7f];
			break;
		case 3:
			ledState = &selectLED[control.addr & 0x7f];
			break;
		case 4:
			ledState = &recordLED[control.addr & 0x7f];
			break;
		case 5:
			ledState = &nullLED;
			break;
		case 6:
			ledState = &soloLED;
			break;
		case 7:
			ledState = &bankLeftLED;
			break;
		case 8:
			ledState = &bankRightLED;
			break;
		default:
			return;
		}
		if ((value != 0) != (*ledState != false))
		{
			if (value)
				*ledState = true;
			else
				*ledState = false;

			unsigned char msg[8] = { kMIDISysexStart, 0x4e, 0x00, 0x12 };
			if ((control.addr & 0x00FF) == 0x00FF)
			{	// "single byte" LED's
				msg[4] = (unsigned char)(control.addr >> 8) & 0x7f;
				msg[5] = (unsigned char)(value != 0 ? 0x7f : 0x00);
				msg[6] = kMIDISysexEnd;
			}
			else
			{	// "multi byte" LED's
				msg[4] = (unsigned char)((control.addr >> 8) & 0x7f);
				msg[5] = (unsigned char)(control.addr & 0x7f);
				msg[6] = (unsigned char)(value != 0 ? 0x7f : 0x00);
				msg[7] = kMIDISysexEnd;
			}
			sendStandard(msg);
		}
	}
}

//------------------------------------------------------------------------ 
void PLUGIN_API HWRemoteERHUI::sendText(const char *text, int32 pos, int32 displId)
{
	// sanity check
	if (displId == -1)
		return;

	if (displId == ChanneldisplayID)
	{
		char    msg[96];  // space to build up the Sysex command.
		long    size = strlen(text);
		long    i;

		// prepare a Sysex header (In this case for the channel displays)
		msg[0] = kMIDISysexStart;
		msg[1] = 0x00;
		msg[2] = 0x00;
		msg[3] = 0x66;
		msg[4] = 0x05;
		msg[5] = 0x00;
		msg[5] = 0x10;
		msg[6] = pos / 4;


	}

/*		char    msg[96];  // space to build up the Sysex command.
	long    size = strlen(text);
	long    i;

	// prepare a Sysex header (In this case for the Steinberg Houston)
	msg[0] = kMIDISysexStart;
	msg[1] = 0x39;
	msg[2] = 'H';         // 'H'ouston
	msg[3] = 1;           // MsgType: send text
	msg[4] = (char)(pos + 1);  // display position
	msg[5] = size;        // Text len

	// Clean up the text, keep only valid character codes for the display.
	// This is very dependend on the device's capabilities
	// In this case only 7 bit ASCII is allowed and only characters codes
	// 0x20 to 0x7f.
	for (i = 0; i < size; i++)
	{
		char c = *text++ & 0x7f;
		if (c < ' ')
			c = ' ';
		msg[i + 6] = c;
	}

	// for Houston a checksum needs to be calculated
	msg[i + 6] = calcCheckSum(&msg[1], i + 5);
	msg[i + 7] = kMIDISysexEnd;

	// bring the Sysex on the way to the device
	sendStandard(msg);*/
}

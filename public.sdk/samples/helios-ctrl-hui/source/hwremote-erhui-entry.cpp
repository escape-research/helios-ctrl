//-----------------------------------------------------------------------------
// Steinberg Remote Interface Software Development Kit (SDK)
// Version 2.1    Date : 2008
//
// Project      : HW Remote Tascam US-224 Sample
// Filename     : hwremote-us224-entry.cpp
// Created by   : Steinberg
// Description  : Steinberg Module Entry
//
//-----------------------------------------------------------------------------
// LICENSE
// (c) 2010, Steinberg Media Technologies GmbH
//-----------------------------------------------------------------------------
// This Software Development Kit may not be distributed in parts or its entirety 
// without prior written agreement by Steinberg Media Technologies GmbH. This
// SDK must not be used to re-engineer or manipulate any technology used in any
// Steinberg or Third-party application or software module, unless permitted by
// law.
//-----------------------------------------------------------------------------

#define INIT_CLASS_IID // this will define all class ids 

//------------------------------------------------------------------------
// Include SDK C++ Classes
// This will assure that all UID's of the used interfaces are defined
#include "pluginfactory.h"
#include "pluginterfaces/host/ihostclasses.h"
#include "pluginterfaces/host/devices/idevice.h"
#include "pluginterfaces/host/devices/iport.h"
//------------------------------------------------------------------------

#include "hwremote-erhui.h"
#include "hwremote-erhui-detect.h"

//------------------------------------------------------------------------
//  Plug-In factory information
//------------------------------------------------------------------------
BEGIN_FACTORY ("Steinberg Media Technologies", 
			   "http://www.steinberg.de", 
			   "mailto:info@steinberg.de", 
			   PFactoryInfo::kNoFlags)

DEF_CLASS (kHWRemoteERHUIUID,
		   PClassInfo::kManyInstances,    
		   kRemoteDeviceClass,
		   "Helios HUI Control",
		   HWRemoteERHUI::createInstance)

DEF_CLASS (kHWRemoteERHUIDetectUID,
		   1, // only one instance for the detector allowed
		   "Device Detection", // HW Remote Detection component
		   "Helios HUI Control",
		   HWRemoteERHUIDetect::createInstance)

ENABLE_NON_UNICODE_HOSTS

END_FACTORY

//------------------------------------------------------------------------
//  Module init/exit
//------------------------------------------------------------------------

// nothing special to do when the library is loaded...
bool InitModule ()   { return true; }
bool DeinitModule () { return true; }

//-----------------------------------------------------------------------------
// Steinberg Remote Interface Software Development Kit (SDK)
// Version 2.1    Date : 2008
//
// Category     : Devices
// Filename     : hwremote-us224-detect.cpp
// Created by   : Steinberg
// Description  : Device Detection Tascam US-224 Sample implementation
//-----------------------------------------------------------------------------
// LICENSE
// (c) 2010, Steinberg Media Technologies GmbH
//-----------------------------------------------------------------------------
// This Software Development Kit may not be distributed in parts or its entirety 
// without prior written agreement by Steinberg Media Technologies GmbH. This
// SDK must not be used to re-engineer or manipulate any technology used in any
// Steinberg or Third-party application or software module, unless permitted by
// law.
//-----------------------------------------------------------------------------

#include "hwremote-erhui-detect.h"
#include "hwremote-erhui.h"

#include "pluginterfaces/host/ihostclasses.h"
#include "pluginterfaces/host/devices/idevice.h"
#include "pluginterfaces/host/devices/iport.h"
#include "pluginterfaces/host/ihostapplication.h"

#if MAC
#define kHeliosCtrlPortName "Helios HUI Control Port"
#elif WINDOWS
#define kHeliosCtrlPortName "Helios HUI Control"
#endif

//------------------------------------------------------------------------
HWRemoteERHUIDetect::HWRemoteERHUIDetect ()
: hostClasses (0)
{
	FUNKNOWN_CTOR
}

//------------------------------------------------------------------------
HWRemoteERHUIDetect::~HWRemoteERHUIDetect ()
{
	FUNKNOWN_DTOR
}

//------------------------------------------------------------------------------
IMPLEMENT_REFCOUNT (HWRemoteERHUIDetect)
//------------------------------------------------------------------------------
tresult PLUGIN_API HWRemoteERHUIDetect::queryInterface (const char* iid, void** obj)
{
	QUERY_INTERFACE (iid, obj, ::FUnknown::iid, IPluginBase)
	QUERY_INTERFACE (iid, obj, ::IPluginBase::iid, IPluginBase)
	QUERY_INTERFACE (iid, obj, ::IDetector::iid, IDetector)

    *obj = 0;
    return kNoInterface;
}

//------------------------------------------------------------------------
tresult HWRemoteERHUIDetect::initialize (FUnknown* context)
{
	context->queryInterface (IHostClasses::iid, (void**)&hostClasses);
	if (!hostClasses)
		return kResultFalse;
	return kResultOk;
}

//------------------------------------------------------------------------
tresult HWRemoteERHUIDetect::terminate ()
{
	hostClasses = 0;
	return kResultOk;
}

//------------------------------------------------------------------------
// create function required for plugin factory,
// it will be called to create new instances of this plugin
FUnknown* HWRemoteERHUIDetect::createInstance (void* context)
{
	return (IDetector*)new HWRemoteERHUIDetect ();
}


//------------------------------------------------------------------------
tresult PLUGIN_API HWRemoteERHUIDetect::getDeviceTypeInfo (struct IDetectorDeviceInfo* devInfo)
{
	devInfo->category = kRemoteDeviceClass;
	devInfo->name = "Helios-Ctrl";
	devInfo->method = kDeviceDetectionPlugAndPlay;
	return kResultOk;
}

//------------------------------------------------------------------------
tresult HWRemoteERHUIDetect::detect ()
{
	// Sample code to detect MIDI ports of plug'n'play devices (usually USB or FireWire/1394)
	// The ports are available on the system while the device is connected to the computer.
	//
	// Only one device is detected in the code below. It is possible to detect more than
	// one device, by keeping record of the ports and install more than one device.

	// retrieve the host interfaces for the Port Registry and the Device List
	FInstancePtr<IPortRegistry> portRegistry (hostClasses);
	FInstancePtr<IDeviceList2> deviceList (hostClasses);


	bool isUnicode = false;
	IHostApplicationW* host = 0;
	if (hostClasses->queryInterface (IHostApplicationW_iid, (void**)&host) == kResultTrue)
	{
		if (host->isUnicodeApplication () == kResultTrue)
			isUnicode = true;
		host->release ();
	}

	if (!portRegistry || !deviceList)
		return kNotInitialized;


	// iterate through the ports and check for availability of the "US-224 Control" port
	IPort* inPort = 0;
	IPort* outPort = 0;

	for (int portIndex = 0; portIndex < portRegistry->countPorts (); portIndex++)
	{
		IPort* port = portRegistry->getPortByIndex (portIndex);

		if (port && port->isPortType (kMidiPortType) && port->isSubType (kSystemPortType))
		{
			bool compareRes = false;
			if( isUnicode )
				compareRes = strcmp16 ((const char16*)port->getSysName(), STR16(kHeliosCtrlPortName)) == 0;
			else
				compareRes = strcmp8 ((const char8*)port->getSysName(), kHeliosCtrlPortName) == 0;

			if( compareRes == true )
//			if (strcmp (port->getSysName (), kHeliosCtrlPortName) == 0)
			{
				if (port->isSystemInput ())
					inPort = port;
				else
					outPort = port;
			}
		}
	}


	// check whether the device is already installed
	IDevice *device = deviceList->getDeviceByClassID ("Helios-Ctrl", 0);

	if (inPort && outPort)
	{	// ports were detected
		if (!device)
		{
			// if no device is present, install a new one and connect the ports
			// device = deviceList->installDeviceByName (kRemoteDeviceClass, "Tascam US-224", -1, true);
			TUID kHeliosCtrlid = kHWRemoteERHUIUID;
			device = deviceList->installDeviceByClassId (kHeliosCtrlid, -1, true);
		}

		// connect the ports to the device
		// Only connect the ports if the device is not already connected to them
		// this check will help to distinguish the connection scenario for multiple
		// connected devices of the same typ
		FUnknownPtr<IConnector> connector (device);
		if (connector)
		{
			if (!connector->isConnected (inPort))
				connector->connectTo (inPort);
			if (!connector->isConnected (outPort))
				connector->connectTo (outPort);
		}
	}
	else
	{	// port vanished, remove the device
		if (device)
			deviceList->uninstallDevice (device);
	}
	return kResultOk;
}

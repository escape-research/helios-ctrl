//-----------------------------------------------------------------------------
// Steinberg Remote Interface Software Development Kit (SDK)
// Version 2.1    Date : 2008
//
// Category     : HW Remote Controller
// Filename     : hwremote-us224.h
// Created by   : Steinberg
// Description  : HW Remote Controller Tascam US-224 Sample implementation
//-----------------------------------------------------------------------------
// LICENSE
// (c) 2010, Steinberg Media Technologies GmbH
//-----------------------------------------------------------------------------
// This Software Development Kit may not be distributed in parts or its entirety 
// without prior written agreement by Steinberg Media Technologies GmbH. This
// SDK must not be used to re-engineer or manipulate any technology used in any
// Steinberg or Third-party application or software module, unless permitted by
// law.
//-----------------------------------------------------------------------------

#ifndef __hwremote_erhui__
#define __hwremote_erhui__

#ifndef __hwremotebase__
#include "hwremote/hwremotebase.h"
#endif

using namespace Steinberg;

enum
{
	// data ranges
	kERHUINumFaders = 8,             // 4 faders for the fader group
	kERHUINumTransportLEDs = 5,

	// pre-catched addresses for the faders (need to be in sync with the position in the HWRemoteUS224CtrlMap)
	kERHUIAddrFader1 = 0,
	kERHUIAddrFader2 = 1,
	kERHUIAddrFader3 = 2,
	kERHUIAddrFader4 = 3,
	kERHUIAddrFader5 = 4,
	kERHUIAddrFader6 = 5,
	kERHUIAddrFader7 = 6,
	kERHUIAddrFader8 = 7
};


class HWRemoteERHUI : public Steinberg::HWRemoteBase
{
public:
	//------------------------------------------------------------------------
	HWRemoteERHUI ();
	virtual ~HWRemoteERHUI ();

	//------------------------------------------------------------------------
	// create function required for plugin factory,
	// it will be called to create new instances of this plugin
	static FUnknown* createInstance (void* context);

	//------------------------------------------------------------------------
	// overloaded methods implement the hw remote specific functionality
	bool PLUGIN_API init (class IHWRemoteStandard *standardImplementation);
	bool PLUGIN_API term ();

	bool PLUGIN_API close ();

	//void PLUGIN_API changeValue (int32 addr, int32 value);
	void PLUGIN_API sendController (PHWRemoteControl *control, int32 addr, int32 value);

	//void PLUGIN_API activateLayer (const char *layerName);   // activation of a layer

	void PLUGIN_API onIdle ();			// called periodically in UI thread context

	void PLUGIN_API sendText(const char *text, int32 pos, int32 displId);

private:
	void forceControlUpdate (const char *control);
	void updateLowerHigherLED(const char *lowerLED, bool isLower, const char *higherLED, bool isHigher);
	void activateLayerLED(const char *activatedLayerName, const char *layer, const char *controlLED);
	void sendLEDController (int32 addr, int32 value);

	// The channel display ( 8 x 4 )
	int32 ChanneldisplayID;

	// cache of the LED states
	bool muteLED[kERHUINumFaders];
	bool selectLED[kERHUINumFaders];
	bool recordLED[kERHUINumFaders];
	bool transportLED[kERHUINumTransportLEDs];
	bool soloLED;
	bool nullLED;
	bool bankLeftLED;
	bool bankRightLED;

	// Null mode handling
	bool nullMode;
	int32 faderCurrentValue[kERHUINumFaders];
	int32 programCurrentValue[kERHUINumFaders];
	bool faderLower[kERHUINumFaders];
	bool faderHigher[kERHUINumFaders];
};

#define kHWRemoteERHUIUID     INLINE_UID (0xCCADB15A, 0x319141F6, 0xB3AA4841, 0xE3E20164)

#endif
